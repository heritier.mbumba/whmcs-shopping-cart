import { gql } from "apollo-server-micro";
export const typeDefs = gql`
  type Ping {
  message: String
  status: Int
}

type DomainPricingAddons {
  dns: Boolean
  email: Boolean
  idprotect: Boolean
}

input DomainForPrice {
  name: String
  tld: String
  status: Int
}

input PGroup {
  type: String
  id: Int
}

type DomainPricing {
  categories: [String]
  addons: DomainPricingAddons
  group: String
  register: String
  transfer: String
  renew: String
  name: String
}

type DomainForPricing {
  categories: [String]
  addons: DomainPricingAddons
  group: String
  register: String
  transfer: String
  renew: String
  tld: String
  name: String
  status: Int
}

type ProductGroup {
  id: ID!
  name: String!
  hidden: Int
  disabledgateways: String
}

type Pricing {
  prefix: String
  suffix: String
  msetupfee: String
  qsetupfee: String
  ssetupfee: String
  asetupfee: String
  bsetupfee: String
  tsetupfee: String
  monthly: String
  quarterly: String
  semiannually: String
  annually: String
  biennially: String
  triennially: String
}

type PricingCategory {
  USD: Pricing
  ZAR: Pricing
}

type Product {
  type: String
  gid: Int
  name: String
  description: String
  hidden: Int
  pid: Int
  pricing: PricingCategory
  billingCycles: Pricing
  defaultBillingCycle: String
}

type Domain {
  name: String
  status: Int
  sld: String
  tld: String
  premium: Boolean
}

type Client {
  userid: Int!
  passwordhash: String
  twoFactorEnabled: Boolean
}

input NewClient {
  firstname: String
  lastname: String
  email: String
  password2: String
  address1: String
  city: String
  state: String
  postcode: String
  country: String
  phonenumber: String
  noemail: Boolean
}

input AddOrderInput {
  clientid: Int
  paymentmethod: String
  pid: [Int]
  domain: [String]
  billingcycle: [String]
  domaintype: [String]
  regperiod: [Int]
  eppcode: [String]
  promocode: String
  idprotection: [Int]
  addons: [String]
  noemail: Boolean
  noinvoiceemail: Boolean
}

type AddOrderSuccess {
  orderid: Int
  serviceids: String
  addonids: String
  domainids: String
  invoiceid: Int
}

type NewClientSuccess {
  clientid: Int!
}

input Gateway {
  name: String
  marchandid: String
  marchandkey: String
  passphrase: String
  paymentmethod: String
}

type Payfast {
  merchant_id:Int
  merchant_key: String
  notify_url: String
  name_first: String
  name_last: String
  email_address: String
  m_payment_id: Int
  amount: Float
  item_name: String
  user_agent: String
  payment_method: String
  signature: String
  uuid: String
}

type SnapScan {
  invoiceid: Int
  userid: Int
  status: String
  credit: String
  tax: String
  total: String
  balance: String
  taxrate: String
  paymentmethod: String
  apiKey: String
  accessApiKey: String
  
}

type Invoice {
  snapscan: SnapScan
  payfast: Payfast,
  paymentidentifier: String
}

type Error {
  status: Int
  message: String
}

"""
Search domain response
"""
type SearchDomainResponse {
  result: [Domain]
  error: Error
}

"""
GetProducts Response
"""
type GetProductsResponse {
  result: [Product]
  error: Error
}


"""
Add Order Response
"""
type AddOrderResponse {
  result: AddOrderSuccess
  error: Error
}

"""
Add Client Response
"""
type AddClientResponse {
  result: NewClientSuccess
  error: Error
}
"""
Get domain tlds Response
"""
type DomainTldsResponse {
  result: [DomainPricing]
  error: Error
}


"""
Get domain pricing
"""
type DomainTldsPricingResponse {
  result: [DomainForPricing]
  error: Error
}

"""
Validate login Response
"""
type ValidateLoginResponse {
  result: Client
  error: Error
}

type GetInvoiceResponse {
  result: Invoice
  error: Error
}




type Query {
  pingApp: Ping
  getProductGroups(groups: [PGroup]): [ProductGroup]
  isValidateLogin(email: String!, password2: String!): ValidateLoginResponse
  searchDomains(domain: [String]): DomainTldsPricingResponse
  getProducts(gid: Int, package: [Int], type: String): GetProductsResponse
  addClient(client: NewClient): AddClientResponse
  getDomainTldsPricing(domains: [DomainForPrice]): DomainTldsPricingResponse
  getDomainTlds(tlds: [String]): DomainTldsResponse
  addOrder(order: AddOrderInput): AddOrderResponse
  getInvoice(id: Int, client: NewClient): GetInvoiceResponse
  
}

`;

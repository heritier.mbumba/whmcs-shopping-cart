import {
  getProductGroups,
  getProducts,
  getEssentialProducts,
  getProductAddons,
} from "./functions/product";
import {
  addClient,
  isValidateLogin,
  getClientInvoices,
  addGCLID,
} from "./functions/client";
import {
  searchDomains,
  getDomainTldsPricing,
  getDomainTlds,
  searchAlternativeDomains,
} from "./functions/domain";
import { addOrder, getInvoice, getPromotions } from "./functions/shop";

export const resolvers = {
  GetAddonProductResponse: {
    __resolveType(obj: Record<string, any>, ctx, info) {
      if (obj?.id) {
        return "AddonProduct";
      } else if (obj?.status) {
        return "Error";
      }
      return null;
    },
  },
  GetClientInvoices: {
    __resolveType(obj, ctx, info) {
      if (obj?.id) {
        return "ClientInvoice";
      } else if (obj?.status) {
        return "Error";
      }
      return null;
    },
  },
  PromotionResponse: {
    __resolveType(obj, ctx, info) {
      if (obj?.message) {
        return "PromotionNotFound";
      } else if (obj?.id) {
        return "Promotion";
      } else {
        return null;
      }
    },
  },
  Query: {
    pingApp: () => {
      try {
        return {
          message: "Server is running",
          status: 200,
        };
      } catch (error) {
        return null;
      }
    },
    /**
     * Get Product groups
     * @param _
     * @param groupsIds
     * @returns
     */
    getProductGroups,
    /**
     * Get Products by group and filtered by supported packages
     * @param _
     * @param group
     * @returns
     */
    getProducts,

    /**
     * Get Essential products
     * for a specific product
     */
    getEssentialProducts,

    /**
     * Get Addons for a specific product
     */
    getProductAddons,

    /**
     * Add Client
     * @param _
     * @param client
     * @returns
     */
    addClient,
    /**
     * Search Domains
     * @param _
     * @param domain
     * @returns
     */
    searchDomains,

    /**
     * Search alternatives domains
     */
    searchAlternativeDomains,

    /**
     * Get Domain TLDS with pricing
     * @param _
     * @param domains
     * @returns
     */
    getDomainTldsPricing,

    /**
     * Get Domain TLD
     * @param _
     * @param args
     * @returns
     */
    getDomainTlds,

    /**
     * Check Client Login credentials
     * @param _
     * @param param1
     * @returns
     */
    isValidateLogin,
    /**
     * Add new order
     * @param _
     * @param order
     * @returns
     */
    addOrder,

    /**
     * Get Invoice
     * @param _
     * @param invoice
     * @returns
     */
    getInvoice,
    /**
     *
     */
    getClientInvoices,

    /**
     * Get promotion
     */
    getPromotions,

    /**
     * Add google identifier to whmcs
     */
    addGCLID,
  },
};

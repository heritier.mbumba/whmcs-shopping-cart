import Request from "@lib/request";
import { IAddNewClient } from "@lib/types/auth";

/**
 * Adding a new client
 * @param _
 * @param client
 * @returns GraphQL Query Response @lib/graphql/schema.graphq
 */
export const addClient = async (_: any, args: { client: IAddNewClient }) => {
  try {
    /**
     * Send a reuest to WHMCS API
     * @link https://developers.whmcs.com/api-reference/addclient/
     * @return clientid
     */
    const response = await Request('WHMCS').post({
      action: 'AddClient',
      ...args?.client,
      skipvalidation: true
    })

    return {
      result: {
        clientid: response?.clientid
      }
    }
  } catch (error) {
    return {
      error: {
        status: error?.statusCode || 500,
        message: error?.message || 'Something went wrong, Please try again'
      }
    }
  }
}

/**
 * Verify client email and password
 * @param _
 * @param param1
 * @returns
 */
export const isValidateLogin = async (
  _: any,
  args: { email: string; password2: string }
) => {
  try {
    /**
     * @link https://developers.whmcs.com/api-reference/validatelogin/
     * @returns userid etc...
     */
    const response = await Request('WHMCS').post({
      action: 'ValidateLogin',
      ...args
    })

    if (response?.result !== 'success') {
      const { result, ...message } = response
      if (response.result === 'error') {
        return {
          error: {
            status: 422,
            message
          }
        }
      }
    }
    const { result, ...client } = response
    return {
      result: client
    }
  } catch (error) {
    return {
      error: {
        status: error?.statusCode || 500,
        message: error?.message || 'Something went wrong, Please try again'
      }
    }
  }
}

export const getClientInvoices = async (
  _: any,
  client: { clientid: number }
) => {
  try {
    const clientInvoice = await Request('WHMCS').post({
      action: 'GetInvoices',
      userid: +client?.clientid
    })
    if (clientInvoice?.result !== 'success') {
      return {
        status: 400,
        message: clientInvoice?.message || 'Something went wrong'
      }
    }
    const {
      invoices: { invoice }
    } = clientInvoice
    const unpaidInvoices = invoice?.filter((inv) => inv?.status === 'Unpaid')
    const invoiceObj = unpaidInvoices?.map((invoice) => ({
      id: invoice?.id,
      subtotal: invoice?.subtotal,
      total: invoice?.total,
      invoicestatus: invoice?.status,
      paymentmethod: invoice?.paymentmethod,
      created_at: invoice?.created_at,
      duedate: invoice?.duedate
    }))

    return invoiceObj
  } catch (error) {}
}

/**
 * Add Google Click Identifier to WHMCS for conversion tracking
 * @param _ 
 * @param args 
 * @returns 
 */
export const addGCLID = async (_:any, args: {orderid: number; gclid: string}) => {
  try {
    
    const response = await Request("WHMCS").post({
      action: 'AddGoogleClickAds',
      ...args
    })

    if(response?.result !== 'success'){
      return {
        status: 500,
        message: response?.message || 'Something went wrong'
      }
    }

    return response

  } catch (error) {
    
    return {
      status: error?.status || 500,
      message: error?.response?.data?.message || 'Something went wrong'
    }
  }
}

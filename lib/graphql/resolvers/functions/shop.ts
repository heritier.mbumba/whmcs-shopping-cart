import crypto from 'crypto'
import Request from "@lib/request";
import axios from "axios";
import { IClient } from '@lib/types/auth';


const generatePaymentIdentifier = async (params: Record<string, any>) => {

  const result = await axios.post(`https://www.payfast.co.za/onsite/process`, JSON.stringify(params), {
    headers: {
      "Content-Type": "application/json"
    }
  })
      .then((res) => {
        return res.data.uuid || null;
      })
      .catch((error) => {
        return
      });
  return result;
};

/**
 * Add order
 * @param _ 
 * @param order 
 * @returns 
 */
export const addOrder = async (_: any, order: { order: Record<string, any> }) => {
  try {

    const response = await Request("WHMCS").post({
      action: "AddOrder",
      ...order?.order,
    });

    if(response.result !== "success"){
      const { message} = response
      return {
        error: {
          status: 400,
          message
        }
      }
    }
    
    const {result, ...fields} = response

    return {
      result: fields
    }
  } catch (error) {
    return {
      error: {
        status: error?.statusCode || 500,
        message: error?.message || 'Something went wrong, Please try again'
      }
    }
  }
}

export const getInvoice = async (_:any, invoice: {id: number, client: IClient}) => {
  try {
    const response = await Request("WHMCS").post({
      action: "GetInvoice",
      invoiceid: invoice?.id
    })

  

    if(response.result !== 'success'){
      return {
        error: {
          status: 400,
          message: response?.message || 'Something went wrong'
        }
      }
    }

    const {result, ...invoiceDetail} = response

    


    /**
     * Generate signature
     */
     const generateSignature = (data: Record<string, string>, passPhrase: string = null) => {

      const keys: string[] = []

      for(const key in data) {
        
        keys.push(`${key}=${encodeURIComponent(data[key])}`)
      }

      if(passPhrase){
        keys.push(`passphrase=${encodeURIComponent(passPhrase.replace(/\s/g, ''))}`)
      }

      const cleanKeys = keys?.map(key=>key.replace(/%20/g, "+"))
    
      return crypto.createHash("md5").update(cleanKeys.join('&')).digest("hex");
    };
 

    let data = {}

    if(invoiceDetail?.paymentmethod !== 'snapscan'){
     
      data = {
        merchant_id:process.env.PAYFAST_MERCHANT_ID,
        merchant_key: process.env.PAYFAST_MERCHANT_KEY,
        return_url: process.env.PAYFAST_RETURN_URL + '?=id' + invoiceDetail?.invoiceid,
        cancel_url: process.env.PAYFAST_CANCEL_URL + '?=id' + invoiceDetail?.invoiceid,
        notify_url: process.env.PAYFAST_NOTIFY_URL + '?=invoiceid' + invoiceDetail?.invoiceid,
        name_first: invoice?.client?.firstname,
        name_last: invoice?.client?.lastname,
        email_address: invoice?.client?.email,
        m_payment_id: invoiceDetail?.invoiceid,
        amount: invoiceDetail?.total,
        item_name: `1-grid purchase, Invoice ID #${invoiceDetail?.invoiceid} express-checkout`,
        
      }

      const signature = generateSignature(data, process.env.PAYFAST_PASSPHRASE)
      
      data['signature'] = signature
     const paymentidentifier = await generatePaymentIdentifier(data)

      if(invoiceDetail?.paymentmethod === 'payfast'){
        data['payment_method']= 'cc'
      }else {
        data['payment_method']= 'eft'
      }


      return {
        result: {
          payfast: data,
          paymentidentifier 
        }
      }
    }

    
    const verify_hash = () => {
      const hash = invoiceDetail?.invoiceid + invoiceDetail.userid
      
      return crypto.createHash("md5").update(String(hash)).digest("hex")
    }

    return {
      result: {
        snapscan: {
          ...invoiceDetail,
          apiKey: process?.env?.SNAPSCAN_API_KEY,
          verifyHash: verify_hash()
        }
      }
    }

  } catch (error) {
    return {
      error: {
        status: error?.statusCode || 500,
        message: error?.message || 'Something went wrong, Please try again'
      }
    }
  }
}

export const getPromotions = async (_:any, code: {code: string}) => {
  try {
    const response = await Request("WHMCS").post({
      action: "GetPromotions",
      code: code?.code
    })

    if(response?.result !== 'success'){
      return null
    }

    if(response.totalresults === 0){
      return [{
        message: `[${code.code}] is not a valid promotion code`,
        code: code.code
      }]
    }
    const {promotion} = response.promotions
    

    return promotion
  } catch (error) {
    return null
  }
}
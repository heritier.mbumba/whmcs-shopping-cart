import Request from "@lib/request";
import { IDomain, IDomainWithPrice } from "@lib/types/domain";

/**
 * Search on .co.za tld domain
 * @param _
 * @param args
 * @returns
 */
export const searchDomains = async (_: any, args: { domain: string[] }) => {
  try {
    /**
     * Send request to the domain middleware
     */
    const response = await Request("DOMAIN").post({
      domain: args.domain,
    });

    /**
     * Check search response status
     */
    if (!response?.data) {
      return [];
    } else if ([422, 500].includes(response?.status)) {
      return {
        error: {
          status: response?.status,
          message: response?.data?.message,
        },
      };
    }

    /**
     * search result
     */
    const { results } = response?.data;

    /**
     *
     */
    const domains: IDomain[] = [];

    for (const item of results) {
      if (item?.name || item.domain) {
        domains.push({
          name: item?.name || item?.domain,
          status: +item?.status,
          sld: item?.sld,
          tld: item?.tld,
          premium: item?.premium || false,
        });
      }
    }

    /**
     * get domain pricing
     */
    const getTldPricing = await Request("WHMCS").post({
      action: "GetTLDPricing",
      currencyid: 1,
    });

    const { pricing, currency } = getTldPricing;

    const domainWithPrice = (domain: string) => {
      return {
        renew:
          pricing[domain]?.renew && Object.values(pricing[domain]?.renew)[0],
        register:
          pricing[domain]?.register &&
          Object.values(pricing[domain]?.register)[0],
        transfer: pricing[domain]?.transfer
          ? Object.values(pricing[domain]?.transfer)[0]
          : "0.00",
      };
    };

    const domainWithPrices: IDomainWithPrice[] = domains?.map((domain) => ({
      ...pricing[domain?.tld],
      ...domain,
      ...domainWithPrice(domain?.tld),
    }));

    return {
      result: domainWithPrices,
    };
  } catch (error) {
    return {
      error: {
        status: error.response?.status || error?.statusCode || 500,
        message:
          error.response.data?.message ||
          error?.message ||
          "Something went wrong, Please try again",
      },
    };
  }
};

/**
 * Search alternative domain excluding .co.za tld
 * @param _
 * @param args
 * @returns
 */
export const searchAlternativeDomains = async (
  _: any,
  args: { domain: string[] }
) => {
  try {
    const response = await Request("DOMAIN").post({
      domain: args.domain,
    });

    if (!response?.data) {
      return [];
    } else if ([422, 500].includes(response?.status)) {
      return {
        error: {
          status: response?.status,
          message: response?.data?.message,
        },
      };
    }
    const { results } = response?.data;

    const domains: IDomain[] = [];

    for (const item of results) {
      if (item?.name || item.domain) {
        domains.push({
          name: item?.name || item?.domain,
          status: +item?.status,
          sld: item?.sld,
          tld: item?.tld,
          premium: item?.premium || false,
        });
      }
    }

    const getTldPricing = await Request("WHMCS").post({
      action: "GetTLDPricing",
      currencyid: 1,
    });

    const { pricing, currency } = getTldPricing;

    const price = (domain: string) => {
      return {
        renew:
          pricing[domain]?.renew && Object.values(pricing[domain]?.renew)[0],
        register:
          pricing[domain]?.register &&
          Object.values(pricing[domain]?.register)[0],
        transfer: pricing[domain]?.transfer
          ? Object.values(pricing[domain]?.transfer)[0]
          : "0.00",
      };
    };

    const prices: IDomainWithPrice[] = domains?.map((domain) => ({
      ...pricing[domain?.tld],
      ...domain,
      ...price(domain?.tld),
    }));

    return {
      result: prices,
    };
  } catch (error) {
    return {
      error: {
        status: error.response?.status || error?.statusCode || 500,
        message:
          error.response.data?.message ||
          error?.message ||
          "Something went wrong, Please try again",
      },
    };
  }
};

export const getDomainTldsPricing = async (
  _: any,
  domains: { domains: { name: string; tld: string }[] }
) => {
  try {
    const response = await Request("WHMCS").post({
      action: "GetTLDPricing",
      currencyid: 1,
    });
    const { pricing, currency } = response;

    const price = (domain: string) => {
      return {
        renew:
          pricing[domain]?.renew && Object.values(pricing[domain]?.renew)[0],
        register:
          pricing[domain]?.register &&
          Object.values(pricing[domain]?.register)[0],
        transfer: pricing[domain]?.transfer
          ? Object.values(pricing[domain]?.transfer)[0]
          : "0.00",
      };
    };

    const prices: IDomainWithPrice[] = domains.domains?.map((domain) => ({
      ...pricing[domain?.tld],
      ...domain,
      ...price(domain?.tld),
    }));

    return {
      result: prices,
    };
  } catch (error) {
    return {
      error: {
        status: error?.statusCode || 500,
        message: error?.message || "Something went wrong, Please try again",
      },
    };
  }
};

export const getDomainTlds = async (_, args: { tlds: string[] }) => {
  try {
    const response = await Request("WHMCS").post({
      action: "GetTLDPricing",
      currencyid: 1,
    });

    if (Object.values(args).length === 0) {
      const { pricing } = response;

      const tlds = Object.keys(pricing);
      return {
        result: tlds?.map((tld) => ({
          name: `.${tld}`,
          register: pricing[tld]?.register?.["1"],
          transfer: pricing[tld]?.transfer?.["1"] || "0.00",
          renew: pricing[tld]?.renew?.["1"] || "0.00",
        })),
      };
    }

    const tldValue = (key: string) => {
      const value = response?.pricing[key];
      return {
        ...value,
        register: value?.register?.["1"],
        transfer: value?.transfer?.["1"],
        renew: value?.renew?.["1"],
      };
    };

    const tldKeys = Object.keys(response?.pricing).map((tld: string) => ({
      name: tld,
      ...tldValue(tld),
    }));

    return {
      result: tldKeys.filter((d) => args.tlds.includes(`.${d.name}`)),
    };
  } catch (error) {
    return {
      error: {
        status: error?.statusCode || 500,
        message: error?.message || "Something went wrong, Please try again",
      },
    };
  }
};

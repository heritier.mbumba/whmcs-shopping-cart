import Request from "@lib/request";
import { IProduct } from "@lib/types/product";
import { Op } from "sequelize";


/**
 * Get Product groups
 * @param _ 
 * @param groupsIds 
 * @returns 
 */
export const getProductGroups =  async (
  _: any,
  groupsIds: { groups: { type: string; id: number }[] }
) => {
  try {
    // const groups = await ProductGroup.findAll({
    //   where: {
    //     hidden: 0,
    //     id: { [Op.in]: groupsIds?.groups.map((g) => g.id) },
    //   },
    // });

    return null;
  } catch (error) {
    return null;
  }
}

/**
 * Get Products
 * @param _ 
 * @param group 
 * @returns 
 */
export const getProducts =  async (
  _: any,
  group: { gid: number; package: number[]; type?: string, billingCycle?: string }
) => {
  try {
    const response = await Request("WHMCS").post({
      action: "GetProducts",
      ...group,
    });
    if (response?.result !== "success") {
      return {
        error: {
          status: 400,
          message: response?.message
        }
      }
    }

    const { product } = response?.products;

    const typedProduct = product as IProduct[];

    const products = typedProduct
      .map((p) => ({
        ...p,
        defaultBillingCycle: group.billingCycle,
        billingCycles: p.pricing.ZAR,
        category: p.type,
        type: group.type,
      }))
      .filter((p) => group.package.includes(Number(p.pid)));

    return {
      result: products
    };
  } catch (error) {
    return {
      error: {
        status: error?.statusCode || 500,
        message: error?.message || 'Something went wrong, Please try again'
      }
    }
  }
}

/**
 * Get only essentials products
 * @param _ 
 * @param args 
 * @returns 
 */
export const getEssentialProducts =  async (
  _: any,
  args: {essentials: [{ gid: number; package: number[]; type?: string, billingCycle?: string }]}
) => {
  try {
 
    const types = []
    args?.essentials?.forEach((essential) => {
     
      types.push({...essential, request:  Request("WHMCS").post({
        action: "GetProducts",
        ...essential,
      })})
    })
  
    
   const response = await Promise.all(types?.map(res=>res.request))


   const products = response?.map(res=>res.products?.product)?.flatMap(d=>d)

   const result = types?.map(responseObj => {

   
    const resultObj = {
      type: responseObj?.type, 
      products: products?.filter(product=>responseObj?.package?.includes(+product?.pid))?.map(p=>(
        {
          ...p, 
          type: responseObj.type, 
          defaultBillingCycle: responseObj.billingCycle,
          billingCycles: p.pricing.ZAR
        }))
    }

    
    
    

    return resultObj
    
   })?.flatMap(p=>p)

  
   
    return result;
  } catch (error) {
    return {
      error: {
        status: error?.statusCode || 500,
        message: error?.message || 'Something went wrong, Please try again'
      }
    }
  }
}

/**
 * Get Product addons by product id
 * @param _ 
 * @param args 
 */
export const getProductAddons = async (_:any, args: {pid: number, name?: string}) => {
  try {
    
    const response = await Request("WHMCS").post({
      action: 'GetProductAddons',
      ...args
    })

    
    

    if(response?.result !== 'success'){
      return [{
        message: response?.message,
        status: 400
      }]
    }

    return response?.addons
  } catch (error) {
    return [{
      message: error?.message || 'something went wrong',
      status: 400
    }]
  }
}
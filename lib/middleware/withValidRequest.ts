import { NextApiHandler, NextApiRequest, NextApiResponse } from "next";
import { use } from "next-api-middleware";

export default use(
  async (req: NextApiRequest, res: NextApiResponse, nextFn: Function) => {

    return nextFn();
  }
);

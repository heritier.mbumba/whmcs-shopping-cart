import { ICommon, IContacts, Theme } from "Application";
import { IContact, IPageCheck } from "./app";
import { IProductGroup } from "./product";


interface ITheme {
  palette: {
    primary: {
      main: string
    },
    common: {
      light:string,
      black: string,
      grey: string
    },
    secondary: {
      main: string
    }
  },
  fontSize: number,
  logo: string,
  name: string
  layout: string
}

export interface IEssentialProduct {
  id: number;
  type: string;
  supportedPackage?: number[];
  freeDomains?: Record<string, string[]>;
  defaultBillingCycle?: string;
}

export interface ISupportedProductConfig {
  id: number;
  type: string;
  supportedPackage?: number[];
  freeDomains?: Record<string, string[]>;
  defaultBillingCycle?: string;
  essentials?: IEssentialProduct[];
  addons?: number[]
}

export interface IInvoice {
  id: number
  userid: number
  subtotal: string
  total: string
  paymentmethod: string
  created_at: string;
  duedate: string
  __typename?: string
}

interface ILang {
  name: string,
  prefix: string
}
export interface ILanguage {
  locales: string[]
  supportLanguage: Record<string, ILang>[]
  defaultLang: string
}

interface ICheckoutFlow {
  domain?: string[];
  sitebuilder?: string[];
  hosting?: string[];
  email?: string[];
};

export type IGateway = {
  name: string;
  config: {
    marchandid: string
    marchandkey: string
    passphrase?: string
    paymentmethod?: string
  }
  key: string
  icon: string
};

export interface IApplicationConfig {
  theme?: ITheme;
  language?: ILanguage;
  contacts?: {
    general: IContact,
    support?: IContact
  };
  common?: ICommon;
  client?: string;
  supportedProductGroups?: ISupportedProductConfig[];
  supportedDomainTlds?: string[];
  flows?: ICheckoutFlow;
  vat?: number;
  currency?: "ZAR" | "USD";
  gateways?: IGateway[];
  maintenance: boolean;
  user?: {
    invoices: IInvoice[]
  }
  ga?: string;
  resetAppState?: boolean
};
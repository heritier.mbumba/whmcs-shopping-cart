import { IPageCheck } from "./app";

export interface IPageHeader {
  steps?: IPageCheck[];
}

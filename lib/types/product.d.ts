export type ProductType =
  | 'hosting'
  | 'email'
  | 'sitebuilder'
  | 'ssl'
  | 'websecurity'
  | 'cloud-backup'
  | 'linux'
  | 'windows'
  | 'domain essentials'
export type Currency = 'USD' | 'ZAR'
export type IProductType =
  | 'hosting'
  | 'ssl'
  | 'email'
  | 'vps'
  | 'security'
  | string
  | string[]
  | undefined

export type IProductGroupType = string | string[] | undefined
export type IProductPrice = {
  prefix: string
  monthly: number
  annually: number
}

export interface IBillingCycle {
  label: string
  value: number
}

export type Pricing = {
  prefix: string
  suffix: string
  msetupfee: string
  qsetupfee: string
  ssetupfee: string
  asetupfee: string
  bsetupfee: string
  tsetupfee: string
  monthly: string
  quarterly: string
  semiannually: string
  annually: string
  biennially: string
  triennially: string
}

export type IProduct = {
  name: string
  gid: number
  pid: number
  type: ProductType
  category: string
  size?: 'Small' | 'Medium' | 'Large'
  iconUrl?: string
  config?: {
    type: string
  }
  description: string
  pricing: {
    USD: Pricing
    ZAR: Pricing
  }
  defaultBillingCycle?: string
  billingCycles?: Record<string, any>
  freeDomains?: string[]
  inPromotion?: boolean
  discount?: number
  customfields?: { customfield: any[] }
  configoptions?: { configoption: any[] }
  addons?: IProductAddons[]
  domaintype?: string
  regperiod?: number | null
}

export type IProductAddons = {
  id: number
  name: string
  pricing: Pricing
};

export type IProductGroup = {
  id: number
  name: string
  description?: string
  type: string
  products?: IProduct[]
  hidden?: number
}

export type TransformProductType = {
  type: string
  description?: string
  params?: {
    priceStartFrom: number
    mostPopular: boolean
    guarantee: string
  }
  products: IProduct[]
}

export type SupportedGroupType = {
  id: number
  type: string
  supportedPackage?: number[]
  freeDomains?: Record<string, string[]>
}

export interface IStep {
  label: string
  checked: boolean
  key: string
  passed: boolean
  fullfilled: boolean
}

export interface IProductStep {
  domain?: IStep[]
  vps?: IStep[]
  email?: IStep[]
  hosting?: IStep[]
  ssl?: IStep[]
  reseller?: IStep[]
  dedicatedserver?: IStep[]
  businessvps?: IStep[]
  webdesign?: IStep[]
  webrank?: IStep[]
}

export interface ProductItemProps {
  product: IProduct
  description?: string
  domains?: IDomainWithPrice[]
  onDomainSelect?: (domain: IDomainWithPrice) => void
  onSelect?: () => void
}

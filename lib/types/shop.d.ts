import { IDomainWithPrice } from "./domain";
import { IProduct } from "./product";

export interface ICartItem {
  domain: IDomainWithPrice;
  products: IProduct[];
  domaintype?: string[]
  regperiod?: string[]

};


declare global {
  interface Window {
    payfast_do_onsite_payment: (
      { uuid: string },
      callback: (result: boolean) => void
    ) => void;
    payfast_close_payment_popup: (e: MessageEvent) => void;
    payfast_do_finish_callback: (...args: any[]) => void;
    dataLayer: Record<string, any>
  }
}
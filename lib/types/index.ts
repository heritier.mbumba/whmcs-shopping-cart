export type ThemeType = {
    name: string
    colors: {
        primary: string,
        secondary: string
    },
    logo: string
}
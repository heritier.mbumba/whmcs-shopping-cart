import React from "react";

export type IDomain = {
  name: string;
  tld: string;
  status: number;
  premium: boolean;
  sld: string;
  __typename?: string;
};

export type IDomainForPrice = {
  name: string;
  tld: string;
  status: number;
};

export interface IDomainWithPrice extends IDomain {
  categories: string[];
  addons?: {
    dns: boolean;
    email: boolean;
    idprotect: boolean;
  };
  renew: string;
  register: string;
  transfer?: string;
  eppcode?: string;
  type?: "register" | "transfer" | "existing";
  freeDomain?: boolean
};

export interface IDomainUsedWithPrice extends IDomainWithPrice {
  used: boolean;
}

export type IDomainTld = {
  renew: string;
  register: string;
  name: string;
  transfer: string;
};

export interface IDomainSearchForm {
  tlds: IDomainTld[];
  loading?: boolean;
  onFormSubmitHandler: (event: React.FormEvent<HTMLFormElement>) => void;
  onValueChange: (
    event: React.FormEvent<HTMLInputElement> | string,
    key: string
  ) => void;
  selectedTld?: string;
  onSelectTld?: (tld: IDomainTld, key?: string) => void;
  error?: Record<string, any>;
  onClearError?: () => void;
  selectedTlds?: string[]
}

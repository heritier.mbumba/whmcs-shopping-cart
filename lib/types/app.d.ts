import { IGateway } from "./api";
import { SessionUser } from "./auth";

interface IPalette {
    main: string;
    contrastText?: string
}

export interface IContact {
    email: string;
    telephone: string;
    businessHours?: {
        time: string;
        days: string
    }
}

export interface Theme {
    palette: {
        primary: IPalette
        common : {
            light: string;
            black: string;
            grey: string
        }
        secondary : IPalette
    }
    fontSize: number | string
    logo: string
    logoSize?: number
    name: string
}

export interface IContacts {
    general: IContact,
    support?: IContact
}

export interface ICommon {
    googleAnalytics?: number;
}

interface IAuth {
    alreadyExpired: boolean
    user: SessionUser
  }

export interface PageProps {
  pathname?: string;
  contacts?: IContacts;
  title: string;
  description: string;
  children?: JSX.Element | JSX.Element[] | React.FC | React.ReactNode;
  supportedProductGroups?: string[];
  client?: string;
  gateways?: IGateway[]
  auth?: IAuth
  withSteps?: boolean
}

export interface IPageCheck {
  label: string;
  checked: boolean;
  key: string;
  passed: boolean;
  fullfilled: boolean;
}
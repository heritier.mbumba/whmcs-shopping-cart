import { User } from "next-auth";

export enum AuthType {
  LOGIN = "LOGIN",
  REGISTER = "REGISTER",
}

export type LoginUserType = {
  type: AuthType.LOGIN;
  email: string;
  password2: string;
  csrfToken: string;
};

export type RegisterUserType = {
  type: AuthType.REGISTER;
  email: string;
  password2: string;
  csrfToken: string;
  firstname: string;
  lastname: string;
};

export type IClient = {
  userid: number;
  firstname: string;
  lastname: string;
  companyname: string;
  email: string;
  datecreated: Date;
  groupid: number;
  status: string;
};

export interface SessionUser extends User {
  userid: number;
  paymentmethod?: string;
  lastname: string
  credit?: string
  invoiceObj?: Record<string, any>[]
}

export interface IAddNewClient {
  firstname: string
  lastname: string
  email: string
  password2: string
  address1: string
  city: string
  state: string
  postcode: string
  country: string
  phonenumber: string
  noemail?: boolean
}



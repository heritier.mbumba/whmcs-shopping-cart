import axios, { AxiosInstance } from "axios";

const DOMAIN_API = axios.create({
  baseURL: process.env.DOMAIN_MIDDLEWARE_ENDPOINT,
});
const WHMCS_API = axios.create({
  baseURL: process.env.WHMCS_API_HOSTNAME,
});

type RequestType = "DOMAIN" | "WHMCS";

export default function Request(type: RequestType) {
  let req: AxiosInstance;
  if (type === "DOMAIN") {
    req = DOMAIN_API;
  } else {
    req = WHMCS_API;
  }

  req.interceptors.request.use((config) => {
    config.headers = {
      "Content-Type": "application/json"
    }

  
    
    return config;
  });

  req.interceptors.response.use(config=>{
  
    
    return config
  })

  

  return {
    post: async (payload: Record<string, any>) => {
      try {
        // let timerOut = setTimeout(()=>{
        //   return Promise.reject({status: 408, message: 'Request timeout'})
        // }, 30000)
        if (type === "WHMCS") {
          const { data } = await req.post(
            "",
            {},
            {
              params: {
                ...payload,
                username: process.env.WHMCS_API_USERNAME,
                password: process.env.WHMCS_API_PASSWORD,
                accesskey: process.env.WHMCS_API_ACCESS_KEY,
                responsetype: "json",
              },
            }
          );
          return data;
        }
        const { data } = await req.post("/v1/domains/search", payload);

        // clearTimeout(timerOut)
        return data;
      } catch (error) {
       
        return Promise.reject(error)
      }
    },
    get: async () => {
      try {
        const {} = await req.get("");
      } catch (error) {}
    },
  };
}

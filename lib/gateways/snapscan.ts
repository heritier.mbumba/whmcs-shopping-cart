class SnapScan {
  constructor(
    private marchand_id: string,
    private marchand_key: string,
    private passphrase: string,
    private on_page_url: string,
    private notify_url: string
  ) {}
}

export default SnapScan;

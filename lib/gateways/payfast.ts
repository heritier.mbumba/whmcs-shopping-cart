class Payfast {
  constructor(
    private marchand_id: string,
    private marchand_key: string,
    private passphrase: string,
    private on_page_url: string,
    private notify_url: string
  ) {}

  /**
   * Get UUI from payfast
   * @param data
   */
  async getUniqueIdentifier(data: Record<string, any>[]) {
    try {
    } catch (error) {}
  }

  /**
   * Generate payfast signature
   */
  async generateSignature() {
    try {
    } catch (error) {}
  }
}

export default Payfast;

class Domain {
  supportedExtension: Record<string, any>[];
  constructor(private endpoint: string) {
    this.getSupportedExtension();
  }

  /**
   * Search domain
   * @param domain
   */
  async search(domain: string) {
    try {
      const domains = this.getDomainWithOtherExtension(domain);
    } catch (error) {}
  }

  /**
   * Get all supported domain extension
   * @returns
   */
  private async getSupportedExtension() {
    return this.supportedExtension.push({
      name: ".co.za",
      prices: {
        register: 99.0,
        transfer: 99.0,
        renew: 99.0,
      },
    });
  }

  /**
   * get searched domain with different extension
   * @param domain
   */
  private getDomainWithOtherExtension(domain: string) {
    try {
      const [name, ...extensions] = domain.split(".");
    } catch (error) {}
  }
}

export default new Domain(process.env.DOMAIN_MIDDLEWARE_ENDPOINT!);

import { IProduct, IProductGroup, IProductAddons } from "../types/product";

export default interface Product {
  getProducts?: (groupid: number) => Promise<IProduct[]>;

  getProductGroups?: () => Promise<IProductGroup[]>;

  getproductsAddons?: (productId: number) => IProductAddons;

  getDiscountedProducts?: () => Promise<IProduct[]>;
}

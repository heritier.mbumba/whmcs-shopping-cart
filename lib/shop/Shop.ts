import WHMCS from "../whmcs";

class Shop {
  constructor(private whmcs = WHMCS) {}

  async getProducts(gid: string) {
    try {
      return {
        data: await this.whmcs.post("GetProducts", { gid }),
      };
    } catch (error) {
      return error;
    }
  }
}

export default new Shop();

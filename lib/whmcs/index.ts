import { IUserRgister } from "@app/auth/auth.dto";
import axios from "axios";
import { IClient } from "../types/auth";

const WHMCSAPI = axios.create({
  baseURL: process.env.WHMCS_API_HOSTNAME,
});

class WHMCS {
  async post(action: string, payload: Record<string, any>) {
    try {
      WHMCSAPI.interceptors.request.use((config) => {
        config.params["username"] = process.env.WHMCS_API_USERNAME;
        config.params["password"] = process.env.WHMCS_API_PASSWORD;
        config.params["responsetype"] = "json";

        return config;
      });

      const { data } = await WHMCSAPI.post(
        "",
        {},
        {
          params: {
            ...payload,
            action,
          },
        }
      );

      const { result, totalresults, ...results } = data;

      return results;
    } catch (error) {
      return Promise.reject({
        message: error?.message || "Something went wrong",
        status: error?.status || 500,
      });
    }
  }

  async getClient(email: string): Promise<IClient | null> {
    try {
      const clients = await this.post("GetClients", { search: email });
      if (clients.result !== "success") {
        return null;
      }
      const client = clients?.client[0];
      return {
        userid: client.id,
        ...client,
      } as IClient;
    } catch (error) {
      return null;
    }
  }

  async checkAuth(email: string, password2: string) {
    try {
      //
      return await this.post("ValidateLogin", { email, password2 });
    } catch (error) {}
  }

  async registerUser(
    user: Record<string, any>
  ): Promise<{ userid: number } | null> {
    try {
      //
      const newClient = await this.post("AddUser", { ...user });
      if (newClient.result !== "success") {
        return null;
      }
      return {
        userid: newClient.user_id,
      };
    } catch (error) {
      return null;
    }
  }

  async applyUserCredit(invoiceid: number, amount: number) {
    try {
      //
    } catch (error) {}
  }

  async getUserById(id: number) {
    try {
      //
      return id;
    } catch (error) {
      return error;
    }
  }

  async getUserByEmail(email: string) {
    try {
      //
      return email;
    } catch (error) {
      return error;
    }
  }

  async validateLoginDetail(email: string, password2: string) {
    try {
      const client = await this.post("ValidateLogin", { email, password2 });

      if (client.result !== "success") {
        return {
          message: "Invalid email or password",
          status: 400,
          isValid: false,
        };
      }
      return {
        userid: client.userid,
        isValid: true,
      };
    } catch (error) {
      return {
        message: "Internal server error",
        status: 500,
        isValid: false,
      };
    }
  }
}

const whmcs = new WHMCS();

export const getUserById = async (id: number) => {
  try {
    const user = await whmcs.getUserById(id);
    if (!user) {
      return Promise.reject({ status: 404, message: "Client not registered" });
    }
    return user;
  } catch (error) {
    return error;
  }
};
export const getUserByEmail = async (email: string) => {
  try {
    const user = await whmcs.getUserByEmail(email);
    if (!user) {
      return Promise.reject({ status: 404, message: "Client not registered" });
    }
    return user;
  } catch (error) {
    return error;
  }
};

export const validateLoginDetail = whmcs.validateLoginDetail;

export default whmcs;

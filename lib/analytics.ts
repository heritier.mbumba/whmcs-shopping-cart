import { getProRataAmount } from '@app/redux/dispatches/shopDispatcher'
import { IOrderSummary } from '@app/redux/reducers/cart.reducer'
import { IDomainWithPrice } from './types/domain'
import { IProduct } from './types/product'
import { ICartItem } from './types/shop'

const ECOMMERCE = 'ecommerce'

enum Events {
  ADD_TO_CART = 'addToCart',
  CHECKOUT = 'checkout',
  PURCHASE = 'purchase',
  REMOVE_TO_CART = 'removeFromCart',
  PRODUCT_CLICK = 'productClick',
  CHECKOUT_OPTION = 'checkoutOption',
  PAGE_VIEW = 'pageview'
}

interface GTMProduct {
  type: string
  price: string
  name: string
  id: string
  category: string
  currencyCode: string
  quantity: number
  coupon: string
}

interface IEvent {
  event: Events
  category?: string
  label?: string;
  value?: number;
  ecommerce?: {
    currencyCode?: string
    checkout_option?: {
      actionField: Record<string, any>
    }
    add?: {
      products: GTMProduct[]
      value?: number
    }
    click?: {
      actionField: Record<string, any>
      products: GTMProduct[]
    }
    remove?: {
      products: GTMProduct[]
    }
    checkout?: {
      actionField: Record<string, any>
      products: GTMProduct[]
    }
    purchase?: {
      actionField: Record<string, any>
      products: GTMProduct[]
    }
  }
  page?: {
    path: string
    title: string
  }
}

export enum CHECKOUT_STEPS {
  'CART_ENTRY' = 1,
  'SELECTED_CART_VARIANT' = 2,
  'SELECTED_DOMAIN' = 3,
  'AUTHENTICATED' = 4,
  'CHECKED_OUT' = 5,
  'PURCHASE' = 6
}

export class GoogleAnalytics {
  private pushEvent(eventData: IEvent) {
    /**@ts-ignore */
    window && window.dataLayer && window.dataLayer.push({ ecommerce: null })
    window && window.dataLayer && window.dataLayer.push(eventData)
  }

  private formatProduct(item: IProduct) {
    return {
      name: item?.name,
      id: String(item?.pid),
      price: String(getProRataAmount(item?.pricing?.['ZAR']?.[item?.defaultBillingCycle], item?.defaultBillingCycle)),
      category: item?.type || 'product',
      type: item?.type,
      coupon: '',
      currencyCode: '1',
      quantity: 1
    }
  }

  private formatItems(items: ICartItem[]): GTMProduct[] {
    const domains: GTMProduct[] = items?.map((item) => ({
      name: item.domain.name,
      id: item.domain.tld,
      price: this.domainPrice(item.domain),
      category: 'Domain',
      type: item?.domain?.status ? 'domain-register' : 'domain-transfer',
      coupon: '',
      currencyCode: 'ZAR',
      quantity: 1
    }))
    const products: GTMProduct[] = items
      ?.map((item) =>
        item?.products?.map((product) => ({
          name: product.name,
          id: String(product.pid),
          price: String(getProRataAmount(product?.pricing?.['ZAR']?.[product.defaultBillingCycle], product.defaultBillingCycle)),
          category: String(product?.category || product?.type || 'Web Hosting'),
          type: product.type,
          coupon: '',
          currencyCode: '1',
          quantity: 1
        }))
      )
      .flatMap((d) => d)

    return products.concat(domains)
  }

  private domainPrice(domain: IDomainWithPrice) {
    if (domain?.status) {
      return domain?.register
    } else if (!domain?.status) {
      return domain.transfer
    }
    return '0.00'
  }

  /**
   * 
   */
  cartInit(){
    this.pushEvent({
      event: Events.CHECKOUT,
      category: ECOMMERCE,
      ecommerce: {
        currencyCode: 'ZAR',
        click: {
          actionField: {
              step: 1,
              option: 'Cart Entry'
          },
          products: []
        }
      }
    })
  }

  /**
   * 
   * @param items 
   * @param total 
   */
  checkout(step: number, option: string, items?: ICartItem[], total?: number) {

    if(step < 7){
      this.pushEvent({
        event: Events.CHECKOUT,
        category: ECOMMERCE,
        label: Events.CHECKOUT,
        value: total,
        ecommerce: {
          currencyCode: 'ZAR',
          checkout: {
            // actionField: { step: 'last', option: 'pay' },
            actionField: { step, option, label: option },
            products: []
          }
        }
      })
    }else {
      this.pushEvent({
        event: Events.CHECKOUT,
        category: ECOMMERCE,
        label: Events.CHECKOUT,
        value: total,
        ecommerce: {
          currencyCode: 'ZAR',
          checkout: {
            // actionField: { step: 'last', option: 'pay' },
            actionField: { step, option: 'purchase',  label:'purchase'},
            products: this.formatItems(items)
          }
        }
      })
    }

    
  }

  /**
   * 
   * @param domains 
   */
  addDomainToCart(domains: IDomainWithPrice[]) {
    const products = domains?.map((domain) => ({
      type: 'domain',
      id: domain?.tld,
      price: domain?.status ? domain?.register : domain?.transfer,
      category: domain?.status ? 'domain-register' : 'domain-transfer',
      currencyCode: '1',
      quantity: 1,
      coupon: '',
      name: domain?.name
    }))
    this.pushEvent({
      event: Events.ADD_TO_CART,
      category: ECOMMERCE,
      label: Events.ADD_TO_CART,
      ecommerce: {
        currencyCode: 'ZAR',
        add: {
          products
        }
      }
    })
  }

  /**
   * 
   * @param item 
   */
  removeItemToCart(item: ICartItem) {
    const { domain, products } = item
    const productsArr = [
      {
        type: 'domain',
        id: domain?.tld,
        price: domain?.status ? domain?.register : domain?.transfer,
        category: domain?.status ? 'domain-register' : 'domain-transfer',
        currencyCode: '1',
        quantity: 1,
        coupon: '',
        name: domain?.name
      }
    ]

    if (products?.length > 0) {
      products.forEach((product) => {
        productsArr.push({
          type: product?.type,
          id: String(product.pid),
          price: String(getProRataAmount(product?.pricing?.['ZAR']?.[product.defaultBillingCycle], product.defaultBillingCycle)),
          category: String(product?.category || product?.type || 'product'),
          currencyCode: '1',
          quantity: 1,
          coupon: '',
          name: product?.name
        })
      })
    }

    this.pushEvent({
      event: Events.REMOVE_TO_CART,
      ecommerce: {
        remove: {
          products: productsArr
        }
      }
    })
  }

  /**
   * 
   * @param product 
   */
  addProductToCart(product: IProduct) {
    const productGMT: GTMProduct = {
      type: product?.type,
      id: String(product.pid),
      price: product?.pricing?.ZAR[product?.defaultBillingCycle],
      category: String(product?.category || product?.type || 'product'),
      currencyCode: '1',
      quantity: 1,
      coupon: '',
      name: product?.name
    }
    this.pushEvent({
      event: Events.ADD_TO_CART,
      category: ECOMMERCE,
      label: Events.ADD_TO_CART,
      ecommerce: {
        currencyCode: 'ZAR',
        add: {
          products: [{...productGMT}],
          value: Number(productGMT?.price)
        }
      }
    })
  }

  /**
   * 
   * @param product 
   */
  removeProductToCart(product: IProduct) {
    this.pushEvent({
      event: Events.REMOVE_TO_CART,
      ecommerce: {
        remove: {
          products: [this.formatProduct(product)]
        }
      }
    })
  }

  /**
   * 
   * @param type 
   * @param payload 
   */
  click(type: string, payload: Record<string, any>) {
    this.pushEvent({
      event: Events.PRODUCT_CLICK,
      ecommerce: {
        click: {
          actionField: { [type]: '' },
          products: [this.formatProduct(payload as IProduct)]
        }
      }
    })
  }

  /**
   * 
   * @param step 
   * @param checkoutOption 
   */
  checkoutOption(step: number, checkoutOption?: any) {
    this.pushEvent({
      event: Events.CHECKOUT_OPTION,
      ecommerce: {
        checkout_option: {
          actionField: {
            step: step,
            option: checkoutOption
          }
        }
      }
    })
  }

  /**
   * 
   * @param pathname 
   * @param title 
   */
  pageView(pathname: string, title?: string) {
    this.pushEvent({
      event: Events.PAGE_VIEW,
      page: {
        path: pathname,
        title: title || 'Express checkout'
      }
    })
  }

  /**
   * 
   * @param action 
   * @param items 
   * @param orderId 
   * @param orderSummary 
   * @param coupon 
   */
  success(action: string, items: ICartItem[], orderId: number, orderSummary: IOrderSummary, coupon: string) {
    this.pushEvent({
      event: Events.PURCHASE,
      ecommerce: {
        purchase: {
          actionField: { 
            id: orderId,                         // Transaction ID. Required for purchases and refunds.
            affiliation: 'Express checkout',
            revenue: orderSummary?.total.toFixed(2),                     // Total transaction value (incl. tax and shipping)
            tax: orderSummary?.vat.toFixed(2),
            coupon
           },
          products: this.formatItems(items)
        }
      }
    })

    this.pushEvent({
      event: Events.CHECKOUT,
      ecommerce: {
        checkout: {
          actionField: {step: 7, option: action},
          products: this.formatItems(items)
        }
      }
    })
  }
}

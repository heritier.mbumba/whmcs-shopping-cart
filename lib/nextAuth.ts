import axios from "axios"
import { getCsrfToken } from "next-auth/client"

const serializeFormParameters = (paramsObject: Record<string, any>) =>
  Object.entries(paramsObject)
    .map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
    .join('&')

export const nextAuthLogin = async (user: {email: string; password2: string}) => {
  try {
    const {data} = await axios.post('/api/auth/callback/whmcs', serializeFormParameters({...user, csrfToken: await getCsrfToken(), json: true}))
    
    if(data?.url?.status === 400){
      return Promise.reject({...data})
    }
    
    // throw new Error(JSON.parse(data)?.error || 'Something went wrong')
    
  } catch (error) {
    return Promise.reject({message: error?.message || 'Something went wrong', status: error?.status || 500})
  }
}


#Base image
FROM node:alpine

#application directory
WORKDIR /app


#Copy application package
COPY package.json .

#Install all application dependencies
RUN npm install

#Copy all project
COPY . .

#Build the application
RUN npm run build

ENV NODE_ENV production

EXPOSE 3000

CMD npm run start

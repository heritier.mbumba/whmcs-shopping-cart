type Endpoint = {
    config: string
}

const ENDPOINT = {
    1: {
        config: ['/api', 'v1', 'config'].join('/')
    }
}
export const getEndpoint = (version: number): Endpoint => {
    return {
        ...ENDPOINT[version]
    }
}
import { gql } from "@apollo/client";

export const GET_PRODUCTS = gql`
  query GetProducts(
    $groupid: Int
    $package: [Int]
    $type: String
    $billingCycle: String
  ) {
    getProducts(
      gid: $groupid
      package: $package
      type: $type
      billingCycle: $billingCycle
    ) {
      result {
        name
        pid
        type
        pricing {
          ZAR {
            monthly
            annually
            semiannually
            quarterly
          }
        }
        billingCycles {
          monthly
          annually
          semiannually
          quarterly
        }
        defaultBillingCycle
        gid
      }
      error {
        status
        message
      }
    }
  }
`;

export const GET_ESSENTIALS_PRODUCTS = gql`
  query GetEssentialProducts($essentials: [EssentialProduct]) {
    getEssentialProducts(essentials: $essentials) {
      type
      products {
        name
        pid
        type
        pricing {
          ZAR {
            monthly
            annually
            semiannually
            quarterly
          }
        }
        billingCycles {
          monthly
          annually
          semiannually
          quarterly
        }
        defaultBillingCycle
        gid
      }
      error {
        status
        message
      }
    }
  }
`;

export const SEARCH_DOMAIN = gql`
  query SearchDomains($domains: [String]) {
    searchDomains(domain: $domains) {
      result {
        tld
        name
        renew
        register
        transfer
        status
        categories
      }
      error {
        status
        message
      }
    }
  }
`;

export const SEARCH_ALTERNATIVE_DOMAIN = gql`
  query SearchAlternativeDomains($domains: [String]) {
    searchAlternativeDomains(domain: $domains) {
      result {
        tld
        name
        renew
        register
        transfer
        status
        categories
      }
      error {
        status
        message
      }
    }
  }
`;

export const GET_DOMAIN_PRICING = gql`
  query GetDomainPricing($domains: [DomainForPrice]) {
    getDomainTldsPricing(domains: $domains) {
      result {
        categories
        addons {
          dns
          email
          idprotect
        }
        renew
        register
        transfer
        tld
        name
        status
      }
      error {
        status
        message
      }
    }
  }
`;

export const GET_DOMAIN_TLDS = gql`
  query GetDomainTld($tlds: [String]) {
    getDomainTlds(tlds: $tlds) {
      result {
        renew
        register
        name
        transfer
      }
      error {
        status
        message
      }
    }
  }
`;

export const PRODUCT_GROUPS = gql`
  query GetProductGroups($groups: [PGroup]) {
    getProductGroups(groups: $groups) {
      id
      name
      hidden
    }
  }
`;

export const ADD_ORDER = gql`
  query Checkout($order: AddOrderInput) {
    addOrder(order: $order) {
      result {
        invoiceid
        orderid
      }
      error {
        status
        message
      }
    }
  }
`;

export const ADD_CLIENT = gql`
  query AddClient($client: NewClient) {
    addClient(client: $client) {
      result {
        clientid
      }
      error {
        status
        message
      }
    }
  }
`;

export const GET_UNPAID_INVOICE = gql`
  query GetInvoice($id: Int, $client: NewClient) {
    getInvoice(id: $id, client: $client) {
      result {
        snapscan {
          invoiceid
          total
          taxrate
          tax
          paymentmethod
          status
          userid
          verifyHash
        }
        payfast {
          merchant_id
          merchant_key
          notify_url
          name_first
          name_last
          email_address
          m_payment_id
          amount
          item_name
          payment_method
          signature
        }
        paymentidentifier
      }
      error {
        status
        message
      }
    }
  }
`;

export const GET_CLIENT_INVOICES = gql`
  query GetClientInvoices($clientid: Int) {
    getClientInvoices(clientid: $clientid) {
      ... on ClientInvoice {
        id
        userid
        subtotal
        total
        paymentmethod
        created_at
        duedate
      }
      ... on Error {
        status
        message
      }
    }
  }
`;

export const GET_PROMOTIONS = gql`
  query GetPromotions($code: String) {
    getPromotions(code: $code) {
      ... on Promotion {
        id
        code
        type
        recurring
        value
        cycles
        appliesto
        requires
        requiresexisting
        startdate
        expirationdate
        maxuses
        uses
        lifetimepromo
        applyonce
        newsignups
        existingclient
        onceperclient
        recurfor
        upgrades
        upgradeconfig
      }
      ... on PromotionNotFound {
        message
        code
      }
    }
  }
`;

export const GET_PRODUCT_ADDONS = gql`
  query GetProductAddons($product: AddonProductInput) {
    getProductAddons(product: $product) {
      ... on AddonProduct {
        id
        name
        pricing {
          prefix
          suffix
          msetupfee
          monthly
          annually
          semiannually
        }
      }
      ... on Error {
        status
        message
      }
    }
  }
`;

export const ADD_GCLID = gql`
  query AddGclid($orderid: Int, $gclid: String) {
    addGCLID(orderid: $orderid, gclid: $gclid) {
      result {
        success
      }
      error {
        status
        message
      }
    }
  }
`;

import { ProductType } from "@lib/types/product";
import useTranslate from "@hooks/useTranslate";
import { IApplicationConfig } from "@lib/types/api";

export const pageMetaDefault = {
  home: {
    title: "Domain Name Registration, Web Hosting, & Web Security | 1-grid",
    description: "",
  },
  cart: {
    title: "Domain Name Registration, Web Hosting, & Web Security | 1-grid",
    description: "",
  },
  product: {
    title: "Domain Name Registration, Web Hosting, & Web Security | 1-grid",
    description: "",
  },
  domain: {
    title: "Domain Name Registration, Web Hosting, & Web Security | 1-grid",
    description: "",
  },
};

export const productDescriptions = {
  hosting: {
    124: {
      description: "",
      features: ["Host 2 websites", "5GB storage", "Unlimited mail accounts"],
    },
    532: {
      description: "",
      features: [
        "Free .co.za domain",
        "Host 5 websites",
        "25GB storage, etc...",
      ],
    },

    525: {
      description: "",
      features: ["4GB Storage", "Host 1 websites", "75 mail accounts, etc..."],
    },
  },
  sitebuilder: {
    572: {
      description: "",
      features: ["Free .co.za domain", "10 pages"],
    },
    573: {
      description: "",
      features: [
        "Free .store(.co.za) domain",
        "E-commerce",
        "35 pages, 10 products",
      ],
    },
    574: {
      description: "",
      features: [
        "Free .store(.co.za) domain",
        "E-commerce",
        "Unlimited products",
      ],
    },
  },
  email: {
    536: {
      description: "",
      features: [
        "Free .store(.co.za) domain",
        "E-commerce",
        "Unlimited products",
      ],
    },
    537: {
      description: "",
      features: [
        "Free .store(.co.za) domain",
        "E-commerce",
        "Unlimited products",
      ],
    },
  },
  ssl: {
    561: {
      description: "",
      title: "Domain SSL",
      features: [
        "Free .store(.co.za) domain",
        "E-commerce",
        "Unlimited products",
      ],
    },
    562: {
      description: "",
      title: "Wildcard domain",
      features: [
        "Free .store(.co.za) domain",
        "E-commerce",
        "Unlimited products",
      ],
    },
  },
  "cloud-backup": {
    581: {
      description: "",
      features: [
        "Free .store(.co.za) domain",
        "E-commerce",
        "Unlimited products",
      ],
    },
  },
};

export const Config = {
  gtmTrackingCode: process.env.GTM_TRACKING_ID,
  uaTrackingCode: process.env.UA_TRACKING_ID,
};

export interface IDomainCheckoutOptions {
  label: string;
  description?: string;
  key: string;
  optionName?: string;
  overview?: {
    pricing?: number;
    description?: string;
    billingcycle?: string;
  };
}

export const CHECKOUT_OPTIONS = (): IDomainCheckoutOptions[] => {
  const { t } = useTranslate(); //translation helper function

  const CHECKOUT_OPTIONS: IDomainCheckoutOptions[] = [
    {
      label: t("checkoutOptions[wb][label]"),
      key: "sitebuilder",
      optionName: t("checkoutOptions[wb][name]"),
      overview: {
        pricing: 109,
        billingcycle: "Monthly",
      },
    },
    {
      label: t("checkoutOptions[hosting][label]"),
      key: "hosting",
      optionName: t("checkoutOptions[hosting][name]"),
      overview: {
        pricing: 99,
        billingcycle: "Monthly",
      },
    },
    {
      label: t("checkoutOptions[email][label]"),
      key: "email",
      optionName: t("checkoutOptions[email][name]"),
      overview: {
        pricing: 49,
        billingcycle: "Monthly",
      },
    },
  ];
  return CHECKOUT_OPTIONS;
};

export const FLOW_TITLE = () => {
  const { t } = useTranslate(); //translation helper function

  const FLOW_TITLE = {
    sitebuilder: {
      package: {
        title: t("flowTitle[sitebuilder][package]"),
        description: t("flowTitle[sitebuilder][description]"),
      },
      essentials: {
        title: t("flowTitle[hosting][essentials]"),
        description: "",
      },
    },
    hosting: {
      package: {
        title: t("flowTitle[hosting][package]"),
        description: "",
      },
      essentials: {
        title: t("flowTitle[hosting][essentials]"),
        description: "",
      },
    },
    email: {
      package: {
        title: t("flowTitle[email][package]"),
        description: "",
      },
    },
  };
  return FLOW_TITLE;
};

export const REGEX_EMAIL = () => {
  const re =
    /(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

  return re;
};

export const REGEX_POSTAL = () => {
  return /^[a-zA-Z0-9-?]*$/;
};
export const REGEX_PHONE_NUMBER = () => {
  return /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
};

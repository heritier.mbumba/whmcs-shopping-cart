import Page from "@components/HOC/Page";
import { PageProps } from "@lib/types/app";
import { NextPage } from "next";
import pageStyles from '../src/containers/page.module.scss'

interface NotFoundPageProps extends PageProps {}



const NotFoundPage: NextPage<NotFoundPageProps> = (props) => 
{
  const {client, ...otherProps} = props
  return <Page {...otherProps}>
    <section className={pageStyles.page}>
      <div className="container">
        <div className="col-xs-12 col-md-6 offset-md-3 text-center">
          <h1 className="title-5 title-f-400">404</h1>
          <h4 className="title-2 title-f-300">Sorry, this page was not found</h4>
        </div>
      </div>
    </section>
  </Page>
}


export default NotFoundPage
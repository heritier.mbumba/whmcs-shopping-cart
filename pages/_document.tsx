import * as React from 'react'
import Document, { Html, Head, Main, NextScript, DocumentContext } from 'next/document'
import { Config } from '@utils/default'

class MyDocument extends Document {
  static async getInitialProps(ctx:DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {

    return (
      <Html lang="en">
        <Head>
          <meta property="og:image" content=""/>
          <meta property="og:description" content="Register your domain for only R89 and get the best Web Hosting in South Africa. We also offer Company Registration with the CIPC"/>
          <meta property="og:title" content="Domain Name Registration, Web Hosting, Website Design, Company Registration" />
          <meta name="twitter:title" content="Domain Name Registration, Web Hosting, Website Design, Company Registration"></meta>
          <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
          <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/favicon144.png" />
          <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/favicon114.png" />
          <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/favicon72.png" />
          <link rel="apple-touch-icon-precomposed" href="/img/favicon57.png" />
          <link rel="shortcut icon" href="/images/favicon-32x32.png" />
          <script src="https://www.payfast.co.za/onsite/engine.js" async></script>
          <meta name="msapplication-navbutton-color" content="#20bfdd" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
          <meta name="author" content="1-grid.com" />
          <script src={`https://www.googletagmanager.com/gtag/js?id=${Config.gtmTrackingCode}`} async></script>
          <script
            dangerouslySetInnerHTML={{
              __html: `
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());
                    gtag('config', '${Config.gtmTrackingCode}');
                `,
            }}
          />
          {!!Config.gtmTrackingCode && (
            <script
              dangerouslySetInnerHTML={{
                __html: `
                      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','${Config.gtmTrackingCode}');
                      
                    `,
              }}
            ></script>
          )}
        </Head>
        <body>
        {!!Config.gtmTrackingCode && (
            <>
              <noscript
                dangerouslySetInnerHTML={{
                  __html: `
                    <iframe src="https://www.googletagmanager.com/ns.html?id=${Config.gtmTrackingCode}" height="0" width="0" style=""></iframe>
                    `,
                }}
              ></noscript>
            </>
          )}
          <Main />
          <NextScript>
          </NextScript>
        </body>
      </Html>
    )
  }
}

export default MyDocument
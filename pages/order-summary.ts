import SummaryPage from "@containers/summary.page";

export default SummaryPage

SummaryPage.getInitialProps = async () => {
    return {
        title: 'Order Summary',
        description: 'Order Summary description',
        withSteps: false
    }
}
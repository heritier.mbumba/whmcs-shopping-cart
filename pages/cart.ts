import CartPage from "@containers/cart.page";
import { GetServerSideProps } from "next";

export default CartPage


export const getServerSideProps: GetServerSideProps = async ({req}) => {

 
  return {
    props : {
      title: "Checkout page",
      description: "checkout page description",
      withSteps: true
    }
  }
}
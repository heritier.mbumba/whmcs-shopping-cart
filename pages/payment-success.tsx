import Page from '@components/HOC/Page'
import { PageProps } from '@lib/types/app'
import { HiOutlineCheckCircle } from 'react-icons/hi'
import { Colors } from 'src/constant'
import useTranslate from 'src/hooks/useTranslate'
import Link from 'next/link'

interface PaymentSuccessPageProps extends PageProps {}

/**
 * Component that handles the UI for the successful payment page
 * @param props
 * @returns JSX element that provides a successful payment UI
 */
export default function PaymentSuccessPage({
  client,
  gateways,
  ...otherProps
}: PaymentSuccessPageProps) {
  const { t } = useTranslate() //translation helper function

  return (
    <Page {...otherProps}>
      <section className='payment__sucess pt-5'>
        <div className='container'>
          <div className='col-md-6 col-xs-12 offset-md-3 text-center pb-5'>
            <p>
              <HiOutlineCheckCircle color={Colors.success} size={50} />
            </p>
            <h1 className='title-2 title-f-300 color-danger pt-3'>
              {t('page[success][title]')}
            </h1>
            <span className='d-block mb-5'>
              {t('page[success][description]')}
            </span>
            <Link href='https://1-grid.com/client/clientarea.php?action=invoices'>
              <a className='btn btn-pink'>{t('user[zone]')}</a>
            </Link>
          </div>
        </div>
      </section>
    </Page>
  )
}

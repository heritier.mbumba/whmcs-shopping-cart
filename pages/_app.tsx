import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
  useLazyQuery
} from '@apollo/client'
import { ApplicationProvider, useApplication } from '@app/context/App'
import { LanguageProvider } from '@app/context/LangProvider'
import ShopProvider, { useShop } from '@app/context/Shop'
import { persistor, store } from '@app/redux'
import { APP_INIT } from '@app/redux/actions'
import { Colors } from '@constants/index'
import useAnalytics from '@hooks/useAnalytics'
import useApp from '@hooks/useApp'
import useAuth from '@hooks/useAuth'
import useTranslate from '@hooks/useTranslate'
import { ICheckoutFlow } from '@lib/types/api'
import { GET_CLIENT_INVOICES } from '@utils/graphql-type'
import { AnimatePresence, motion } from 'framer-motion'
import { Provider as NextSessionProvider } from 'next-auth/client'
import type { AppProps, NextWebVitalsMetric } from 'next/app'
import NextNprogress from 'nextjs-progressbar'
import React from 'react'
import { Provider, useDispatch } from 'react-redux'
import { BeatLoader } from 'react-spinners'
import { PersistGate } from 'redux-persist/integration/react'
import '../styles/globals.scss'



export function reportWebVitals(metric: NextWebVitalsMetric) {
  /**
   * TODO: report for web vitals
   */
}

const { t } = useTranslate()

/**
 * Provides the app with query functionality from Apollo
 */
const apolloClient = new ApolloClient({
  uri: '/api/graphql',
  cache: new InMemoryCache({
    resultCaching: false
  }),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'no-cache'
    },
    query: {
      fetchPolicy: 'no-cache'
    }
  }
})

const Bootstrap = ({ Component, pageProps, router }: AppProps) => {
  /**
   * Hook used to manage the state of a loading varable to conditonally render data
   */
  const [isLoading, setIsLoading] = React.useState(true)

  /**
   * Extracts the configuration for the app from the config.json file
   */
  const { loadingConfig } = useApp()

  /**
   * Hook used to dispatch actions with redux
   */
  const dispatch = useDispatch()

  /**
   * Extracts the hooks from the application state to manage config
   */
  const { setConfig, config } = useApplication()

  /**
   * Helper function used to retrieve the authentication status and user details
   */
  const { isAuthenticated, user } = useAuth()

  /**
   * Helper function
   */
  const { flows: existingFlows, googleClick } = useShop()

  /**
   * Provides a GraphQl query for retrieving the invoices of a client
   */
  const [getClientInvoices, { data, loading, error }] =
    useLazyQuery(GET_CLIENT_INVOICES)

  /**
   * exracts the query name inside the route path
   */
  const query = router.query

  /**
   * Google Analytics hook used to log an event
   */
  const LogEvent  = useAnalytics()

  /**
   * Google identifier interceptor
   */
  const getGclid = () => query?.gclid || googleClick?.gclid

  /**
   * Function that checks if the current checkout flow can be updated
   * @param flows the flows of the application
   * @returns a boolean that determines if the flow can be updated
   */
  const canUpdateCheckoutFlow = (flows: ICheckoutFlow) => {
    if (!existingFlows) {
      return true
    } else if (
      existingFlows &&
      Object.values(flows).length !== Object.values(existingFlows).length
    ) {
      return true
    } else {
      return false
    }
  }

  /**
   * Page view analytics
   */
  React.useEffect(() => {
    if (router?.pathname) {
      LogEvent().pageView(router?.pathname, '1-grid express checkout')
      if(router?.pathname === '/'){
        LogEvent().cartInit() //Step 1
      }
    }
  }, [router?.pathname])

  /**
   * When the component is mounted, it sets the configuration
   */
  React.useEffect(() => {
    const config = loadingConfig()
    setConfig(config)

    /**
     * dispatches an action to the redux store to initialize the app state
     */
    dispatch({
      type: APP_INIT,
      payload: {
        client: config?.client,
        flows: canUpdateCheckoutFlow(config?.flows)
          ? config?.flows
          : existingFlows,
        currency: config?.client,
        gclid: getGclid()
      }
    })

    setIsLoading(false)
  }, [])

  /**
   * When the authentication of a user changes, this function runs
   * If a user is authenticated, it does a query to retrieve all the invoices related to the user
   */
  React.useEffect(() => {
    if (isAuthenticated) {
      getClientInvoices({
        variables: {
          clientid: user?.userid
        }
      })
    }
  }, [isAuthenticated])

  /**
   * Hook that runs when a query is loading
   * Sets the invoices of a client in the configuration state
   */
  React.useEffect(() => {
    if (data?.getClientInvoices) {
      setConfig({
        ...config,
        user: { invoices: data?.getClientInvoices || [] }
      })
    }
  }, [loading])

  /**
   * Displays a loading UI to a user when data is loading
   */
  if (isLoading) {
    return (
      <div
        className={`d-flex align-items-center justify-content-center flex-column vh-100`}
      >
        <BeatLoader loading size={40} color={Colors.primary} />
        <span className='mt-3'>{t('common[loading]')}</span>
      </div>
    )
  }

  return (
    <AnimatePresence>
      <motion.div
        initial='pageInitial'
        animate='pageAnimate'
        variants={{
          pageInitial: {
            opacity: 0
          },
          pageAnimate: {
            opacity: 1
          }
        }}
      >
        <NextNprogress
          color={Colors.primary}
          startPosition={0.3}
          stopDelayMs={200}
          height={3}
        />
        <Component {...pageProps} pathname={router?.pathname} />
      </motion.div>
    </AnimatePresence>
  )
}

/**
 * Main component for handling everything related to the app
 * Includes all the providers needed for the app to receive data
 * @param {Component} AppProps
 * @param {pageProps} AppProps
 * @param {router} AppProps
 */
function App({ Component, pageProps, router }: AppProps) {
  return (
    <NextSessionProvider
      session={pageProps.session}
      options={{
        basePath: '/api/auth',
        keepAlive: 60 * 60 * 24 * 1
      }}
    >
      <Provider {...{ store }}>
        <PersistGate {...{ persistor }}>
          <ApolloProvider {...{ client: apolloClient }}>
            <ApplicationProvider>
              <LanguageProvider>
                <ShopProvider>
                  <Bootstrap {...{ Component, pageProps, router }} />
                </ShopProvider>
              </LanguageProvider>
            </ApplicationProvider>
          </ApolloProvider>
        </PersistGate>
      </Provider>
    </NextSessionProvider>
  )
}

export default App




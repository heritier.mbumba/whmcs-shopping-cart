import { RESET_CART } from '@app/redux/actions'
import Page from '@components/HOC/Page'
import { PageProps } from '@lib/types/app'
import { GetServerSideProps } from 'next'
import Link from 'next/link'
import React from 'react'
import { useDispatch } from 'react-redux'
import useTranslate from 'src/hooks/useTranslate'
import { TiCancel } from 'react-icons/ti'
import { Colors } from 'src/constant'

interface PaymentSuccessPageProps extends PageProps {}

/**
 * Component that handles the UI for the cancelled payment page
 * @param props
 * @returns JSX element that provides a cancelled payment UI
 */
export default function PaymentSuccessPage({
  client,
  gateways,
  ...otherProps
}: PaymentSuccessPageProps) {
  const { t } = useTranslate() //translation helper function

  /**
   * Hook used to dispatch actions to redux
   */
  const dispatch = useDispatch()

  /**
   * When component mounts, this dispatches an action to reset the cart state
   */
  React.useEffect(() => {
    dispatch({ type: RESET_CART })
  }, [])

  return (
    <Page {...otherProps}>
      <section className='payment__sucess pt-5'>
        <div className='container'>
          <div className='col-md-6 col-xs-12 offset-md-3 text-center pb-5'>
            <p>
              <TiCancel color={Colors.danger} size={50} />
            </p>
            <h1 className='title-2 title-f-300 color-danger pt-3'>
              {t('page[cancellation][title]')}
            </h1>
            <span className='d-block mb-5'>
              {t('page[cancellation][description]')}
            </span>
            <Link href='https://1-grid.com/client/clientarea.php?action=invoices'>
              <a className='btn btn-pink'>{t('user[zone]')}</a>
            </Link>
          </div>
        </div>
      </section>
    </Page>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {
  return {
    props: {
      withSteps: false
    }
  }
}

import { ApolloError, ApolloServer } from 'apollo-server-micro'
// import { typeDefs } from "../../lib/graphql/schema";
import typeDefs from '../../lib/graphql/schema/schema.graphql'
import { resolvers } from '../../lib/graphql/resolvers'
import * as Sentry from '@sentry/node'

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: process.env.NODE_ENV === 'development' ? true : false,
  // plugins: [
  //   {
  //     requestDidStart(_){
  //       /**
  //        * Within this returned object, define functions that respond
  //        * to request-specific lifecycle events
  //        */
  //       return {
  //         didEncounterErrors(ctx){

  //           /**
  //            * If we couldn't parse the operation, don't
  //            * do anything here
  //            */
  //           if(!ctx.operation){
  //             return
  //           }
  //           for(const error of ctx.errors){
  //             /**
  //              * Only report internal server errors,
  //              * all errors extending ApolloError should be user-facing
  //              */
  //             if(error instanceof ApolloError){
  //               continue
  //             }

  //             /**
  //              * Annotate whether failing operation was query/mutation/subscription
  //              */
  //             Sentry.withScope(scope => {
  //               scope.setTag("Kind", ctx.operation.operation)

  //               /**
  //                * Log query and variables as extras (make sure to strip out sensitive data!)
  //                */
  //               scope.setExtra("query", ctx.request.query)
  //               scope.setExtra("variables", ctx.request.variables);

  //               if (error.path) {
  //                 // We can also add the path as breadcrumb
  //                 scope.addBreadcrumb({
  //                   category: "query-path",
  //                   message: error.path.join(" > "),
  //                   level: Sentry.Severity.Debug
  //                 });
  //               }
  //               const transactionId = ctx.request.http.headers.get(
  //                 "x-transaction-id"
  //               );
  //               if (transactionId) {
  //                 scope.setTransaction(transactionId);
  //               }

  //               Sentry.captureException(error);
  //             })
  //           }
  //         }
  //       }
  //     }
  //   }
  // ]
 });


//  Sentry.init({
//    dsn: process.env.NEXT_PUBLIC_SENTRY_DSN
//  })

export const config = {
  api: {
    bodyParser: false
  }
}

export default apolloServer.createHandler({ path: '/api/graphql' })

import NextAuth, { NextAuthOptions } from 'next-auth'
import Providers from 'next-auth/providers'
import { LoginUserType, RegisterUserType, SessionUser } from '@lib/types/auth'
import { NextApiRequest, NextApiResponse } from 'next'
import Request from '@lib/request'

const options: NextAuthOptions = {
  providers: [
    Providers.Credentials({
      id: 'whmcs',
      name: 'whmcs',
      credentials: {
        email: {
          type: 'text'
        },
        password: {
          type: 'password'
        }
      },
      async authorize(credentials: LoginUserType | RegisterUserType) {
        try {
          const { email, password2 } = credentials

          const response = await Request('WHMCS').post({
            action: 'ValidateLogin',
            email,
            password2
          })

          const { result, ...other } = response

          if (result !== 'success') {
            return Promise.reject({ message: other?.message, status: 400 })
          }

          const user = await Request('WHMCS').post({
            action: 'GetClientsDetails',
            email
          })

          if (user.result !== 'success') {
            return Promise.reject({
              message: 'Could not fetch client profile',
              status: 400
            })
          }

          const { client } = user

          return {
            name: client?.firstname,
            lastname: client?.lastname,
            email: client?.email,
            userid: +client?.id,
            paymentmethod: client?.defaultgateway,
            credit: client?.credit
          }
        } catch (error) {
          return error
        }
      }
    })
  ],
  pages: {
    error: '/checkout'
  },
  secret: process.env.AUTH_SECRET,
  jwt: {
    secret: process.env.AUTH_SECRET,
    encryption: true
  },
  callbacks: {
    jwt: async (token, user: SessionUser) => {
      if (user?.userid) {
        token.userid = user?.userid
        token.lastname = user?.lastname
        token.paymentmethod = user?.paymentmethod
        token.credit = user?.credit
      }

      return token
    },
    session: async (session, user: SessionUser) => {
      return { ...session, user }
    }
  },
  session: {
    jwt: true,
    maxAge: 60 * 60 * 24 * 1
  }
}

export default (req: NextApiRequest, res: NextApiResponse) =>
  NextAuth(req, res, options)

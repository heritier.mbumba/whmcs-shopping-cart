import { NextApiRequest, NextApiResponse } from "next"
import { pageMetaDefault } from "utils/default";

export default (req: NextApiRequest, res:NextApiResponse) => {
    return res.status(200).json({
        status: 200,
        data: pageMetaDefault[req.body.page]
    })
}
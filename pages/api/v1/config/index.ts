import { NextApiRequest, NextApiResponse } from "next"
import jwt from 'jsonwebtoken'
import config from "../../../../config.json";


export default async (req: NextApiRequest, res: NextApiResponse) => {
    try {
       const client = jwt.sign({clientip: req.url}, process.env.APP_SECRET_KEY!)
        return res.status(200).json({
          status: 200,
          data: { ...config, client },
        });
    } catch (error) {
        
    }
}
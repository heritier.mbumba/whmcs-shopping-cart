import cookie from 'cookie';
import { NextApiRequest, NextApiResponse } from "next";


export default (req: NextApiRequest, res: NextApiResponse) => {
  const invoice = req.body
  
  res.setHeader('Set-Cookie', cookie.serialize("orderInvoice", JSON.stringify(invoice), {
    httpOnly: true,
    secure: process.env.NODE_ENV !== "development",
    maxAge: 60 * 60 * 24 * 2,
    sameSite: 'strict',
    path: '/'
  }))
  return res.status(200).json({
    status: 200
  })
}
import { NextApiRequest, NextApiResponse } from "next";
import withValidRequest from "@lib/middleware/withValidRequest";


export default withValidRequest(async (req: NextApiRequest, res: NextApiResponse)=>{
  try {
    
    return res.status(200).json({
      status: 200,
      data: [
        {
          id: 14,
          name: "SSL Certificates",
          hidden: 0,
          type: "security",
        },
    
        {
          id: 25,
          name: "Web Hosting",
          hidden: 0,
          type: "hosting",
        },
        {
          id: 59,
          name: "Business Email",
          hidden: 0,
          type: "email",
        },
      ],
    });
    
  } catch (error) {
   
  }
})

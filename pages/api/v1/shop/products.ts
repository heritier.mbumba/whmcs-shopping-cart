import withValidRequest from "@lib/middleware/withValidRequest";
import Shop from "@lib/shop/Shop";
import { NextApiRequest, NextApiResponse } from "next";

export default withValidRequest(
  async (req: NextApiRequest, res: NextApiResponse) => {
    try {
      const { groupid } = req.body;

      const {
        data: {
          products: { product },
        },
      } = await Shop.getProducts(groupid);

      return res.status(200).json({
        status: 200,
        data: product,
      });
    } catch (error) {
      return res.status(400).json({
        status: 400,
        message: error?.message
      })
    }
  }
);

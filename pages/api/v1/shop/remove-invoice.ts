import cookie from 'cookie'
import { session } from 'next-session'
import { NextApiRequest, NextApiResponse } from "next";

export default (req: NextApiRequest, res: NextApiResponse) => {
  
  res.setHeader('Set-Cookie', cookie.serialize("orderInvoice", "", {
    httpOnly: true,
    secure: process.env.NODE_ENV !== "development",
    expires: new Date(0),
    sameSite: 'strict',
    path: '/'
  }))
  
  

  return res.status(200).json({
    status: 200
  })
}
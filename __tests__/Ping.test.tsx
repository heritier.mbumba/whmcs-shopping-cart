import { render, screen, cleanup } from '@testing-library/react'

describe('Ping should trigger', () => {
  it('Should render Hello ping app', ()=>{
    render(<h1 data-testid="ping-app">Hello ping</h1>)
    expect(screen.getByTestId("ping-app")).toHaveTextContent("Hello ping")
  })
})
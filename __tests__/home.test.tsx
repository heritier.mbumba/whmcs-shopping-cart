import { rest } from 'msw'
import {setupServer} from 'msw/node'
import { render, screen } from '@testing-library/react'
import HomePage from '@containers/home.page'


const server = setupServer(
  rest.get('/api/config', (req, res, ctx) => {
    
    return res(ctx.json({}))
  })
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

jest.mock('../src/hooks/useTranslate', () => ({
  useTranslate: () => {
    
  }
}))

describe('Home page', () => {
  it('Should render home page', () => {
    render(<h1>Choose a website address</h1>)

    expect(screen.getByText('Choose a website address')).toBeInTheDocument()
  })
})
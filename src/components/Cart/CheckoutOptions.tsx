import { useShop } from '@app/context/Shop'
import DomainResultAction from '@components/Domain/SearchResult/Theme/winter/ResultAction'
import useTranslate from '@hooks/useTranslate'
import { CHECKOUT_OPTIONS } from '@utils/default'
import Link from 'next/link'

interface CheckoutOptionsProps {
  onContinueHandler?: () => void
}

/**
 * UI for selecting the flow for a domain
 */
export default function CartCheckoutOptions({}: CheckoutOptionsProps) {
  const { t } = useTranslate() //translation helper function

  const { currentCheckoutFlow } = useShop()

  return (
    <div className='row mt-5'>
      <div className='col-md-8 offset-md-2'>
        <div className='container'>
          <div className='row'>
            <DomainResultAction
              {...{ flows: CHECKOUT_OPTIONS(), alignTextCenter: true }}
            />
          </div>
          <div className='row mt-5'>
            <div className='col-md-12 col-xs-12 text-center'>
              <Link href={`/cart?config=${currentCheckoutFlow}`}>
                <a className='btn btn-pink'>{t('button[continue]')}</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

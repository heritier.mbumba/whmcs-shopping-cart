import { useLazyQuery } from '@apollo/client'
import { useApplication } from '@app/context/App'
import { useShop } from '@app/context/Shop'
import withErrorBundary from '@components/HOC/withErrorBundary'
import EssentialProducts from '@components/Product/EssentialProducts'
import ProductAddons from '@components/Product/ProductAddons'
import RenderProducts from '@components/Product/RenderProducts'
import useAnalytics from '@hooks/useAnalytics'
import useTranslate from '@hooks/useTranslate'
import { IProduct } from '@lib/types/product'
import { FLOW_TITLE } from '@utils/default'
import { GET_PRODUCTS } from '@utils/graphql-type'
import { useRouter } from 'next/dist/client/router'
import Link from 'next/link'
import React from 'react'
import { MoonLoader } from 'react-spinners'
import styles from './cart.module.scss'
import { cartFlowReducer, checkoutFlowState } from './cartFlowReducer'

interface CartCheckoutFlowProps {
  steps: string[]
  flowName: string
}

/**
 * Component for handling the flow of the checkout process
 * @param steps the provided steps available in the checkout flow
 * @param flowName the name of a flow
 * @returns a collection of products related to the current flow
 */

function CartCheckoutFlow({
  steps,
  flowName
}: CartCheckoutFlowProps) {
  /**
   * Hook used to dispatch action types
   */
  const [state, localDispatch] = React.useReducer(
    cartFlowReducer,
    checkoutFlowState
  )
  const { t } = useTranslate() //translation helper function

  /**
   * Retrieves all the items from the Shop Context provider
   * Shop Context receives the data from the redux cart state
   */
  const { items } = useShop()

  /**
   * Hook used to manage the state of the products
   */
  const [products, setProducts] = React.useState<IProduct[]>()

  /**
   * Hook used to manage the state of any errors that occur
   */
  const [responseError, setResponseError] = React.useState()

  /**
   * Next.js function used to reroute a user
   */
  const router = useRouter()

  /**
   * Grabs the config variables provided from the App context
   */
  const {
    config: { supportedProductGroups, currency }
  } = useApplication()

  /**
   * GraphQL query used to retrieve the data regarding the products
   */
  const [getFlowPackage, { data, loading }] = useLazyQuery(GET_PRODUCTS)

  /**
   * Google Analytics fucntion used to log an event
   */
  const LogEvent = useAnalytics()

  /**
   * This runs when the component is mounted since it receives the 'steps' props.
   * when the steps are made available or change, it dispatches a action to the redux store to initialise the steps
   */
  React.useEffect(() => {
    if (steps) {
      const stepsWithoutDomains = steps.filter((stp) => stp !== 'domain')
      localDispatch({ type: 'INIT_STEPS', payload: stepsWithoutDomains })
    }
  }, [steps])

  /**
   * This runs every time the current step in the state in the redux store changes
   */
  React.useEffect(() => {
    const group = supportedProductGroups?.find(
      (group) => group.type === flowName
    )

    // reroutes the user to the order summary page when they are done
    if (state?.currentStep === 'done') {
      router.replace('/order-summary')
    } else {
      /**
       * Need to fetch flow based package products from graphql server
       */

      //if the current step in the redux state is 'package', retrieve all the packages related to the current step
      if (state?.currentStep === 'package') {
        getFlowPackage({
          variables: {
            gid: group?.id,
            package: group?.supportedPackage,
            type: group?.type,
            billingCycle: group?.defaultBillingCycle
          }
        })
        //if the current step in the redux state is 'essential', set all the essential product related to the current step
      }
    }
  }, [state?.currentStep])

  //when a query is loading, we first check for an error, if there is an error, it sets the response error
  //if there is data returned from the query, we set the products with the data recived
  React.useEffect(() => {
    if (data?.getProducts) {
      const { result, error } = data?.getProducts
      if (error) {
        setResponseError(error)
      } else {
        setProducts(result)
      }
    }
  }, [loading])

  //this handles setting the next step in the flow.
  const onNextHandler = (currentStep: string) => {
    if (state?.currentStep !== 'done') {
      const index = state?.steps?.indexOf(currentStep)
      const nextIndex = index + 1
      if (state?.steps?.length - 1 === index) {
        LogEvent().checkoutOption(3, flowName)
        LogEvent().checkout(3, flowName)
        localDispatch({ type: 'NEXT_STEP', payload: 'done' })
        return router.push('/order-summary')
      } else {
        LogEvent().checkoutOption(3, flowName)
        LogEvent().checkout(3, flowName)
        localDispatch({
          type: 'NEXT_STEP',
          payload: state?.steps?.[nextIndex]
        })
      }
    }
  }

  /**
   * Check if any products exist in the cart
   * @returns
   */
  const hasProducts = () =>
    items?.filter((item) => item?.products?.length > 0)?.length > 0

  const renderContent = () => {
    if (state?.currentStep === 'essentials') {
      return (
        <EssentialProducts
          {...{
            flow: flowName,
            group: supportedProductGroups?.find(
              (group) => group.type === flowName
            )
          }}
        />
      )
    } else if (state?.currentStep === 'addons') {
      return <ProductAddons {...{ pid: 512 }} />
    }
    return (
      <RenderProducts
        {...{
          products,
          currency,
          domains: items?.map((item) => item.domain),
          theme: 'modern'
        }}
      />
    )
  }

  if (state?.currentStep === 'done') {
    return (
      <div className='d-flex align-items-center justify-content-center h-100 mb-5 mt-3 flex-column'>
        <MoonLoader size={40} color='#20bfdd' />
        <p className='title-1 title-f-300 mt-4'>{t('common[pleasewait]')}</p>
      </div>
    )
  }

  const renderTitle = () => {
    if (loading) {
      return (
        <React.Fragment>
          <h4 className='title-2 title-f-300'>Loading packages</h4>
        </React.Fragment>
      )
    }
    return (
      <React.Fragment>
        <h4 className='title-2 title-f-300 upper'>
          {FLOW_TITLE()?.[flowName]?.[state?.currentStep]?.title}
        </h4>
        <span className='text-muted'>
          {FLOW_TITLE()?.[flowName]?.[state?.currentStep]?.description}
        </span>
      </React.Fragment>
    )
  }

  return (
    <div className={`mt-5 ${styles.flow__result}`}>
      <div className={`${styles.flow__steps}`}>{renderTitle()}</div>
      {loading ? (
        <div className='d-flex align-items-start justify-content-start h-100 mb-5 mt-3 flex-column'>
          <MoonLoader size={40} color='#20bfdd' />
          <p className='title-1 title-f-300 mt-4'>{t('common[pleasewait]')}</p>
        </div>
      ) : (
        <React.Fragment>
          {renderContent()}

          <div className='d-flex align-items-center justify-content-center mt-5'>
            {hasProducts() && (
              <Link href='/order-summary'>
                <a className='btn btn-secondary me-2'>
                  {t('button[ordersummary]')}
                </a>
              </Link>
            )}

            <button
              className='btn btn-pink'
              disabled={!hasProducts()}
              onClick={() => onNextHandler(state?.currentStep)}
            >
              {t('button[next]')}
            </button>
          </div>
        </React.Fragment>
      )}
      {!loading && <div className={`mt-5`}>
        <Link href="http://1-grid.com/client/cart.php">
        
        <a className="mt-3 btn-link" target="_blank">I need a different product</a>
        </Link>
      </div>}
    </div>
  )
}

export default withErrorBundary(CartCheckoutFlow)

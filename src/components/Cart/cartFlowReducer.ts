
interface IFlowState {
  steps?: string[];
  currentStep?: string;
  fullfilled?: string[]
}

interface IFlowAction {
  type: string;
  payload?: any
}

export const checkoutFlowState: IFlowState = {
  fullfilled: []
}

export const cartFlowReducer = (state=checkoutFlowState, action: IFlowAction) => {
  switch (action.type) {

    case 'INIT_STEPS':
      return {
        ...state,
        steps: action?.payload as string[],
        currentStep: action?.payload[0] as string
      }

    case 'NEXT_STEP':
      const fulfilledIndex = state?.steps?.indexOf(action?.payload) - 1
      const fullfilledEl = state?.steps?.[fulfilledIndex]
      const fullfilled = [...state?.fullfilled]
      fullfilled?.push(fullfilledEl)
      return {
        ...state,
        currentStep: action?.payload as string,
        fullfilled
      }
  
    default:
     return state
  }
}
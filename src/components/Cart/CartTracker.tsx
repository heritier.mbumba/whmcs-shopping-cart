import { ICartItem } from '@lib/types/shop'
import React from 'react'
import { FcSettings } from 'react-icons/fc'
import { HiCheckCircle } from 'react-icons/hi'
import { Colors } from '@constants/index'
import useCart from '@hooks/useCart'
import styles from './cart.module.scss'
interface ITrackerData {
  label: string
  status: 'unconfigured' | 'Configured' | 'Pending'
  key: string
}

interface CartTrackerProps {
  data: ICartItem[]
  steps?: string[]
}

/**
 *responsible for displaying the products attached to a domain
 * @param data current items in the redux cart state
 * @returns a UI for products attached to a domain
 */
export default function CartTracker({ data, steps }: CartTrackerProps) {
  const { isCartItemHas } = useCart()

  return (
    <div className={styles.cart__tracker}>
      <ul className={styles.cart__tracker_container}>
        {data?.map((item) => (
          <li key={item.domain?.name} className={styles.cart__tracker_item}>
            <div className='d-flex align-items-start justify-content-between'>
              <div>
                <span className='paragraph title-f-300'>{item.domain?.name}</span>
                <div className='d-flex align-items-center mt-3 flex-wrap'>
                  {item?.products?.map((product) => (
                    <p
                      key={product?.pid}
                      className={`d-flex align-items-center justify-content-start ps-0 ${
                        styles.cart__tracker_item_step
                      } ${
                        isCartItemHas(product?.type, product?.name)
                          ? 'bg-success'
                          : 'bg-grey'
                      }`}
                    >
                      <HiCheckCircle
                        className='me-2'
                        size={18}
                        color={Colors.success}
                      />
                      <span className={styles.cart__tracker_item_steptext}>
                        {product?.name?.split('(')?.[0]}
                      </span>
                    </p>
                  ))}
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  )
}

import { useApplication } from '@app/context/App'
import { useShop } from '@app/context/Shop'
import { IContact } from '@lib/types/app'
import { useRouter } from 'next/dist/client/router'
import React from 'react'
import { AiOutlineShoppingCart } from 'react-icons/ai'
import Logo from '../Logo'
import styles from './header.module.scss'
import TopHeader from './top.header'

interface HeaderProps {
  activeLink?: string
  type?: 'basic' | 'default' | 'light'
  showTopHeader?: boolean
  contact?: IContact
}

/**
 * this is the main headear component that displays the logo, cart total and the additional details header
 * @param showTopHeader a boolean used to conditionally render the top header
 * @returns a react element that handles the header
 */

export default function Header({ showTopHeader }: HeaderProps) {
  const { config } = useApplication()
  const { orderSummary, items } = useShop()

  const router = useRouter()

  const totalProducts = items?.reduce((prev, current) => {
    const domain = prev + 1
    const productsLenght = current.products.length

    return (prev = domain + productsLenght)
  }, 0)

  return (
    <header className={`${styles.header__default}`}>
      {showTopHeader && (
        <TopHeader {...{ ...config.contacts, language: config.language }} />
      )}
      <div className={`container border-bottom ${styles.header__default_main}`}>
        <div className={` ${styles.header__default_main_row}`}>
          <div className=''>
            <div className={`cursor ${styles.logo__container}`}>
              <Logo link='/' />
            </div>
          </div>
          <div className='cursor' onClick={() => router.push('/order-summary')}>
            <div className={styles.menu__hamburger}>
              <div className={styles.menu__total_price}>
                R{orderSummary?.total?.toFixed(2) || '0.00'}
              </div>
              <span className={styles.menu__total_items}>{totalProducts}</span>
              <AiOutlineShoppingCart size={30} />
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}

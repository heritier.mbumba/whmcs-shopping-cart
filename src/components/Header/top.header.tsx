import { useLazyQuery } from '@apollo/client'
import { useApplication } from '@app/context/App'
import ModalDefault, { IDefaultModalRef } from '@components/Modal/ModalDefault'
import PaymentModal, { IPaymentModalRef } from '@components/Modal/PaymentModal'
import { IInvoice, ILanguage } from '@lib/types/api'
import { IContact } from '@lib/types/app'
import { GET_CLIENT_INVOICES } from '@utils/graphql-type'
import { motion } from 'framer-motion'
import moment from 'moment'
import { signOut } from 'next-auth/client'
import Link from 'next/link'
import React from 'react'
import { AiOutlineUser } from 'react-icons/ai'
import { HiOutlineMenuAlt2 } from 'react-icons/hi'
import { Colors } from '@constants/index'
import useAuth from '@hooks/useAuth'
import useTranslate from '@hooks/useTranslate'
import styles from './header.module.scss'

interface TopHeaderProps {
  general: IContact
  support?: IContact
  language?: ILanguage
}

/**
 * this is the top header component that displays contact details, client zone link, client invoices, current language
 * @param language the language enabled in the user's browser
 * @param general contact info to be displayed in the top header. Email and contact number
 * @returns a React element that handles the UI for contact and client details
 */

export default function TopHeader({
  general,
  support,
  language
}: TopHeaderProps) {
  const { t } = useTranslate() //translation helper function
  const { isAuthenticated, user } = useAuth()
  const { config } = useApplication()
  const [invoiceid, setInvoiceId] = React.useState<number>()
  const [showPayment, setShowPayment] = React.useState(false)
  const invoicesModalRef = React.useRef<IDefaultModalRef>()

  const [getClientInvoices, { data, loading, error }] =
    useLazyQuery(GET_CLIENT_INVOICES)
  const [showMenu, setShowMenu] = React.useState(false)

  //retrieves a user's invoices if they are logged in
  React.useEffect(() => {
    if (isAuthenticated) {
      getClientInvoices({
        variables: {
          clientid: user?.userid
        }
      })
    }
  }, [isAuthenticated, user])

  const onInvoicePayHandler = () => {
    setShowMenu(false)
    invoicesModalRef.current.open()
  }

  const onPayInvoiceHandler = (invoice: number) => {
    setInvoiceId(invoice)
    invoicesModalRef?.current?.close()
    setShowPayment(true)
  }

  const renderInvoiceContent = () => {
    const { t } = useTranslate() //translation helper function

    return config?.user?.invoices?.map((invoice) => (
      <div
        className={`d-flex justify-content-between align-items-center ${styles.payment__invoice_item}`}
        key={invoice?.id}
      >
        <div>
          <p className='mb-0'>#{invoice?.id}</p>
          <p className='small text-muted fs-6'>
            {t('common[due]')}: {moment(invoice?.duedate).format('DD-MM-YYYY')}
          </p>
        </div>
        <div className='d-flex align-items-center'>
          <span className='heading_small color-pink me-3'>
            R{invoice?.total}
          </span>
          <button
            className='btn btn_custom btn_custom_success'
            onClick={() => onPayInvoiceHandler(invoice?.id)}
          >
            {t('button[payNow]')}
          </button>
        </div>
      </div>
    ))
  }

  return (
    <div className={styles.header__top}>
      <div className='container pb-2 pt-2'>
        <div className='row'>
          <div className='d-none d-lg-block col-md-6 col-xs-12 col-lg-4 col-xl-6'>
            <span className={`d-md-block d-none ${styles.header__top_contact}`}>
              {general?.email} | {general?.telephone}
            </span>
            <span
              className={`d-md-none d-block text-center ${styles.header__top_contact}`}
            >
              {general?.email} | {general?.telephone}
            </span>
          </div>
          <div className='col-xs-12 col-sm-12 d-lg-none'>
            <div className='d-flex align-items-center justify-content-between'>
              <HiOutlineMenuAlt2
                size={30}
                onClick={() => setShowMenu(!showMenu)}
              />
              {isAuthenticated && (
                <div
                  className={styles.site__auth}
                  onClick={() => signOut({ redirect: true })}
                >
                  <span className='site__auth'>{t('user[logout]')}</span>
                </div>
              )}
            </div>
            {showMenu && (
              <motion.div
                className={`${styles.menu__mobile}`}
                initial={{ translateX: -999 }}
                animate={{ translateX: 0 }}
              >
                <h4 className='title-3 title-f-300'>
                  {t('user[greeting]')}, {user?.name}
                </h4>
                <span className='color-pink'>{user?.email}</span>
                <div className='content mt-4'>
                  <span>{t('user[invoice][have]')}: </span>
                  <h3 className='title-2 title-f-500 upper color-pink'>
                    {config?.user?.invoices?.length}{' '}
                    {t('user[invoice][unpaid]')}
                  </h3>
                  <button
                    className='btn btn_custom btn_custom_pink mt-3'
                    onClick={onInvoicePayHandler}
                  >
                    {t('user[invoices][pay]')}
                  </button>
                  <span className='site__auth'>{t('user[logout]')}</span>
                </div>
              </motion.div>
            )}

            {showMenu && (
              <motion.div
                className={`${styles.menu__mobile}`}
                initial={{ translateX: -999 }}
                animate={{ translateX: 0 }}
              >
                {user ? (
                  <React.Fragment>
                    <h4 className='title-3 title-f-300'>
                      {t('er[greeting]')}, {user?.name}
                    </h4>
                    <span className='color-pink'>{user?.email}</span>
                    <div className='content mt-4'>
                      <span>{t('user[invoice][have]')}: </span>
                      <h3 className='title-2 title-f-500 upper color-pink'>
                        {config?.user?.invoices?.length}{' '}
                        {t('user[invoice][unpaid]')}
                      </h3>
                      <button
                        className='btn btn_custom btn_custom_pink mt-3'
                        onClick={onInvoicePayHandler}
                      >
                        {t('user[invoice][pay]')}
                      </button>
                    </div>
                  </React.Fragment>
                ) : (
                  <h4>{t('user[notLoggedIn]')}</h4>
                )}
              </motion.div>
            )}
          </div>
          <div className='d-none d-lg-flex col-md-6 col-lg-8 col-xl-6 align-items-center justify-content-end pe-0'>
            {config?.user && (
              <span
                className={`d-flex align-items-center ${styles.client__unpaid_btn} me-2`}
                onClick={onInvoicePayHandler}
              >
                <span className={`d-block pe-2`}>
                  {t('user[invoice][title]')}:
                </span>
                <span className={styles.client__unpaid_total}>
                  {config?.user?.invoices?.length}
                </span>
              </span>
            )}
            <div className='client__login text-right'>
              <Link href='https://1-grid.com/client/clientarea.php'>
                <a
                  className={`d-flex align-items-center ${styles.client__login_btn} me-2`}
                  target="_blank"
                >
                  <AiOutlineUser size={20} color={Colors.light} />
                  {!user && (
                    <span className='text-light ms-2 d-block'>
                      {t('user[zone]')}
                    </span>
                  )}

                  {user && (
                    <span className='text-light ms-2 d-block'>
                      {t('user[greeting]')}, {user?.name}
                    </span>
                  )}
                </a>
              </Link>
            </div>

            {isAuthenticated && (
              <div
                className={styles.site__auth}
                onClick={() => signOut({ redirect: true })}
              >
                <span className='site__auth'>{t('user[logout]')}</span>
              </div>
            )}
          </div>
        </div>
      </div>
      <ModalDefault
        ref={invoicesModalRef}
        title={t('modal[invoices][unpaid]')}
        description={t('modal[invoices][select]')}
        renderContent={() => renderInvoiceContent()}
      />
      {showPayment && (
        <PaymentModal
          {...{ invoiceid, onClose: () => setShowPayment(false) }}
        />
      )}
    </div>
  )
}

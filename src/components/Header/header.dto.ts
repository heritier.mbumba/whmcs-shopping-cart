export type HeaderProp = {
  backgroundColor?: string;
  options?: Record<string, any>;
};

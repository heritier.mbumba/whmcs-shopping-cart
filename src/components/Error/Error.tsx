import { useRouter } from 'next/dist/client/router'
import React from 'react'
import useTranslate from '@hooks/useTranslate'

interface ErrorProps {
  title: string
  message: string
  action?: () => void
  reload?: boolean
}

/**
 * Reusable error component for displaying errors to a user
 * @param title the title of the error message
 * @param message the message to display to a user with information regarding the error
 * @param action the action to perform when a user clicks on the button provided in the error component
 * @param relaod a boolean to determine if the page should be reloaded
 * @returns
 */

function Error({ title, message, action, reload }: ErrorProps) {
  const { t } = useTranslate() //translation helper function
  const router = useRouter()

  const actionHandler = () => {
    if (action) {
      action()
    } else if (reload) {
      router.reload()
    } else {
      return
    }
  }
  return (
    <div className='box box__error'>
      <h4 className='heading heading_small'>{title}</h4>
      <span>{message}</span>
      <button className='btn btn-secondary mt-4' onClick={actionHandler}>
        {t('common[tryAgain]')}
      </button>
    </div>
  )
}

export default React.memo(Error)

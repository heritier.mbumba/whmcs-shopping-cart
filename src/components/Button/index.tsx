import * as React from 'react'

interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  title: string
  type: 'primary' | 'secondary' | 'warning' | 'danger' | 'default'
}

/**
 * Component for wrapping elements
 * @param type what type of button will it be
 * @param title the text to be displayed in the button
 * @returns buton component
 */

export default function Button({ type, title, ...props }: ButtonProps) {
  return (
    <button className={`btn btn-${type ? type : 'default'}`} {...props}>
      {title}
    </button>
  )
}

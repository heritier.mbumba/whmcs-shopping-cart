import React, { useImperativeHandle } from 'react'
import useAuth from '@hooks/useAuth'
import styles from './modal.module.scss'
import { Colors } from '@constants/index'
import { useLazyQuery } from '@apollo/client'
import { GET_UNPAID_INVOICE } from '@utils/graphql-type'
import { AiOutlineClose } from 'react-icons/ai'
import { MoonLoader } from 'react-spinners'
import useTranslate from '@hooks/useTranslate'

export interface IPaymentModalRef {
  close: () => void
  open: () => void
}

interface PaymentModalProps {
  invoiceid?: number
  onClose: () => void
}

/**
 * a modal UI for displaying the different payment methods
 * @param invoiceid the id of the invoice that will be paid
 * @param onClose handles the behaviour when the modal is closed. Clears the state
 * @returns JSX element for displaying the different payment methods in a modal
 */

const PaymentModal = ({ invoiceid, onClose }: PaymentModalProps) => {
  const { t } = useTranslate() //translation helper function

  /**
   * authorization helper functions from next that checks if a user is logged in
   * also gives the details of the current logged in user
   */
  const { isAuthenticated, user } = useAuth()

  /**
   * this query retrieves the invoice of an order
   * @returns invoice details related to the order
   */
  const [getOrderInvoiceIdHandler, { data, loading }] =
    useLazyQuery(GET_UNPAID_INVOICE)

  /**
   * retrieves the detials of an invoice when an invoice id is passed and the user is authenticated
   * @param invoiceid the id of the invoice that has been passed from the parent component
   * @param isAuthenticated a boolean value that is determined by a user being logged in
   * @returns data related to the invoice of an order
   */
  React.useEffect(() => {
    if (invoiceid) {
      if (isAuthenticated) {
        getOrderInvoiceIdHandler({
          variables: {
            id: +invoiceid,
            client: {
              firstname: user?.name,
              lastname: user?.lastname,
              email: user?.email
            }
          }
        })
      }
    }
  }, [invoiceid, isAuthenticated])

  if (!invoiceid) {
    return null
  }

  /**
   * this is displayed if the user has selected snapscan as their payment method
   */
  const renderModalContent = React.useCallback(() => {
    if (data?.getInvoice?.result?.snapscan) {
      const { invoiceid, total, apiKey, verifyHash } =
        data?.getInvoice?.result?.snapscan
      return (
        <div
          className={`d-flex flex-column align-items-center ${styles.gateway__snapscan}`}
        >
          <h4 className='heading__primary color-pink'>R{total}</h4>

          <a
            href={`https://pos.snapscan.io/qr/${apiKey}?id=${invoiceid}&amount=${Math.ceil(
              total * 100
            )}&verifyHash=${verifyHash}&strict=true`}
            className={styles.gateway__snapscan_item}
          >
            <img
              src={`https://pos.snapscan.io/qr/${apiKey}.svg?id=${invoiceid}&amount=${Math.ceil(
                total * 100
              )}&verifyHash=${verifyHash}&strict=true&snap_code_size=225`}
            />
          </a>
        </div>
      )
    }

    return null
  }, [data, loading])

  /**
   * this is displayed if the user has selected payfast as their payment method
   */
  if (data?.getInvoice?.result?.payfast) {
    if (data?.getInvoice?.result?.paymentidentifier) {
      window?.payfast_do_onsite_payment(
        { uuid: data?.getInvoice?.result?.paymentidentifier },
        (result) => {
          if (result === true) {
            //set analytic
          } else {
          }
        }
      )
    }

    return null
  }

  return (
    <div className={styles.modal__container}>
      <div className={styles.modal__content}>
        <div
          className={`d-flex align-items-center justify-content-center text-center ${styles.modal__header}`}
        >
          <div className={styles.modal__header_content}>
            <div className={styles.modal__header_logo}></div>
            {
              <h2 className={`${styles.modal__header_title}`}>
                {t('modal[payment][title]')}
              </h2>
            }
            {
              <p className={styles.modal__header_description}>
                {t('modal[payment][description]')}
              </p>
            }
          </div>
          <div className={`cursor ${styles.modal__close}`} onClick={onClose}>
            <AiOutlineClose size={25} color={Colors.light} />
          </div>
        </div>
        {loading && (
          <div className='d-flex align-items-center justify-content-center'>
            <MoonLoader loading size={80} color={Colors.primary} />
          </div>
        )}
        {data?.getInvoice?.result?.snapscan && renderModalContent()}
      </div>
    </div>
  )
}

export default PaymentModal

import { Colors } from '@constants/index'
import useTranslate from '@hooks/useTranslate'
import { IDomainTld } from '@lib/types/domain'
import { TLDS } from '@utils/domain'
import React from 'react'
import { ImCheckboxChecked, ImCheckboxUnchecked } from 'react-icons/im'
import styles from './modal.module.scss'

interface DomainTldsContentProps {
  onSelectChange?: (tld: IDomainTld) => void
  selectedTlds?: string[]
  tlds: IDomainTld[]
}

/**
 * modal that displays a list of all the supported tlds a user can check
 * @param onSelectChange handles the behaviour of when a tld is selected
 * @param selectedTlds array of selected tlds
 * @param tlds collection of supported tlds
 * @returns a React element that provides a Ui for a user to select multiple tlds to use for their domain search
 */

export default function DomainTldsContent({
  onSelectChange,
  selectedTlds,
  tlds
}: DomainTldsContentProps) {
  const { t } = useTranslate() //translation helper function

  const tldsList = tlds || TLDS?.map((tld: string)=>({name: tld, register: 'failed', transfer: 'failed', renew: 'failed'}))

  const onSelectTldHandler = React.useCallback(
    (tld: IDomainTld) => {
      return onSelectChange(tld)
    },
    [onSelectChange]
  )

  return (
    <div className={`${styles.domain__tld_container}`}>
      {tldsList?.map((tld) => {
        return (
          <div
            className={`d-flex align-items-center ${styles.domain__tld}`}
            key={tld.name}
            onClick={() => onSelectTldHandler(tld)}
          >
            <div
              className={`d-flex align-items-center w-100 justify-content-between ${styles.domain__tld_item}`}
            >
              <div className={`${styles.domain__tld_content}`}>
                <h4
                  className={`me-2 color-secondary ${styles.domain__tld_name}`}
                >
                  {tld?.name}
                </h4>
                <div className='d-flex align-items-center'>
                  <span className={`small d-block text-muted`}>
                    {t('form[register][title]')}: R{tld?.register} |{' '}
                    {t('form[transfer][title]')}: R{tld?.transfer || '0.00'}
                  </span>
                </div>
              </div>
              <div className='ps-2 cursor'>
                {selectedTlds.includes(tld.name) ? (
                  <ImCheckboxChecked size={20} color={Colors.success} />
                ) : (
                  <ImCheckboxUnchecked size={20} color={Colors.secondary} />
                )}
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}

import { MoonLoader } from 'react-spinners'
import styles from './modal.module.scss'

interface LoadingSpinnerProps {
  title?: string
  description?: string
}

/**
 * component that displays a loading interface to a user when the app is retrieving data
 * @param title title to display to a user
 * @param description description to display to a user
 * @returns a JSX element to display a loading UI
 */
export default function LoadingSpinner({
  title,
  description
}: LoadingSpinnerProps) {
  return (
    <div className={styles.modal__container}>
      <div className={styles.modal__content}>
        <div className='d-flex align-items-center justify-content-center'>
          <MoonLoader size={40} color='#20bfdd' />
        </div>
        <div className='text-center mt-4'>
          <h4 className='title-1 title-f-300'>{title}</h4>
          <span className='paragraph'>{description}</span>
        </div>
      </div>
    </div>
  )
}

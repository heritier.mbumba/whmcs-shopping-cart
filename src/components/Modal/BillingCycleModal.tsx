import {
  getProRataAmount,
  updateBillingCycleDispatcher
} from '@app/redux/dispatches/shopDispatcher'
import BoxItem from '@components/Box/BoxItem'
import { IDomainWithPrice } from '@lib/types/domain'
import { IProduct } from '@lib/types/product'
import React from 'react'
import { AiOutlineClose } from 'react-icons/ai'
import { FaCheckCircle, FaRegCircle } from 'react-icons/fa'
import { useDispatch } from 'react-redux'
import { Colors } from '@constants/index'
import styles from './modal.module.scss'
import useTranslate from '@hooks/useTranslate'

interface BillingCycleModalProps {
  domain: IDomainWithPrice
  product: IProduct
  visible: boolean
  onClose: () => void
}

/**
 * a modal UI for when a user changes the billing cycle for a product
 * @param product the product who's billing cycle needs to be changed
 * @param domain the domain the product is related to
 * @param visible boolean to conditionally display the modal
 * @param onClose funtionality that resets the selected product state when the modal is closed
 * @returns
 */

const BillingCycleModal = ({
  product,
  domain,
  visible,
  onClose
}: BillingCycleModalProps) => {
  const { t } = useTranslate() //translation helper function

  const dispatch = useDispatch()

  const getBillingCycles = () => {
    const { __typename, ...values } = product.billingCycles
    const keys = Object.keys(values)
      .map((cycle) => ({
        key: cycle,
        value: values[cycle],
        selected: product.defaultBillingCycle === cycle,
        pid: product.pid,
        domain: domain.name
      }))
      .filter((bc) => !['-1.00', '0.00'].includes(bc.value))

    return keys
  }

  if (!visible) {
    return null
  }

  return (
    <div className={styles.modal__container}>
      <div className={styles.modal__content}>
        <div className={`d-flex align-items-center ${styles.modal__header}`}>
          <div className={styles.modal__header_content}>
            <h2 className={`${styles.modal__header_title}`}>{product?.name}</h2>
            <p className={styles.modal__header_description}>
              {t('page[billing][cycle]')}
            </p>
          </div>

          <div className={`cursor ${styles.modal__close}`} onClick={onClose}>
            <AiOutlineClose size={25} color={Colors.light} />
          </div>
        </div>
        {getBillingCycles()?.map((billingCycle) => (
          <BoxItem
            cssClasses={`d-flex align-items-center justify-content-between ${styles.billingcycle}`}
            onClick={() =>
              dispatch(
                updateBillingCycleDispatcher(
                  billingCycle.domain,
                  billingCycle.pid,
                  billingCycle.key
                )
              )
            }
            key={billingCycle.key}
          >
            <div className='d-flex align-items-center'>
              {product.defaultBillingCycle === billingCycle.key ? (
                <FaCheckCircle size={20} color={Colors.success} />
              ) : (
                <FaRegCircle size={20} />
              )}
              <span
                className={styles.billingcycle__name}
                key={billingCycle.key}
              >
                {billingCycle?.key.charAt(0).toUpperCase() +
                  '' +
                  billingCycle?.key?.slice(1)}
              </span>
            </div>
            <div className='text-end'>
              <span
                className={`d-block ${styles.billingcycle__value}`}
                key={billingCycle.key}
              >
                R{billingCycle?.value}
              </span>
              <i className='text-muted small'>
                {t('common[prorata]')}: R
                {getProRataAmount(
                  billingCycle?.value,
                  billingCycle.key
                ).toFixed(2)}
              </i>
            </div>
          </BoxItem>
        ))}
      </div>
    </div>
  )
}

export default BillingCycleModal

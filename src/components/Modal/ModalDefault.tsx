import { IProduct } from '@lib/types/product'
import React, { useImperativeHandle } from 'react'
import { ReactNode } from 'react'
import { AiFillCloseCircle, AiOutlineClose } from 'react-icons/ai'
import { MoonLoader } from 'react-spinners'
import { Colors } from '@constants/index'
import LoadingSpinner from './LoadingSpinner'
import styles from './modal.module.scss'
import useTranslate from '@hooks/useTranslate'

export interface IDefaultModalRef {
  toggle?: () => void
  open?: () => void
  close?: () => void
  renderTitle?: (title: string) => void
  renderDescription?: (title: string) => void
  renderContentWithProducts?: (products: Record<string, any>[]) => void
  renderHeaderLogo?: (logo: JSX.Element) => void
  triggerSpinner?: () => void
  closeSpinner?: () => void
}

interface ModalDefaulProps {
  renderContent?: () => JSX.Element | React.FC | ReactNode | null
  title?: string
  description?: string
  error?: Record<string, any>
  onCloseHandler?: () => void
  hideCloseBtn?: boolean
}

/**
 * this is a boilerplate modal used to display data to a user
 * @param renderContent this is used to conditionally render the content in the modal that has been passed through from the parent component
 * @param title title to be rendered in the modal
 * @param description description to be rendered in the modal
 * @param error errors to be displayed in the modal if any occur
 * @param onCloseHandler handles the behaviour when the modal is closed
 * @returns a JSX building block modal to be used in the UI
 */

const ModalDefaul = (
  {
    renderContent,
    title,
    description,
    error,
    onCloseHandler,
    hideCloseBtn
  }: ModalDefaulProps,
  ref: React.Ref<IDefaultModalRef>
) => {
  const { t } = useTranslate() //translation helper function

  /**
   * state used to manage the visibility of the modal
   */
  const [visible, setVisible] = React.useState(false)

  /**
   * hook used to set the title in the modal
   */
  const [getTitle, setGetTitle] = React.useState<string>(title)

  /**
   * hook used to set the title in the modal
   */
  const [getDescription, setGetDescription] =
    React.useState<string>(description)

  /**
   * hook used to set the products to be used in the modal
   */
  const [getProducts, setProducts] = React.useState<IProduct[]>()

  /**
   * hook used to set the visibility and boolean value of the spinner component
   */
  const [spinner, setSpinner] = React.useState(false)

  /**
   * toggler used to change the boolean value of the visible value
   */
  const toggle = () => setVisible(!visible)

  /**
   * function used to set the visible boolean state tot true
   */
  const open = () => setVisible(true)

  /**
   * callback used to set the visible boolean to false when the close function is invoked
   */
  const close = React.useCallback(() => setVisible(false), [visible])

  /**
   * function that invokes the setGetTitle method to update the title state
   */
  const renderTitle = (title: string) => setGetTitle(title)

  /**
   * function that invokes the setGetDescription method to update the description state
   */
  const renderDescription = (description: string) =>
    setGetDescription(description)

  /**
   * function used to set the spinner state to true. Used to display the spinner
   */
  const triggerSpinner = () => setSpinner(true)

  /**
   * function used to set the spinner state to false. Used to hide the spinner
   */
  const closeSpinner = () => setSpinner(false)

  /**
   * hook used to manage the logo
   */
  const [logoContent, setLogoContent] = React.useState<JSX.Element>()

  /**
   * callback used to set the products that will be rendered in the modal
   */
  const renderContentWithProducts = React.useCallback(
    (products: IProduct[]) => {
      return setProducts(products)
    },
    [visible]
  )

  /**
   * function used to set the logo content used in the modal
   */
  const renderHeaderLogo = (logo: JSX.Element) => setLogoContent(logo)

  /**
   * this is used to create a ref to this modals methods
   * allows you to refer to all these methods within the modal without having to prop-drill
   */
  useImperativeHandle(
    ref,
    () => ({
      toggle,
      renderTitle,
      renderContentWithProducts,
      renderDescription,
      renderHeaderLogo,
      close,
      open,
      triggerSpinner,
      closeSpinner
    }),
    []
  )

  if (spinner) {
    return <LoadingSpinner />
  }

  if (!visible) {
    return null
  }

  if (error && Object.values(error).length > 0) {
    return (
      <div className={styles.modal__container}>
        <div className={styles.modal__content}>
          <div className='text-center'>
            <span className='d-flex align-items-center justify-content-center mb-4'>
              <AiFillCloseCircle
                size={50}
                color={Colors.danger}
                className='d-block'
              />
            </span>
            <h4 className='heading heading_small'>{error.title}</h4>
            <span>{error.message}</span>
            <button className='btn btn-primary mt-5' onClick={close}>
              {t('common[close]')}
            </button>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className={styles.modal__container}>
      <div className={styles.modal__content}>
        <div
          className={`d-flex align-items-center justify-content-center text-center ${styles.modal__header}`}
        >
          <div className={styles.modal__header_content}>
            <div className={styles.modal__header_logo}>
              {logoContent && logoContent}
            </div>
            {getTitle && (
              <h2 className={`${styles.modal__header_title}`}>{getTitle}</h2>
            )}
            {getDescription && (
              <p className={styles.modal__header_description}>
                {getDescription}
              </p>
            )}
          </div>
          <div
            className={`cursor ${styles.modal__close}`}
            onClick={onCloseHandler ? onCloseHandler : close}
          >
            <AiOutlineClose size={25} color={Colors.light} />
          </div>
        </div>
        {renderContent && renderContent()}
      </div>
    </div>
  )
}

export default React.forwardRef(ModalDefaul)

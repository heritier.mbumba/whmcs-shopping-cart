import { useLazyQuery } from '@apollo/client'
import LoginForm from '@components/Auth/login.form'
import RegisterForm from '@components/Auth/register.form'
import { Colors } from '@constants/index'
import { nextAuthLogin } from '@lib/nextAuth'
import { IAddNewClient } from '@lib/types/auth'
import { ADD_CLIENT } from '@utils/graphql-type'
import { motion } from 'framer-motion'
import { signIn } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import React from 'react'
import { AiOutlineClose } from 'react-icons/ai'
import useTranslate from 'src/hooks/useTranslate'
import styles from './modal.module.scss'

enum AuthType {
  LOGIN = 'LOGIN',
  REGISTER = 'REGISTER'
}

type AuthUserType = {
  LOGIN?: {
    email: string
    password2: string
  }
  REGISTER?: IAddNewClient
}

const authDescription = {
  LOGIN: 'Enter your email and password to login',
  REGISTER: 'Fill the form field to register your account'
}

export type LoginModalRefType = {
  toggle?: () => void
  open?: () => void
  close?: () => void
  renderTitle?: (title: string) => void
  renderDescription?: (title: string) => void
  renderContentWithProducts?: (products: Record<string, any>[]) => void
}

interface LoginModalProps {
  onCloseHandler?: () => void
}

/**
 * Authentication modal that displays a UI for a user to login or register
 * @param onCloseHandler handler for closing the modal
 * @returns a JSX element that conditionally displays a login or register form in a modal component
 */

const LoginModal = ({ onCloseHandler }: LoginModalProps) => {
  const { t } = useTranslate() //translation helper function

  const [loginError, setLoginError] = React.useState<string>()
  const [loginSuccess, setLoginSuccess] = React.useState<string>()

  //fields that a user can possibly enter
  const [user, setUser] = React.useState<AuthUserType>({
    LOGIN: {
      email: '',
      password2: ''
    },
    REGISTER: {
      firstname: '',
      lastname: '',
      email: '',
      password2: '',
      address1: '',
      city: '',
      state: '',
      postcode: '',
      country: '',
      phonenumber: '',
      noemail: true
    }
  })

  const [authType, setAuthType] = React.useState<AuthType>(AuthType.REGISTER)

  const [addClientHandler, { loading, data, error }] = useLazyQuery(ADD_CLIENT)
  const [isLoading, setIsLoading] = React.useState<boolean>(false)

  const router = useRouter()

  //reloads the page when a user successfully logs in
  React.useEffect(() => {
    if (loginSuccess) {
      router.reload()
    }
  }, [loginSuccess])

  //used to log a user in when they successfully sign in
  React.useEffect(() => {
    if (data?.addClient?.result) {
      signIn('whmcs', {
        email: user?.REGISTER.email,
        password2: user?.REGISTER.password2
      })
    }
  }, [loading])

  /**
   * Input value handler
   * @param e
   * @param target
   */
  const inputValuehandler = (
    e: React.FormEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement>,
    target: string
  ) => {
    e.preventDefault()

    setLoginError(null)
    const value = e.currentTarget?.value
    setUser((prev) => ({
      ...prev,
      [authType]: { ...prev[authType], [target]: value }
    }))
  }
  /**
   * Login form submit handler
   * @param event
   * @returns
   */
  const loginHandler = async (event: React.FormEvent<HTMLFormElement>) => {
    try {
      event.preventDefault()
      setIsLoading(true)
      if (!user.LOGIN) {
        return alert(t('auth[login][credentialsMissing]'))
      }

      // await signIn("whmcs", { ...user.LOGIN });
      await nextAuthLogin(user.LOGIN)
      setIsLoading(false)
      setLoginSuccess(t('auth[login][success]'))
    } catch (error) {
      setLoginError(error?.url?.message || t('auth[login][something]'))
      setIsLoading(false)
    }
  }

  /**
   * Register form submit handler
   * @param event
   */
  const registerHandler = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    addClientHandler({
      variables: {
        client: user?.REGISTER
      }
    })
  }

  if (loginSuccess) {
    return (
      <div className={styles.modal__container}>
        <div className={styles.modal__content}>
          <div
            className={`d-flex align-items-center justify-content-center ${styles.modal__header}`}
          >
            <div className={styles.modal__header_content}>
              <h2 className='heading__primary color-pink pt-3'>
                {loginSuccess}
              </h2>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const onValueHandler = (values: Record<string, any>) => {
    setUser((prev) => ({
      ...prev,
      [authType]: { ...prev?.[authType], ...values }
    }))
  }

  return (
    <div className={styles.modal__container}>
      <div className={styles.modal__content}>
        <div
          className={`d-flex align-items-center justify-content-center ${styles.modal__header}`}
        >
          <div className={styles.modal__header_content}>
            <h2 className={`${styles.modal__header_title}`}>
              {authType === 'LOGIN'
                ? t('form[login][title]')
                : t('form[register][title]')}
            </h2>
            <p className={styles.modal__header_description}>
              {authDescription?.[authType]}
            </p>
            {loginError && <span className='text-danger'>{loginError}</span>}
          </div>
          <div
            className={`cursor ${styles.modal__close}`}
            onClick={onCloseHandler}
          >
            <AiOutlineClose size={25} color={Colors.light} />
          </div>
        </div>
        {authType === 'LOGIN' && (
          <motion.div
            layout
            initial={{ translateX: 100, opacity: 0 }}
            animate={{ translateX: 0, opacity: 1 }}
          >
            <LoginForm
              {...{
                loginHandler,
                inputValuehandler,
                inputValue: user.LOGIN,
                loading: isLoading
              }}
            />
          </motion.div>
        )}

        {authType === 'REGISTER' && (
          <motion.div
            layout
            initial={{ translateX: -100, opacity: 0 }}
            animate={{ translateX: 0, opacity: 1 }}
          >
            <RegisterForm
              {...{
                registerHandler,
                inputValuehandler,
                inputValue: user.REGISTER,
                loading,
                onValueHandler
              }}
            />
          </motion.div>
        )}

        {!(isLoading || loading) && (
          <div className='text-center mt-4'>
            <span className='d-block small'>
              {authType === 'REGISTER'
                ? t('auth[login][haveAccount]')
                : t('auth[login][noAccount]')}
            </span>
            {authType === 'REGISTER' ? (
              <a
                className='link link-primary'
                onClick={() => setAuthType(AuthType.LOGIN)}
                href='#'
              >
                {t('form[login][title]')}
              </a>
            ) : (
              <a
                className='link link-primary'
                onClick={() => setAuthType(AuthType.REGISTER)}
                href='#'
              >
                {t('form[register][title]')}
              </a>
            )}
          </div>
        )}
      </div>
    </div>
  )
}

export default LoginModal

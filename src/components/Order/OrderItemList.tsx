import { useShop } from '@app/context/Shop'
import ModalDefault, { IDefaultModalRef } from '@components/Modal/ModalDefault'
import { Colors } from '@constants/index'
import useTranslate from '@hooks/useTranslate'
import { IDomainWithPrice } from '@lib/types/domain'
import { IProduct } from '@lib/types/product'
import { motion } from 'framer-motion'
import Link from 'next/link'
import React from 'react'
import { BsFillTrashFill } from 'react-icons/bs'
import { FaChevronDown, FaChevronUp } from 'react-icons/fa'
import styles from './order.module.scss'
import OrderItem from './OrderItem'

export type BillingCycleType = {
  key: string
  value: string
  selected: boolean
  pid: number
  domain: string
}

interface OrderItemProps {
  products: IProduct[]
  domain: IDomainWithPrice
}

/**
 * Component used to display the collection of all the products added to a domain and the details regarding the products
 * @param products the collection of the products currently added to the domain
 * @param domain the name of the domain that the products should link to
 * @returns a JSX component that lists a collection of all the details and products regarding a domain
 */
export default function OrderItemList({ products, domain }: OrderItemProps) {
  const { t } = useTranslate() //translation helper function

  /**
   * Hook used to conditionally render the visibilty of the component
   */
  const [show, setShow] = React.useState(true)

  /**
   * Hook that manages the state of the epp code associated with a domain
   */
  const [eppcode, setEppCode] = React.useState<string>()

  /**
   * Creates a ref to the current epp code fro this product
   * This prohibits the app from altering all the epp codes, and only mutates this ref
   */
  const eppCodeRef = React.useRef<IDefaultModalRef>()

  /**
   * Use shop hooks which expose all method for dealing with the shop
   */
  const {
    onAddDomainEppCode,
    onRemoveItemToCart,
    onRemoveDomainEppCode,
    onRemoveItemProductToCart
  } = useShop()

  /**
   * This function generates tiny pills with the product type
   * @returns a JSX collection of all the types of products associated with the domain
   */
  const renderProductTypes = () => {
    const types = products?.map((product) => product.type)

    return types.map((type) => {
      const typeName = type?.split('-')?.join(' ')
      return (
        <span className={styles.order__item_productlabel} key={type}>
          {typeName}
        </span>
      )
    })
  }

  /**
   * Checks if a domain can be given for free
   */
  const hasFreeDomainOption = () => {
    return (
      products?.filter((product) => product?.freeDomains?.includes(domain?.tld))
        ?.length > 0
    )
  }

  /**
   * Generates a button for the user when there are no products attached to a domain
   */
  const renderSuggestProducts = () => {
    return (
      <div className={`d-block ${styles.domain__suggestproduct}`}>
        <Link href={`/cart?config=flow`}>
          <a className='btn btn_custom btn_custom_secondary'>
            {t('common[addProduct]')}
          </a>
        </Link>
      </div>
    )
  }

  /**
   * Handles the behavior when a user adds an epp code to a domain
   */
  const onAddEppCodeHandler = () => {
    eppCodeRef.current.open()
    eppCodeRef.current.renderTitle(t('common[eppCode][add]'))
    eppCodeRef.current.renderDescription(
      `${t('Please add your eppcode for')} ${domain.name}`
    )
  }

  /**
   * Renders a form component for a user to enter an epp code for their domain
   */
  const renderEppCodeFormInput = () => {
    return (
      <div className='text-center'>
        <div className='form-group mb-3'>
          <input
            type='text'
            className='form-control text-center'
            placeholder='Add your epp code'
            aria-label='Add your epp code'
            onChange={(e) => setEppCode(e.currentTarget.value)}
          />
        </div>
        <button
          className='btn btn-pink'
          onClick={() => {
            onAddDomainEppCode(domain, eppcode)
            eppCodeRef.current.close()
          }}
          disabled={!eppcode}
        >
          {t('common[add]')}
        </button>
      </div>
    )
  }

  return (
    <motion.div className={styles.order__item}>
      <motion.div>
        <BsFillTrashFill
          size={20}
          className={`${styles.order__item_close} cursor`}
          color={Colors.danger}
          onClick={() => onRemoveItemToCart(domain)}
        />
        <motion.h4 className={`title-2 title-f-300 me-5`}>
          {domain?.name}
        </motion.h4>
        <strong className='d-block heading_small color-pink'>
          {hasFreeDomainOption()
            ? t('common[free]')
            : `R
            ${
              domain?.status
                ? Number(domain?.register).toFixed(2)
                : Number(domain?.transfer).toFixed(2)
            } ${t('component[timepill][year]')}`}
        </strong>
        <span
          className={`${
            domain?.status
              ? styles.order__item_domainstatus
              : styles.order__item_domainstatus_transfer
          }`}
        >
          {t('checkoutOptions[domain][name]')}
          {domain?.status
            ? ` ${t('common[registration]')}`
            : ` ${t('button[transfer]')}`}
        </span>

        <hr />

        {products.length === 0 ? (
          <div className='pt-4 pb-4'>
            <h4 className='title-1 title-f-300'>
              {t('common[customizeDomains]')}
            </h4>
            {products.length === 0 && renderSuggestProducts()}
          </div>
        ) : (
          <div className='pt-4 pb-4'>
            <div className='d-flex align-items-center justify-content-between'>
              <h4 className='small heading_upper'>
                {products.length} {t('common[products]')}
              </h4>

              {show ? (
                <FaChevronUp
                  size={20}
                  className='cursor custom-icon'
                  onClick={() => setShow(!show)}
                />
              ) : (
                <FaChevronDown
                  size={20}
                  className='cursor custom-icon'
                  onClick={() => setShow(!show)}
                />
              )}
            </div>
            {renderProductTypes()}

            {!domain?.status && (
              <div className='d-block'>
                {domain?.eppcode ? (
                  <button
                    className='btn btn_custom btn_custom_danger mt-5'
                    onClick={() => onRemoveDomainEppCode(domain)}
                  >
                    {t('common[code]')}: {domain?.eppcode}
                  </button>
                ) : (
                  <button
                    className='btn btn_custom btn_custom_primary mt-5'
                    onClick={onAddEppCodeHandler}
                  >
                    {t('common[eppCode][addAlt]')}
                  </button>
                )}
              </div>
            )}
          </div>
        )}
        {products?.map((product, index) => (
          <OrderItem
            {...{
              product,
              onDelete: () => onRemoveItemProductToCart(domain, product),
              show,
              domain
            }}
            key={product?.pid + index}
          />
        ))}
      </motion.div>
      <ModalDefault ref={eppCodeRef} renderContent={renderEppCodeFormInput} />
    </motion.div>
  )
}

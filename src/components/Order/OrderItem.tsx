import { useApplication } from '@app/context/App'
import { getProRataAmount } from '@app/redux/dispatches/shopDispatcher'
import BoxItem from '@components/Box/BoxItem'
import BillingCycleModal from '@components/Modal/BillingCycleModal'
import { IDomainWithPrice } from '@lib/types/domain'
import { IProduct } from '@lib/types/product'
import React from 'react'
import { FaChevronCircleRight } from 'react-icons/fa'
import { IoClose } from 'react-icons/io5'
import { Colors } from '@constants/index'
import useTranslate from '@hooks/useTranslate'
import styles from './order.module.scss'

interface OrderItemProps {
  product: IProduct
  onDelete: () => void
  show?: boolean
  domain: IDomainWithPrice
}

/**
 * component used to display a single item in an order
 * @param product the details regarding the product
 * @param onDelete manages the behaviour when a product is deleted
 * @param show boolean used to conditionally render the order item
 * @param domain the domain this product item belongs to
 * @returns a JSX wrapper component that manages the layout of a single order item in the order summary page
 */

export default function OrderItem({
  product,
  onDelete,
  show,
  domain
}: OrderItemProps) {
  /**
   * hook that manages the billing cycle for the product
   */
  const [selectedProductBc, setSelectedProductBc] = React.useState<number>()

  /**
   * this extracts the currency used in the app from the context provider
   */
  const {
    config: { currency }
  } = useApplication()

  const { t } = useTranslate() //translation helper function

  /**
   * the billing cycle that is associated with this product
   */
  const defaultBillingCycle = product.defaultBillingCycle

  if (!show) {
    return null
  }

  /**
   * function that sets the selected product's billing cycle
   */
  const toggleBcHandler = (pid: number) => setSelectedProductBc(pid)

  /**
   * this function renders the prorata amount for the current product
   */
  const renderProdRata = () => {
    if (!['annually'].includes(defaultBillingCycle)) {
      return (
        <span
          className={`small mt-2 ${styles.product__price_rata}`}
          onClick={() => toggleBcHandler(product.pid)}
        >
          R{' '}
          {getProRataAmount(
            product?.pricing?.[currency]?.[defaultBillingCycle],
            defaultBillingCycle
          ).toFixed(2) +
            ' ' +
            t('common[prorata]')}
        </span>
      )
    }
  }

  return (
    <React.Fragment>
      <BoxItem cssClasses='position-relative'>
        <div>
          <h4 className='title-1 title-f-300'>{product?.name}</h4>
          <div className='d-flex align-items-center'>
            <h3 className='title-1 color-success me-3 mb-0'>
              R{product?.pricing?.[currency]?.[defaultBillingCycle]}
            </h3>

            <div
              className='d-flex align-items-center cursor'
              onClick={() => toggleBcHandler(product?.pid)}
            >
              <span className='me-2'>
                {defaultBillingCycle.charAt(0).toUpperCase() +
                  defaultBillingCycle.slice(1)}
              </span>
              <FaChevronCircleRight size={16} color={Colors.primary} />
            </div>
          </div>
          {renderProdRata()}
        </div>
        <IoClose
          size={20}
          color={Colors.danger}
          className={`cursor ${styles.order__item_close}`}
          onClick={onDelete}
        />
      </BoxItem>
      <BillingCycleModal
        {...{
          product,
          domain,
          visible: selectedProductBc === product.pid,
          onClose: () => setSelectedProductBc(null)
        }}
      />
    </React.Fragment>
  )
}

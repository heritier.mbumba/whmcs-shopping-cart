import { IDomainWithPrice } from '@lib/types/domain'
import { FaCheckCircle, FaRegCircle } from 'react-icons/fa'
import { Colors } from '@constants/index'
import styles from '../domain.module.scss'
import useTranslate from '@hooks/useTranslate'

interface AlternativeSearchProps {
  domains: IDomainWithPrice[]
  onSelectDomain: (domain: IDomainWithPrice) => void
  wasSelected: (domain: IDomainWithPrice) => boolean
}

export default function AlternativeDomainSearch({
  domains,
  onSelectDomain,
  wasSelected
}: AlternativeSearchProps) {
  const { t } = useTranslate() //translation helper function

  return (
    <div className='row pt-5'>
      <div>
        <div className='col-xs-12 col-md-8 offset-md-2'>
          <h4 className='heading_small'>
            {t('domain[alternative][heading]')}:
          </h4>
          <div className={styles.domain__alternatives}>
            {domains?.map((domain) => (
              <div
                className={`${styles.domain__alternatives_item} pb-4 pt-4 border-bottom`}
                key={domain?.tld}
                onClick={() => onSelectDomain(domain)}
              >
                <div className='d-block w-75'>
                  <h4 className='domain__name domain__name_medium'>
                    {domain?.name}
                  </h4>
                  <div className='d-flex align-items-start flex-column '>
                    <span className='color-pink heading_small'>
                      R{domain?.register}
                    </span>
                    <p className='yearpill mb-0'>
                      {t('component[timepill][year]')}
                    </p>
                    <span className={styles.domain__category}>
                      {
                        domain?.categories?.filter((c) =>
                          [
                            'Popular',
                            'Shopping',
                            'Technology',
                            'Education',
                            'Geographic'
                          ].includes(c)
                        )?.[0]
                      }
                    </span>
                  </div>
                </div>
                <button
                  className={`d-none d-lg-flex align-self-center btn ${
                    domain?.status ? 'btn-success' : 'btn-warning'
                  }`}
                >
                  {wasSelected(domain) && (
                    <FaCheckCircle
                      size={20}
                      color={Colors.light}
                      className='me-2'
                    />
                  )}
                  <span className='d-block me-4 heading_upper'>
                    {domain.status
                      ? t('button[register]')
                      : t('button[transfer]')}
                  </span>
                </button>
                <span
                  className={`d-lg-none d-flex flex-column align-items-center`}
                >
                  <span className={`d-block heading_tiny`}>
                    {domain?.status
                      ? t('button[register]')
                      : t('button[transfer]')}
                  </span>
                  <span>
                    {wasSelected(domain) ? (
                      <FaCheckCircle
                        size={30}
                        color={Colors.success}
                        className='m-2'
                      />
                    ) : (
                      <FaRegCircle
                        size={20}
                        color={Colors.grey}
                        className='m-2'
                      />
                    )}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

import { IDomainWithPrice } from '@lib/types/domain'
import { FaCheckCircle, FaTimesCircle } from 'react-icons/fa'
import { Colors } from '@constants/index'
import styles from '../domain.module.scss'
import { motion } from 'framer-motion'
import useTranslate from '@hooks/useTranslate'

interface MainSearchResultProps {
  primarySearch: IDomainWithPrice
  scrollToContinue?: () => void
  wasSelected: (domain: IDomainWithPrice) => boolean
}

export default function MainSearchResult({
  primarySearch,
  scrollToContinue,
  wasSelected
}: MainSearchResultProps) {
  const { t } = useTranslate() //translation helper function

  return (
    <div
      className={`d-flex align-items-center ${styles.domain__lookup_result_main}`}
    >
      <div className={`w-75 ${styles.domain__lookup_result_maincontent}`}>
        <h4 className='heading'>
          {primarySearch?.status
            ? t('domain[result][available]')
            : t('domain[result][unavailable]')}
        </h4>
        <h2
          className={`domain-name ${
            primarySearch?.status ? 'domain-name-success' : 'domain-name-failed'
          }`}
        >
          {primarySearch?.name}
        </h2>
        <span className=''>
          {primarySearch?.status
            ? t('domain[result][msgAvailable]')
            : t('domain[result][msgUnvailable]')}
        </span>
        <h4 className='domain-name-price upper mt-3 color-pink'>
          R
          {primarySearch?.status
            ? primarySearch?.register
            : primarySearch?.transfer}
        </h4>
        <span className='mb-4 d-block upper'>
          {primarySearch?.status
            ? t('component[timepill][year]')
            : t('domain[result][transfer]')}
        </span>
      </div>
      <button
        className={`btn btn${primarySearch?.status ? '-success' : '-warning'}`}
        onClick={scrollToContinue}
      >
        {wasSelected(primarySearch) && (
          <FaCheckCircle size={20} color={Colors.light} className='me-2' />
        )}
        {primarySearch?.status ? t('button[register]') : t('button[transfer]')}
      </button>
    </div>
  )
}

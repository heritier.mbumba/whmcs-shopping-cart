import BoxItem from '@components/Box/BoxItem'
import { IDomainWithPrice } from '@lib/types/domain'
import { FaCheckCircle } from 'react-icons/fa'
import { Colors } from '@constants/index'
import styles from './winter.module.scss'
import useTranslate from '@hooks/useTranslate'
import React from 'react'
import withErrorBundary from '@components/HOC/withErrorBundary'
import { useQuery } from '@apollo/client'
import { SEARCH_ALTERNATIVE_DOMAIN } from '@utils/graphql-type'
import { MoonLoader } from 'react-spinners'

interface DomainResultAlternativesProps {
  domains: string[]
  onSelect: (domain: IDomainWithPrice) => void
  selectedDomains: IDomainWithPrice[]
}

/**
 * UI component for displaying the alternative domain results
 * @param domains the domains provided from the WHMCS search response
 * @param onSelect helper function that handles the select behaviour of a domain
 * @param selectedDomains the domains a user selecetd
 * @returns alternative domains UI
 */

function DomainResultAlternatives({
  domains,
  onSelect,
  selectedDomains
}: DomainResultAlternativesProps) {
  const { t } = useTranslate() //translation helper function

  const {data, loading, error} = useQuery(SEARCH_ALTERNATIVE_DOMAIN, {
    variables: {
      domains
    }
  })

  const [alternativeDomains, setAlternativeDomains] = React.useState<IDomainWithPrice[]>()


  React.useEffect(()=>{
    if(!domains){
      throw new Error('Could not load alternatives')
    }
  }, [])

  React.useEffect(() => {

    if(data?.searchAlternativeDomains?.result){
      setAlternativeDomains(data?.searchAlternativeDomains?.result)
    }

  }, [loading, data])

  //checks if a domain has already been selected
  const isSelected = (domain: IDomainWithPrice) => {
    const item = selectedDomains?.find((d) => d.tld === domain.tld)

    if (item) {
      return true
    }
    return false
  }


  if(loading){
    return <div className='d-flex align-items-center justify-content-center h-100 mb-5 mt-3 flex-column'>
    <MoonLoader size={40} color='#20bfdd' />
    <p className='title-1 title-f-300 mt-4'>{t('common[pleasewait]')}</p>
  </div>
  }

  return (
    <div className='mt-4 d-none d-md-block'>
      <BoxItem cssClasses={`${styles.winter_domain__alternatives}`}>
        <h4 className='title-1 title-f-300 upper'>
          ({domains?.length}) - {t('domain[alternative][heading]')}
        </h4>

        <div className='domain__alternatives mt-4'>
          {alternativeDomains?.map((domain) => (
            <BoxItem
              cssClasses={`d-flex align-items-center justify-content-between flex-wrap`}
              boxShowOff
              key={domain?.tld}
            >
              <div>
                <h4 className='title-1 title-f-300'>{domain?.name}</h4>
                <span className='color-success'>
                  R{domain?.status ? domain?.register : domain?.transfer} | year
                </span>
              </div>
              <button
                className={`btn btn-${
                  domain?.status ? 'secondary' : 'warning'
                } ${styles.winter_domain__alternatives_btn}`}
                onClick={() => onSelect(domain)}
              >
                {isSelected(domain) && (
                  <FaCheckCircle
                    color={Colors.light}
                    size={20}
                    className='me-2'
                  />
                )}
                {domain?.status ? t('button[register]') : t('button[transfer]')}
              </button>
            </BoxItem>
          ))}
        </div>
      </BoxItem>
    </div>
  )
}

export default withErrorBundary(DomainResultAlternatives)

import { useShop } from '@app/context/Shop'
import BoxItem from '@components/Box/BoxItem'
import { Colors } from '@constants/index'
import useTranslate from '@hooks/useTranslate'
import { IDomainCheckoutOptions } from '@utils/default'
import { motion } from 'framer-motion'
import { FaCheckCircle, FaRegCircle } from 'react-icons/fa'
import styles from './winter.module.scss'

interface DomainResultActionProps {
  flows: IDomainCheckoutOptions[]
  alignTextCenter?: boolean
}

/**
 * UI component for displaying the different flows a user can select
 * @param flows //the different flows provided from the redux app state
 * @returns UI for guiding a user to select a cart flow for a domain
 */

export default function DomainResultAction({
  flows,
  alignTextCenter
}: DomainResultActionProps) {
  const { t } = useTranslate() //translation helper function
  const { onSelectedCheckoutOption, currentCheckoutFlow } = useShop()

  return (
    <motion.div
      className='mt-5'
      initial={{ translateY: 100, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
      transition={{ duration: 0.5 }}
    >
      <BoxItem>
        <h4
          className={`title-2 title-f-300 mt-3 ${
            alignTextCenter && 'text-center'
          }`}
        >
          {t('page[cart][title]')}
        </h4>

        <div className='mt-5'>
          <div className='container ps-0 pe-0'>
            <div className='row'>
              {/* maps over the different flows provided in the redux store and creates a UI component for a user */}
              {flows?.map((flow) => (
                <div
                  className='col-md-4'
                  key={flow?.key}
                  onClick={() => onSelectedCheckoutOption(flow?.key)}
                >
                  <BoxItem cssClasses={`cursor ${styles.winter_action}`}>
                    {flow?.key === currentCheckoutFlow && (
                      <FaCheckCircle
                        size={25}
                        className={`${styles.winter_action__icon}`}
                        color={Colors.success}
                      />
                    )}
                    {flow?.key !== currentCheckoutFlow && (
                      <FaRegCircle
                        size={25}
                        className={`${styles.winter_action__icon}`}
                        color={Colors.grey}
                      />
                    )}
                    <h3 className='title-1 title-f-300 pt-4'>{flow?.label}</h3>
                    <span className='upper'>From</span>
                    <h4 className='title-1 title-f-600 color-pink'>
                      R{flow?.overview?.pricing.toFixed(2)}
                    </h4>
                    <span>{flow?.overview?.billingcycle}</span>
                  </BoxItem>
                </div>
              ))}
            </div>
            {/* this is the UI for the express checkout option */}
            <div className='row mt-3 ps-3 pe-3'>
              <BoxItem
                cssClasses={`col-md-12 cursor ${styles.winter__express}`}
              >
                <div
                  className={`d-flex align-items-start`}
                  onClick={() => onSelectedCheckoutOption('express')}
                >
                  {currentCheckoutFlow === 'express' ? (
                    <FaCheckCircle
                      size={25}
                      className={`me-3`}
                      color={Colors.light}
                    />
                  ) : (
                    <FaRegCircle
                      size={25}
                      className='me-3'
                      color={Colors.light}
                    />
                  )}
                  <div>
                    <h3 className='title-1 title-f-300'>
                      {t('button[expresscheckout]')}
                    </h3>
                    <span>{t('checkoutOptions[domain][label]')}</span>
                  </div>
                </div>
              </BoxItem>
            </div>
          </div>
        </div>
      </BoxItem>
    </motion.div>
  )
}

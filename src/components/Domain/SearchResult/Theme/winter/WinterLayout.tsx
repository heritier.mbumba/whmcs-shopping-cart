import { useShop } from '@app/context/Shop'
import { addDomainDispatcher } from '@app/redux/dispatches/shopDispatcher'
import BoxItem from '@components/Box/BoxItem'
import { Colors } from '@constants/index'
import useAnalytics from '@hooks/useAnalytics'
import useTranslate from '@hooks/useTranslate'
import { IDomainWithPrice } from '@lib/types/domain'
import { CHECKOUT_OPTIONS } from '@utils/default'
import { motion } from 'framer-motion'
import { useRouter } from 'next/dist/client/router'
import React from 'react'
import { GrUpdate } from 'react-icons/gr'
import { useDispatch } from 'react-redux'
import DomainResultAction from './ResultAction'
import DomainResultAlternatives from './ResultAternatives'
import DomainResultSuccess from './ResultSuccess'
import styles from './winter.module.scss'

interface WinterLayoutProps {
  domains: IDomainWithPrice[]
  tld: string
  error?: Record<string, any>;
  alternativeDomains: string[]
  resetResultState: () => void
}

/**
 * UI for the winter themed layout
 * @param domains collection of the domains
 * @param tld list of all the tlds
 * @returns whatever I wrote 3 lines up
 */

export default function WinterLayout({
  domains,
  tld,
  error,
  alternativeDomains,
  resetResultState
}: WinterLayoutProps) {
  const [selectedDomains, setSelectedDomains] = React.useState<
    IDomainWithPrice[]
  >([])

  const { t } = useTranslate() //translation helper function

  //extracts the selected checkout flow from the redux app state
  const { currentCheckoutFlow, onFlowInit } = useShop()
  const router = useRouter()
  const dispatch = useDispatch()
  const LogEvent  = useAnalytics()

  React.useEffect(() => {
    const autoSelectedDomain = domains?.find((domain) => domain.tld === tld)
    if (autoSelectedDomain) {
      let selectedDomainsCp = selectedDomains.slice()
      const domainExist =
        selectedDomainsCp.findIndex(
          (domain) => domain.tld === autoSelectedDomain.tld
        ) !== -1
      if (domainExist) {
        selectedDomainsCp = selectedDomainsCp.filter(
          (domain) => domain.tld !== autoSelectedDomain.tld
        )
        setSelectedDomains(selectedDomainsCp)
      } else {
        selectedDomainsCp.push(autoSelectedDomain)
        setSelectedDomains(selectedDomainsCp)
      }
    }
  }, [domains])

  //handles where a user is directed to based off of their selected checkout option
  const onContinueHandler = () => {
    dispatch(addDomainDispatcher(selectedDomains))

    LogEvent().checkout(2, 'addToCart')

    if(!currentCheckoutFlow){
      router.push(`/cart?config=sitebuilder`)
      return onFlowInit()
    }
    if (currentCheckoutFlow === 'express') {
      return router.push('/checkout')
    }
    return router.push(`/cart?config=${currentCheckoutFlow}`)
  }

  //handles adding a domain to the selected domains collection
  const onSelectDomain = (domain: IDomainWithPrice) => {
    const domains: IDomainWithPrice[] = selectedDomains.slice()
    LogEvent().click('domain', domain)
    const domainWasSelectedIndex = domains.findIndex(
      (dm) => dm.name === domain.name
    )

    if (domainWasSelectedIndex === -1) {
      domains.push(domain)
    } else if (domainWasSelectedIndex > -1) {
      domains.splice(domainWasSelectedIndex, 1)
    }

    setSelectedDomains(domains)
  }

  if (error) {
    return (
      <div className={`row ${styles.winter_domain__result_action}`}>
        <div className='col-md-12'>
          <div
            className={`d-flex align-items-center justify-content-between mt-5 ${styles.domain__search_again}`}
          >
            <BoxItem>
              <h2>{error?.title}</h2>
              <p>{error?.message}</p>
            </BoxItem>
          </div>
        </div>
      </div>
    )
  }

  return (
    <React.Fragment>
      <div className={`row ${styles.winter_domain__result_action}`}>
        <div className='col-md-12'>
          <div
            className={`d-flex align-items-center justify-content-between mt-5 ${styles.domain__search_again}`}
          >
            <div
              className={`d-flex align-items-center cursor`}
              onClick={resetResultState}
            >
              <GrUpdate size={20} color={Colors.primary} className='me-3' />
              <h4 className='upper mb-0'>{t('common[tryAgain]')}</h4>
            </div>
            <h4 className='color-pink'>
              {selectedDomains?.length} {t('domain[selectedAmount]')}
            </h4>
          </div>
        </div>
      </div>
      <div className='row mb-5'>
        <div className='col-md-8'>
          <DomainResultSuccess
            {...{
              domain: domains[0],
              onSelect: (domain) => onSelectDomain(domain),
              selectedDomains
            }}
          />
          <DomainResultAction {...{ flows: CHECKOUT_OPTIONS() }} />
          <div className='mt-5'>
            <button
              className='btn btn-pink'
              disabled={selectedDomains.length === 0}
              onClick={onContinueHandler}
            >
              {t('button[continue]')}
            </button>
          </div>
        </div>
        <motion.div
          className='col-md-4'
          initial={{ translateX: 100, opacity: 0 }}
          animate={{ translateX: 0, opacity: 1 }}
          transition={{ duration: 0.5 }}
        >
          <DomainResultAlternatives
            {...{
              domains: alternativeDomains,
              onSelect: (domain) => onSelectDomain(domain),
              selectedDomains
            }}
          />
        </motion.div>
      </div>
    </React.Fragment>
  )
}

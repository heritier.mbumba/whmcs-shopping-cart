import BoxItem from '@components/Box/BoxItem'
import { IDomainWithPrice } from '@lib/types/domain'
import { motion } from 'framer-motion'
import { FaCheckCircle } from 'react-icons/fa'
import { Colors } from '@constants/index'
import styles from './winter.module.scss'
import useTranslate from '@hooks/useTranslate'
import withErrorBundary from '@components/HOC/withErrorBundary'
import React from 'react'

interface DomainResultSuccessProps {
  domain: IDomainWithPrice
  onSelect: (domain: IDomainWithPrice) => void
  selectedDomains: IDomainWithPrice[]
}

/**
 * UI for displaying the main domain search result
 * @param domain the main domain that has been searched based off of the primary tld that has been selected
 * @param onSelect function that handles behavior when the domain has been selected
 * @param selectedDomains all the domains that have been selected
 * @returns react element
 */

function DomainResultSuccess({
  domain,
  onSelect,
  selectedDomains
}: DomainResultSuccessProps) {
  const { t } = useTranslate() //translation helper function


  React.useEffect(()=>{
    if(!domain){
      throw new Error('Could not load your result')
    }
  }, [])

  return (
    <motion.div
      className='mt-4'
      initial={{ translateY: -100, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
      transition={{ duration: 0.5 }}
    >
      <BoxItem cssClasses={`d-flex align-items-end justify-content-between`}>
        <div>
          <h1 className={`title-2 color-${domain?.status ? 'success' : 'warning'}`}>
            {domain?.status ? t('domain[result][available]') : t('domain[result][unavailable]')}
          </h1>
          <h4 className='title-2 title-f-300 mt-4'>{domain?.name}</h4>
          <span className={`d-block color-${domain?.status ? 'success' : 'warning'}`}>{domain?.status ? t('domain[result][msgAvailable]') : t('domain[result][msgUnvailable]')}</span>
          <button
            className={`btn btn-${domain?.status ? 'success' : 'warning'} mt-4`}
            onClick={() => onSelect(domain)}
          >
            {selectedDomains?.filter((d) => d.tld === domain.tld).length >
              0 && (
              <FaCheckCircle color={Colors.light} size={20} className='me-2' />
            )}
            {domain?.status ? t('button[register]') : t('button[transfer]')}
          </button>
        </div>
        <div className='text-end'>
          <h3
            className={`title-2 title-f-600 color-pink ${styles.winter_domain__result_price}`}
          >
            R{domain?.status ? domain?.register : domain?.transfer}
          </h3>
          <span>{t('component[timepill][year]')}</span>
        </div>
      </BoxItem>
    </motion.div>
  )
}

export default withErrorBundary(DomainResultSuccess)
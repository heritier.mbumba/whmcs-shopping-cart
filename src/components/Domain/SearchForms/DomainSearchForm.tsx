import { useLazyQuery, useQuery } from '@apollo/client';
import { useApplication } from '@app/context/App';
import useTranslate from '@hooks/useTranslate';
import { IDomainTld, IDomainWithPrice } from '@lib/types/domain';
import { TLDS } from '@utils/domain';
import { GET_DOMAIN_TLDS, SEARCH_DOMAIN } from '@utils/graphql-type';
import React from 'react';
import DefaultDomainSearchForm from './DefaultForm';

const DOMAIN_REGEX_COZA = /^[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/
const DOMAIN_REGEX_INTER = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/
const LOCAL_EXT = ['co.za']

interface DomainSearchFormProps {
  style:
    | 'default'
    | 'summer'
    | 'winter'
    | 'christmas'
    | 'domain-promo'
    | 'product-promo'
    | 'security'

  onResultHandler?: (
    tld: string,
    domains: IDomainWithPrice[],
    alternativeDomains: string[]
  ) => void;
  query?: {
    domain: string;
    ext?: string
  }
}

/**
 * wrapper component that passes data to the domain search form
 * @param onResultHandler helper function that sets the results in the redux state when a domain has been searched
 * @returns a wrapper component for the domain search form
 */

const DomainSearchForm = ({
  style,
  query,
  onResultHandler
}: DomainSearchFormProps) => {

  const [tlds, setTlds] = React.useState<IDomainTld[]>()

  /**
   * Application config state
   */
  const {
    config: { supportedDomainTlds }
  } = useApplication()

  /**
   * Get domain tlds GraphQL
   */
  const { data, loading, error } = useQuery(GET_DOMAIN_TLDS, {variables: supportedDomainTlds})

  /**
   * Domain 
   */
  const [domain, setDomain] = React.useState<{ name?: string; tld: string }>({tld: 'co.za'})

  const [alternativeDomains, setAlternativeDomains] = React.useState<string[]>()

  /**
   * Selected tlds
   */
  const [selectedTlds, setSelectedTlds] = React.useState<string[]>(['co.za'])

  /**
   * Validation error
   */
  const [validateError, setValidateError] =
    React.useState<Record<string, any>>()

  /**
   * Search domain availability GraphQL request
   */
  const [
    searchDomainHandler,
    { data: dData, loading: dLoading, error: dError }
  ] = useLazyQuery(SEARCH_DOMAIN)

  const {t} = useTranslate()


  /**
   * Search domain through query string
   */
  React.useEffect(()=>{
    
    if(query && Object.keys(query).length > 0){
     
      /**
       * Get domain name and ext
       */
      const {domain: qDomain, ext} = query
    
      if (!qDomain) {
        return setValidateError({
          key: 'domainNameError',
          message: t('domain[errorName][message]'),
          title: t('domain[errorName][title]')
        })
      }

      //capture user tld
      let tld = domain.tld

      if(ext){
        tld = ext
      }
      const [name, ...anotherTld] = qDomain.split('.')
      

      if (anotherTld.length > 0) {
        tld = anotherTld.join('.')
        const tldsCp = selectedTlds?.slice()
        
        if (!tldsCp.includes(tld)) {
          if(!TLDS.includes(`.${tld}`)){
           
            setValidateError({
              title: t('domain[invalidextension]'), 
              message: tld +  t('domain[invalidextension][message]')
            })
            return
          }else {
            setDomain(prev => ({...prev, tld}))
            tldsCp.push(tld)
            
          }
        }
        setSelectedTlds(tldsCp)
      }

      let domains: string[] = []

      //Check if the domain search is supported
      const filteredTlds = selectedTlds.filter((tld) =>
        TLDS.includes(`.${tld}`)
      )
      const filteredSupportedTlds = supportedDomainTlds.filter(
        (tld) => !filteredTlds.includes(tld)
      )

      if (filteredTlds.length > 2) {
        domains = filteredTlds.map((tld) => `${name + '.'}${tld}`)
      } else {
        domains = filteredSupportedTlds?.map((tld) => name + tld)
      }
     
      
      setAlternativeDomains(domains?.filter(domain =>!domain.includes(`${name}.${tld}`)))
      
      searchDomainHandler({
        variables: {
          domains: domains?.filter(domain =>domain === `${name}.${tld}`)
        }
      })
      
    }
  }, [query])


  /**
   * Get Domain tlds result and dispatched to redux store
   */
  React.useEffect(() => {
    if (data?.getDomainTlds?.result) {
      setTlds(data?.getDomainTlds?.result)
    }
  }, [loading, data])

  /**
   * Domain search handler
   * @param event
   * @returns
   */
  const onFormSubmitHandler = React.useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault()

      if (!domain.name) {
        return setValidateError({
          key: 'domainNameError',
          message: t('domain[errorName][message]'),
          title: t('domain[errorName][title]')
        })
      }

      //capture user tld
      let tld = domain.tld
      const [name, ...anotherTld] = domain.name.split('.')
      

      if (anotherTld.length > 0) {
        tld = anotherTld.join('.')
        
        
        const tldsCp = selectedTlds?.slice()
        
        
        if (!tldsCp.includes(tld)) {
          if(!TLDS.includes(`.${tld}`)){
           
            setValidateError({
              title: t('domain[invalidextension]'), 
              message: tld +  t('domain[invalidextension][message]')
            })
            return
          }else {
            setDomain(prev => ({...prev, tld}))
            tldsCp.push(tld)
            
          }
        }
        setSelectedTlds(tldsCp)
      }

      let domains: string[] = []

      //Check if the domain search is supported
      const filteredTlds = selectedTlds.filter((tld) =>
        TLDS.includes(`.${tld}`)
      )
      const filteredSupportedTlds = supportedDomainTlds.filter(
        (tld) => !filteredTlds.includes(tld)
      )

      if (filteredTlds.length > 2) {
        domains = filteredTlds.map((tld) => `${name + '.'}${tld}`)
      } else {
        domains = filteredSupportedTlds?.map((tld) => name + tld)
      }
      
     
      
      setAlternativeDomains(domains?.filter(domain =>!domain.includes(`${name}.${tld}`)))
      console.log(domains?.filter(domain =>domain === `${name}.${tld}`));
      
      searchDomainHandler({
        variables: {
          domains: domains?.filter(domain =>domain === `${name}.${tld}`)
        }
      })
    },
    [domain, dData, dError, selectedTlds, dLoading]
  )

  /**
   * Domain search result effect
   */

  React.useEffect(() => {
    if (dData?.searchDomains) {

      const {result, error} = dData?.searchDomains

      if(error){
        setValidateError(error)
      }
      
      onResultHandler(
        domain?.tld || selectedTlds[0],
        result,
        alternativeDomains
      )
    }
  }, [dLoading, dData])

  /**
   * Get Domain name
   * @param event
   * @param key
   */
  const onValueChange = (
    event: React.FormEvent<HTMLInputElement>,
    key: string
  ) => {
    event.preventDefault()
    setValidateError(null)
    const value = event.currentTarget.value

    setDomain((prev) => ({ ...prev, [key]: value }))
  }

  /**
   * Select domain tld handler
   * @param tld
   * @param key
   * @returns
   */
  // const onSelectTld = (tld: IDomainTld, key: string) => setDomain(prev=>({...prev, [key]:tld.name}))

  const onSelectTld = (tld: IDomainTld) => {
    const tldsCp = selectedTlds.slice()
    if (tldsCp.includes(tld?.name)) {
      tldsCp.splice(tldsCp.indexOf(tld?.name), 1)
    } else {
      tldsCp.push(tld.name)
    }

    setSelectedTlds(tldsCp)
  }


  switch (style) {
    default:
      return (
        <DefaultDomainSearchForm
          {...{
            tlds,
            selectedTlds,
            loading: dLoading,
            onFormSubmitHandler,
            onValueChange,
            onSelectTld,
            error: validateError
          }}
        />
      )
  }
}

export default DomainSearchForm

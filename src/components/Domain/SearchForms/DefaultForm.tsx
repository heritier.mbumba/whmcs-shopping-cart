import DomainTldsContent from '@components/Modal/DomainTldsContent'
import LoadingSpinner from '@components/Modal/LoadingSpinner'
import ModalDefault, { IDefaultModalRef } from '@components/Modal/ModalDefault'
import { IDomainSearchForm, IDomainTld } from '@lib/types/domain'
import React from 'react'
import { BiSearch } from 'react-icons/bi'
import { HiChevronRight } from 'react-icons/hi'
import { Colors } from '@constants/index'
import useTranslate from '@hooks/useTranslate'
import styles from './searchforms.module.scss'
import { motion } from 'framer-motion'

interface DefaultDomainSearchFormProps extends IDomainSearchForm {}

/**
 * component that displays the form and fields for searching a domain
 * @param tlds //the different domain extensions
 * @param onFormSubmitHandler //handles submitting the form
 * @param onValueChange //handles updating the value in the domain search input field
 * @param selectedTlds //handles the tlds a user selects
 * @param onSelectTld //handles behaviour when a user selects a tld
 * @returns a UI for a user to search for a domain and associate tlds with it
 */
export default function DefaultDomainSearchForm({
  tlds,
  loading,
  onFormSubmitHandler,
  onValueChange,
  selectedTlds,
  onSelectTld,
  error
}: DefaultDomainSearchFormProps) {
  const { t } = useTranslate() //translation helper function
  const domainTldRef = React.useRef<IDefaultModalRef>()

  /**
   * On Select domain tld handler
   * @param tld
   */
  const onSelectTldHandler = (tld: IDomainTld) => {
    onSelectTld(tld)
    // domainTldRef?.current.close()
  }

  return (
    <div
      className={`d-flex align-items-center justify-content-center ${styles.form__domainsearch}`}
      id='domain-search-form'
    >
      <div className={`${styles.form__domainsearch_customize} text-center`}>
        <h4 className='title-1 title-f-400'>
          {t('form[domainregisterandtransfer]')}
        </h4>
        <h1 className='title-3 title-f-600'>{t('form[choosewebaddress]')}</h1>
        <form
          onSubmit={onFormSubmitHandler}
          className={`${styles.form__domainsearch_customizeform}`}
        >
          <div className='input-group mb-3'>
            <input
              type='text'
              className={`form-control ${styles.form__domainsearch_customizeformfield}`}
              placeholder={t('form[choosewebaddressfield]')}
              aria-label={t('form[choosewebaddressfield]')}
              aria-describedby='domain search'
              onChange={(event) => onValueChange(event, 'name')}
            />
            <button
              className='btn btn-pink input-group-text'
              id='basic-addon2'
              type='submit'
              aria-label='Search'
            >
              <BiSearch size={20} />
            </button>
          </div>
        </form>
        <button
          className={`mt-3 btn btn-default ${styles.form__domainsearch_customizesearch}`}
          onClick={() => domainTldRef.current.open()}
        >
          <span>
            {t('form[choosewebaddresscustomize]')}{' '}
            {selectedTlds?.length > 1 &&
              `${selectedTlds?.length} ${t('common[selected]')}`}
          </span>
          <HiChevronRight size={20} color={Colors.light} />
        </button>
        {error && (
          <motion.div
            className='alert alert-danger mt-3'
            initial={{ scale: 0, opacity: 0 }}
            animate={{ scale: 1, opacity: 1 }}
            transition={{ duration: 0.5 }}
            exit={{ scale: 0, opacity: 0 }}
          >
            <strong>{error?.title}</strong>
            <p>{error?.message}</p>
          </motion.div>
        )}
      </div>

      {loading && (
        <LoadingSpinner
          {...{
            title: t('modal[loading][checking]'),
            description: t('common[pleasewait]')
          }}
        />
      )}

      <ModalDefault
        ref={domainTldRef}
        title={t('modal[domainTLD][title]')}
        description={t('modal[domainTLD][description]')}
        renderContent={() => (
          <DomainTldsContent
            {...{ tlds, selectedTlds, onSelectChange: onSelectTldHandler }}
          />
        )}
      />
    </div>
  )
}

import { IDomainWithPrice } from '@lib/types/domain'
import React from 'react'
import WinterLayout from './SearchResult/Theme/winter/WinterLayout'

interface DomainLookupResultProps {
  result: {
    tld: string
    domains: IDomainWithPrice[]
    error?: Record<string, any>;
    alternativeDomains: string[];
  }
  resetResultState?: () => void
  theme?: 'winter' | 'summer'
}

/**
 * Wrapper component for the main layout displays. Depending on the theme provided, it will return the layout related to the theme
 * @param theme the theme variable defined to determine which layout to load
 * @param result the serach results sent back from the WHMCS domain search query
 * @param resetResultState resets the search results
 * @returns
 */

export default function DomainLookupResult({
  theme,
  result,
  resetResultState
}: DomainLookupResultProps) {
  switch (theme) {
    default:
      return (
        <section className='domain__result'>
          <div className='container'>
            <WinterLayout {...{ ...result, resetResultState }} />
          </div>
        </section>
      )
  }
}

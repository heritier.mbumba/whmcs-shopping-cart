import { IDomainWithPrice } from '@lib/types/domain'
import { IProduct } from '@lib/types/product'
import React from 'react'
import ModernProductItem from './items/modern'
import StandardProductItem from './items/standard'

interface RenderProductsProps {
  products: IProduct[]
  currency?: string
  domains: IDomainWithPrice[]
  theme?: 'modern' | 'standard'
}

/**
 * Component that wraps a product
 * Renders the component based off a theme
 * @param products the products passed through to be rendered
 * @param domains the current domains selected by the user
 * @param theme the theme layout to be used for a product's layout
 * @returns JSX element that displays a collection of products based off of a theme
 */
export default function RenderProducts({
  products,
  domains,
  currency,
  theme
}: RenderProductsProps) {
  React.useEffect(() => {}, [products])

  const renderSelectedTheme = (product: IProduct) => {
    switch (theme) {
      case 'modern':
        return (
          <ModernProductItem
            key={product?.pid}
            {...{
              product,
              domains,
              theme
            }}
          />
        )

      default:
        return (
          <StandardProductItem
            {...{
              product,
              domains
            }}
            key={product?.pid}
          />
        )
    }
  }

  return (
    <div className='row'>
      {products?.map((product) => renderSelectedTheme(product))}
    </div>
  )
}

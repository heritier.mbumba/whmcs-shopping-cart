import { useLazyQuery } from '@apollo/client'
import { useApplication } from '@app/context/App'
import { useShop } from '@app/context/Shop'
import withErrorBundary from '@components/HOC/withErrorBundary'
import useTranslate from '@hooks/useTranslate'
import { ISupportedProductConfig } from '@lib/types/api'
import { IProduct } from '@lib/types/product'
import { GET_ESSENTIALS_PRODUCTS } from '@utils/graphql-type'
import React from 'react'
import { MoonLoader } from 'react-spinners'
import RenderProducts from './RenderProducts'

const ESSENTIAL_TYPE = {
  ssl: {
    title: 'SSL Certificate',
    description: ''
  },
  email: {
    title: 'Business Email',
    description: ''
  },
  'domain-essentials': {
    title: 'Domain Essentials',
    description: ''
  },
  'cloud-backup': {
    title: 'Cloud backup',
    description: ''
  }
}

interface EssentialProductsProps {
  flow: string
  group: ISupportedProductConfig
}

/**
 * Component that displays a UI for essential products a user can add to their domain
 * @param flow the current flow based off of the value of the 'config' in a user's browser
 * @param group collection of products related to the current flow
 * @returns JSX element that provides a UI for essential products
 */
function EssentialProducts({
  flow,
  group
}: EssentialProductsProps) {
  /**
   * Hook to manage the essential products received from the GraphQL query
   */
  const [essentialProducts, setEssentialProducts] =
    React.useState<{ type: string; products: IProduct[] }[]>()

  /**
   * getEssentialsProducts is a function used to fetch data
   */
  const [getEssentialsProducts, { data, loading }] = useLazyQuery(
    GET_ESSENTIALS_PRODUCTS
  )

  /**
   * Retrieves the currency used from the application context
   */
  const {
    config: { currency }
  } = useApplication()

  /**
   * Retrives the items from the Shop context
   */
  const { items } = useShop()

  const { t } = useTranslate() //translation helper function

  /**
   * When the group or flow of the application changes, this function executes
   * Grabs the 'essentials' from the 'group'
   * Passes the essentials as a variable and returns an array of results
   */
  React.useEffect(() => {

    if(!group){
      throw new Error('Could not send the request')
    }
    if (group) {
      const { essentials } = group
      getEssentialsProducts({
        variables: {
          essentials: essentials?.map((ess) => ({
            gid: ess.id,
            package: ess?.supportedPackage,
            type: ess.type,
            billingCycle: ess?.defaultBillingCycle
          }))
        }
      })
    }
  }, [group, flow])

  /**
   * This hook runs when a query is loading
   * It sets the essentialProducts state with the data that has been returned
   */
  React.useEffect(() => {
    if (data?.getEssentialProducts) {
      setEssentialProducts(data?.getEssentialProducts)
    }
  }, [loading])

  /**
   * Loading UI for when a query is loading
   */
  if (loading) {
    return (
      <div className='d-flex align-items-center justify-content-center h-100 mb-5 mt-3 flex-column'>
        <MoonLoader size={40} color='#20bfdd' />
        <p className='title-1 title-f-300 mt-4'>{t('common[pleasewait]')}</p>
      </div>
    )
  }

  return (
    <div className='product__essentials'>
      <div className=''>
        <span>{t('common[essential][description]')}</span>
      </div>
      {essentialProducts?.map((essential) => (
        <div key={essential?.type} className='mb-5 mt-5'>
          <h3 className='title-1 title-f-300 upper mb-4'>
            {ESSENTIAL_TYPE?.[essential?.type]?.title}
          </h3>
          <div className='product__essentials'>
            <RenderProducts
              {...{
                products: essential?.products,
                currency,
                domains: items?.map((item) => item.domain),
                theme: 'standard'
              }}
            />
          </div>
        </div>
      ))}
    </div>
  )
}

export default withErrorBundary(EssentialProducts)

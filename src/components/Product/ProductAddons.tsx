import { useQuery } from '@apollo/client'
import useTranslate from '@hooks/useTranslate'
import { IProductAddons } from '@lib/types/product'
import { GET_PRODUCT_ADDONS } from '@utils/graphql-type'
import React from 'react'
import { MoonLoader } from 'react-spinners'

interface ProductAddonsProps {
  pid: number
  name?: string
}

/**
 * Component that manages the UI for displaying the user a collection of addon products for their domain
 * @param pid the id of a product
 * @param name the name of the product
 * @returns JSX element for displaying a UI for domain addons
 */
export default function ProductAddons({ pid, name }: ProductAddonsProps) {
  /**
   * Hook used to manage the addons state
   */
  const [addons, setAddons] = React.useState<IProductAddons[]>()

  /**
   * Hook used to manage the state of a response error
   */
  const [responseError, setResponseError] =
    React.useState<Record<string, any>>()

  /**
   * Query that gets executed everytime the component mounts
   */
  const { data, loading, error } = useQuery(GET_PRODUCT_ADDONS, {
    variables: {
      pid,
      name
    }
  })

  const { t } = useTranslate() //translation helper function

  /**
   * This hook executes when the GraphQL query is loading
   * Sets the addons state with the data returned from the query
   */
  React.useEffect(() => {
    if (data?.getProductAddons) {
      const error = data?.getProductAddons?.find((addon) => addon?.status)
      if (error) {
        setResponseError(error)
      } else {
        setAddons(data?.getProductAddons)
      }
    }
  }, [loading])

  /**
   * Displays a loading UI when a query is loading
   */
  if (loading) {
    return (
      <div className='d-flex align-items-center justify-content-center h-100 mb-5 mt-3 flex-column'>
        <MoonLoader size={40} color='#20bfdd' />
        <p className='title-1 title-f-300 mt-4'>{t('common[pleasewait]')}</p>
      </div>
    )
  }

  /**
   * Displays a error UI when an error occurs
   */
  if (responseError) {
    return (
      <div className='text-center'>
        <h1 className='title-3 title-f-300'>
          {t('flowTitle[hsoting][essentail]')}
        </h1>
        <span>{responseError?.message}</span>
      </div>
    )
  }

  return (
    <div className='product__essentials'>
      <h1 className='title-3 title-f-300'>
        {t('flowTitle[hosting][essentials]')}
      </h1>
      <span>{t('common[essential][description]')}</span>
      {addons?.map((addon) => (
        <div key={addon?.id} className='mb-5 mt-5'>
          <h3 className='title-1 title-f-300 upper mb-4'>{addon?.name}</h3>
          <div className='product__essentials'></div>
        </div>
      ))}
    </div>
  )
}

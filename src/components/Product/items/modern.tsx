import { useApplication } from '@app/context/App'
import { useShop } from '@app/context/Shop'
import { getProRataAmount } from '@app/redux/dispatches/shopDispatcher'
import BoxItem from '@components/Box/BoxItem'
import ModalDefault, { IDefaultModalRef } from '@components/Modal/ModalDefault'
import useProducts from '@hooks/useProducts'
import useTranslate from '@hooks/useTranslate'
import { IDomainWithPrice } from '@lib/types/domain'
import { IProduct, ProductItemProps } from '@lib/types/product'
import React from 'react'
import styles from '../product.module.scss'
import ProductConfig from './config'

interface ModernProductItemProps extends ProductItemProps {}

/**
 * Component used to display the details of a specified product
 * @param product the product details that are used to render the data related to it
 * @param domains the current domains attached to the selected product
 * @returns A JSX element for displaying a UI related to a product
 */
export default function ModernProductItem({
  product,
  domains
}: ModernProductItemProps) {
  /**
   * Extracts the currency and product groups the app supports from the Application context
   */
  const {
    config: { currency, supportedProductGroups }
  } = useApplication()

  /**
   * Creates a ref for the modal to point back to
   */
  const configModalRef = React.useRef<IDefaultModalRef>()

  /**
   * Extracts helper data from the Shop context
   * @param items the items in the cart state of redux
   * @param onAddProductToCart helper function that manages the behaviour when a product is added to the cart
   */
  const { items, onAddProductToCart } = useShop()

  /**
   * Extracts the description of a product using the product's type and product id
   */
  const { getProductDescription } = useProducts()

  /**
   * A collection of items that have already been selected
   */
  const selected = items
    ?.find((item) => item.products?.find((p) => p.pid === product.pid))
    ?.products.find((p) => p.pid === product.pid)

  /**
   * Extract the typename and cycles related to a product's billingcycles
   */
  const { __typename, ...cycles } = product?.billingCycles

  /**
   * Extracts the name of the keys from the billing cycles
   */
  const keys = Object.keys(cycles)

  /**
   * Collection of the billing cycles
   * Maps over the keys and returns a new array that contains the label and value of each billing cycle
   */
  const billingCycles = keys
    ?.map((key) => ({
      label: key,
      value: cycles[key]
    }))
    .filter((bc) => bc.value !== '-1.00')

  /**
   * TODO:
   * This will be added later
   */
  const addons = []

  const { t } = useTranslate() //translation helper function;

  /**
   * Handles the behavior for when a product is selected
   * Opens the modal related to the current product
   * Sets the title of the modal to the name of the current product
   */
  const onProductSeleted = () => {
    configModalRef.current.open()
    configModalRef.current.renderTitle(`${product?.name}`)
  }

  /**
   * Close product config modal
   */
  const onFinishConfig = () => configModalRef.current.close()

  /**
   * Split product title which has : in the string
   */
  const nicerTitle = () => {
    const [top, ...bottom] = product?.name?.split(':')

    return {
      top,
      bottom: bottom.join(' ')
    }
  }

  /**
   * Handles the behavior when a product is added to the cart
   * @param domain the domain the product should be linked to
   * @param product the product that should be linked to the domain
   */
  const onAddProductTocartHandler = (
    domain: IDomainWithPrice,
    product: IProduct
  ) => {
    const group = supportedProductGroups?.find(
      (group) => group.id === product.gid
    )
    onAddProductToCart(domain, {
      ...product,
      freeDomains: group?.freeDomains?.[product?.pid]
    })
  }

  /**
   * Displays the pro-rata data for the pricing of a product
   * @param product the product for which to calculate the pro-rata
   * @returns JSX element that contains the Ui for a products pro-rata data
   */
  const renderProRataContent = (product: IProduct) => {
    if (['annually'].includes(product?.defaultBillingCycle)) {
      return null
    }

    return (
      <div className=''>
        <span className={`text-muted ${styles.product__price_billingcycle}`}>
          {t('common[totalduetoday]')}
        </span>
        <span className={`paragraph color-pink me-2`}>
          R
          {getProRataAmount(
            product?.pricing?.[currency]?.[product?.defaultBillingCycle],
            product?.defaultBillingCycle
          ).toFixed(2)}
        </span>
      </div>
    )
  }

  /**
   * Renders the action button that allows a user to add or remove a product related to a domain
   * @param product the product for which to render the button for
   * @returns JSX element that provides a UI for adding/removing products
   */
  const renderActionBtn = (product: IProduct) => {
    /**
     * Maps over the items and returns a new array containing the domain names
     */
    const domains = items?.map((item) => item.domain.name)

    /**
     * Returns a UI for when the user only has one domain in their cart
     */
    if (domains?.length === 1) {
      return (
        <button
          className={`btn btn_custom btn_custom_${
            selected?.pid === product.pid ? 'success' : 'secondary'
          }`}
          onClick={() => onAddProductTocartHandler(items[0].domain, product)}
        >
          {selected?.pid === product.pid
            ? t('common[added]')
            : t('common[addProductToCart]')}
        </button>
      )
    }

    /**
     * Returns a UI when a user has multiple domains in their cart
     */
    return (
      <button
        className={`btn btn_custom btn_custom_${
          selected?.pid === product.pid ? 'success' : 'secondary'
        }`}
        onClick={onProductSeleted}
      >
        {selected?.pid === product.pid
          ? t('common[selected]')
          : t('common[select]')}
      </button>
    )
  }

  return (
    <div className='col-md-4 mt-5'>
      <BoxItem
        cssClasses={`text-center ${styles.product__standard_container} `}
      >
        <span className='upper'>{nicerTitle().bottom}</span>
        <h4 className='title-1 color-pink'>{nicerTitle().top}</h4>
        <div className='mt-5'>
          <span className='small text-muted'>
            {product?.defaultBillingCycle?.charAt(0)?.toUpperCase() +
              product?.defaultBillingCycle?.slice(1)}
          </span>
          <h4 className='title-2 title-f-600 me-2 color-primary'>
            R{product?.pricing?.ZAR?.[product?.defaultBillingCycle]}
          </h4>
          <div>{renderProRataContent(product)}</div>
        </div>
        <ul className={`mt-5 mb-5 ${styles.product__modern_features}`}>
          {getProductDescription(product?.type, product?.pid)?.features?.map(
            (f) => (
              <li key={f}>{f}</li>
            )
          )}
        </ul>
        <div className={`${styles.product__modern_action}`}>
          {renderActionBtn(product)}
        </div>
      </BoxItem>
      <ModalDefault
        ref={configModalRef}
        renderContent={() => (
          <ProductConfig
            {...{ domains, billingCycles, addons, product, onFinishConfig }}
          />
        )}
        onCloseHandler={() => configModalRef.current.close()}
      />
    </div>
  )
}

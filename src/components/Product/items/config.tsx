import { useShop } from '@app/context/Shop'
import useProducts from '@hooks/useProducts'
import useTranslate from '@hooks/useTranslate'
import { IDomainWithPrice } from '@lib/types/domain'
import { IBillingCycle, IProduct, IProductAddons } from '@lib/types/product'
import { motion } from 'framer-motion'
import { useRouter } from 'next/dist/client/router'
import React from 'react'
import styles from '../product.module.scss'

interface ProductConfigProps {
  title?: string
  description?: string
  domains?: IDomainWithPrice[]
  billingCycles: IBillingCycle[]
  addons?: IProductAddons[]
  product?: IProduct
  onFinishConfig?: () => void
}

interface IProductConfigState {
  steps: string[]
  current: string
}

interface LinkedProduct extends IProduct {
  domain: IDomainWithPrice
}

const PRODUCT_CONFIG_DESCRIPTION = () => {
  const { t } = useTranslate() //translation helper function

  const PRODUCT_CONFIG_DESCRIPTION = {
    domains: {
      title: t('productConfigDesc[domains][title]'),
      description: t('productConfigDesc[domains][description]')
    },
    billing: {
      title: t('productConfigDesc[billing][title]'),
      description: t('productConfigDesc[billing][description]')
    },
    addons: {
      title: t('productConfigDesc[addons][title]'),
      description: t('productConfigDesc[addons][description]')
    }
  }

  return PRODUCT_CONFIG_DESCRIPTION
}

/**
 * This provides the initial starting state for the steps the app uses
 */
const productConfigState: IProductConfigState = {
  steps: ['domains'],
  current: 'domains'
}

/**
 * Redux reducer used to dispatch the 'NEXT' action to the redux store to update the current product step
 */
const reducer = (
  state = productConfigState,
  action: { type: string; payload: any }
) => {
  switch (action.type) {
    case 'NEXT':
      return {
        ...state,
        current: action.payload
      }

    default:
      return state
  }
}

/**
 * Component that displays a list of products relatied to the current step in the app state
 * @param domains the current domains in the cart
 * @param billingCycles the different billing cycles related to a product
 * @param addons the different addon products you can link to a domain
 * @param product the products that have been returned from the GET_PRODUCTS query in GraphQL
 * @param onFinishConfig function that closes the current modal using a ref
 * @returns a JSX element that conditionally renders the products associated to the current step of the cart flow
 */

export default function ProductConfig({
  domains,
  billingCycles,
  addons,
  product,
  onFinishConfig
}: ProductConfigProps) {
  /**
   * Hook used to dispatch action types
   */
  const [state, dispatch] = React.useReducer(reducer, productConfigState)

  /**
   * Hook used to set and update the state of the domain
   */
  const [domain, setDomain] = React.useState<IDomainWithPrice>()

  /**
   * Extracts data from the redux Shop store
   * @param onAddProductToCart handles the fucntionality for when an itema is added to the cart
   * @param items the current items in the cart in the redux state
   * @param onUpdateProductBillingCycle handles the functionality for when the billing cycle of a product is changed
   */
  const { onAddProductToCart, items, onUpdateProductBillingCycle } = useShop()

  /**
   * Functionality from Next used for routing a user
   */
  const router = useRouter()

  /**
   * Extracts helper function created in useProducts file
   * @param getProductFreeDomains checks if the domain is free on a selected product
   * @param isProductSelected checks if the product has been selected in the items state in redux
   * @param getUsedDomains checks for domains that have products attached to them
   */
  const { getProductFreeDomains, isProductSelected, getUsedDomains } =
    useProducts()

  /**
   * This is done intentionaly.
   * The product props does not get updated so,
   * Access directly redux and get the update product
   */
  const realyTimeProduct = items
    .find((item) => item.products.find((p) => p.pid === product.pid))
    ?.products.find((p) => p.pid === product.pid)

  /**
   * This function runs every time the 'product' from the parent component changes
   * It searches through the 'items' and then though the 'products' in each item
   * If the product in the item's type equals the type of the parent 'product', we set the domain state
   */
  React.useEffect(() => {
    if (product) {
      const productDomain = items.find((item) =>
        item.products.find((p) => p.type === product?.type)
      )?.domain
      setDomain(productDomain)
    }
  }, [product])

  /**
   * Next config
   */
  const nextConfig = React.useCallback(
    (currentStep: string) => {
      const currendIndex = state?.steps?.indexOf(currentStep)
      let nextIndex = 0
      if (currendIndex === state?.steps?.length - 1) {
        return onFinishConfig()
      }
      nextIndex = currendIndex + 1
      dispatch({ type: 'NEXT', payload: state?.steps[nextIndex] })
    },
    [state.current]
  )

  /**
   * Helper function used to render the content for a step depending on what the current step in the redux flow state is
   * @param currentStep The current step set in the redux state
   * @returns A JSX element containing the UI for products related to the current step
   */
  const content = (currentStep: string) => {
    const { t } = useTranslate() //translation helper function

    if (currentStep === 'domains') {
      if (domains.length === 0) {
        return (
          <div className='mt-4 mb-5'>
            <h4 className='title-1 title-f-300'>
              {t('page[noDomain][title]')}
            </h4>
            <span>{t('page[noDomain][description]')}</span>
            <div className='d-flex align-items-center justify-content-start mt-5'>
              <button
                className='btn btn_custom btn_custom_pink me-3'
                onClick={() => router.push('/')}
              >
                {t('page[noDomain][action]')}
              </button>
            </div>
          </div>
        )
      }
      return (
        <React.Fragment>
          <motion.ul className={styles.domain__item_container}>
            {domains?.map((domain) => (
              <li
                key={domain.name}
                className={`
           ${styles.domain__item}`}
              >
                <div className={styles.domain__item_content}>
                  <span
                    className={`badge bg-pink color-${
                      domain?.status ? 'light' : 'warning'
                    } ${styles.domain__item_status}`}
                  >
                    {domain?.status
                      ? t('button[register]')
                      : t('button[transfer]')}
                  </span>
                  <span
                    className={` ${styles.domain__item_name} title-1 d-block`}
                  >
                    {domain.name}
                  </span>
                  <span
                    className={`small color-success ${styles.domain__item_linked}`}
                  >
                    {getUsedDomains().includes(domain.name) && 'Linked'}
                  </span>
                </div>
                <div className={styles.domain__item_action}>
                  <button
                    className={`btn btn_custom btn_custom_${
                      isProductSelected(domain?.name, product.pid)
                        ? 'success'
                        : 'secondary'
                    } mt-3 ${styles.domain__item_btn} `}
                    onClick={() =>
                      onAddProductToCart(domain, {
                        ...product,
                        freeDomains: getProductFreeDomains(
                          product?.gid,
                          product?.pid
                        )
                      })
                    }
                  >
                    {isProductSelected(domain?.name, product.pid)
                      ? t('common[added]')
                      : t('common[add]')}
                  </button>
                </div>
              </li>
            ))}
          </motion.ul>
        </React.Fragment>
      )
    } else if (currentStep === 'billing') {
      return (
        <React.Fragment>
          <motion.ul
            className={styles.product__config_billing}
            initial={{ translateX: 500, opacity: 1 }}
            animate={{ translateX: 0, opacity: 1 }}
            transition={{ duration: 0.5 }}
          >
            {billingCycles?.map((billing) => (
              <li
                key={billing.label}
                className={`d-flex justify-content-between align-items-center ${styles.product__config_domainitem}`}
                onClick={() =>
                  onUpdateProductBillingCycle(
                    domain?.name,
                    product.pid,
                    billing.label
                  )
                }
              >
                <div>
                  <span className='title-2 d-block color-pink'>
                    R{billing.value}
                  </span>
                  <span className='text-muted'>
                    {billing?.label?.charAt(0).toUpperCase() +
                      billing?.label?.slice(1)}
                  </span>
                </div>
                <button
                  className={`btn btn_custom btn_custom_${
                    billing.label === realyTimeProduct?.defaultBillingCycle
                      ? 'success'
                      : 'secondary'
                  }`}
                  disabled={billingCycles.length === 1}
                >
                  {billingCycles.length === 1
                    ? 'Default'
                    : billing.label === realyTimeProduct?.defaultBillingCycle
                    ? t('common[selected]')
                    : t('common[select]')}
                </button>
              </li>
            ))}
          </motion.ul>
          <div className='d-flex align-items-center justify-content-start mt-5'>
            <button
              className='btn btn_custom btn_custom_pink me-3'
              onClick={() => nextConfig(state.current)}
            >
              {t('button[continue]')}
            </button>
          </div>
        </React.Fragment>
      )
    } else if (currentStep === 'addons') {
      return (
        <div className={styles.product__config_addons}>
          {addons?.length === 0 && (
            <h4 className='text-center'>{t('domain[noAddon]')}</h4>
          )}
          {addons?.length > 0 && (
            <ul className='product__config_domains'>
              {addons?.map((addon) => (
                <li key={addon.id}>{addon.name}</li>
              ))}
            </ul>
          )}
        </div>
      )
    }
    return null
  }
  /**
   * Returns a UI displaying the current step's title and description
   * Also renders the content and products related to the current step
   */
  return (
    <div className={styles.product__config}>
      <div className='product__config_header'>
        <div className='text-center'>
          <h4 className='title-1 title-f-300'>
            {PRODUCT_CONFIG_DESCRIPTION()?.[state.current]?.title}
          </h4>
          <span className='small text-muted'>
            {PRODUCT_CONFIG_DESCRIPTION()?.[state.current]?.description}
          </span>
        </div>
      </div>
      {content(state.current)}
    </div>
  )
}

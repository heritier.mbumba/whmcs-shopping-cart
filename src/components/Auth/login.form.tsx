import React from 'react'
import { BeatLoader, MoonLoader } from 'react-spinners'
import { Colors } from '@constants/index'
import styles from './auth.module.scss'
import { FORMS } from './auth.tdo'
import useTranslate from '@hooks/useTranslate'

interface RegisterFormProps {
  loginHandler: (event: React.FormEvent<HTMLFormElement>) => void
  inputValuehandler: (
    event: React.FormEvent<HTMLInputElement>,
    target: string
  ) => void
  inputValue?: { email: string; password2: string }
  loading?: boolean
}

export default function LoginForm({
  loginHandler,
  inputValuehandler,
  inputValue,
  loading
}: RegisterFormProps) {
  const { t } = useTranslate() //translation helper function

  if (loading) {
    return (
      <div className='d-flex align-items-center justify-content-center'>
        <MoonLoader loading size={80} color={Colors.primary} />
      </div>
    )
  }

  return (
    <div className={`${styles.register__container}`}>
      {!loading && (
        <form onSubmit={loginHandler}>
          {FORMS?.login?.fields?.map((field) => (
            <div className='form-group mt-4' key={field?.name}>
              <label className='form-control-label'>{t(field?.label)}</label>
              <input
                className='form-control'
                type={field.type}
                // placeholder={field.placeholder}
                onChange={(event) => inputValuehandler(event, field.name)}
                value={inputValue?.[field?.name]}
                required
              />
            </div>
          ))}
          <button className='btn btn-pink mt-4' type='submit'>
            {t('common[submit]')}
          </button>
        </form>
      )}
    </div>
  )
}

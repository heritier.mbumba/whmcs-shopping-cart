import React from 'react'
import useTranslate from '@hooks/useTranslate'

const ENDPOINT = 'https://api.first.org/data/v1/countries'

interface ICountry {
  country: string
  region: string
  code: string
}

interface CountriesSelectProps {
  onSelect: (event: React.ChangeEvent<HTMLSelectElement>) => void
}

export default function CountriesSelectInput({
  onSelect
}: CountriesSelectProps) {
  const { t } = useTranslate() //translation helper function
  const [countries, setCountries] = React.useState<ICountry[]>([])

  React.useEffect(() => {
    ;(async () => {
      const request = await fetch(ENDPOINT)
      const response = await request.json()

      if (response?.status === 'OK') {
        const contriesCode = Object.keys(response?.data)
        const contriesList = contriesCode?.map((code) => ({
          code,
          ...response?.data?.[code]
        }))
        setCountries(contriesList)
      }
    })()
  }, [])

  return (
    <div className='form-group mt-4'>
      <select
        name='country'
        className='form-select form-select-lg'
        onChange={onSelect}
      >
        <option value=''>{t('form[register][country][select]')}</option>
        {countries?.map((country, i) => (
          <option value={country.code} key={country.code + String(i)}>
            {country.country}
          </option>
        ))}
      </select>
    </div>
  )
}

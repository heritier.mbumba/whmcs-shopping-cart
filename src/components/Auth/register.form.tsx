import { IAddNewClient } from '@lib/types/auth'
import React from 'react'
import styles from './auth.module.scss'
import { FORMS, IFormField, IStepFormField, isValidInputs } from './auth.tdo'
import { motion } from 'framer-motion'
import CountriesSelectInput from './Countries'
import { MoonLoader } from 'react-spinners'
import { Colors } from '@constants/index'
import useTranslate from '@hooks/useTranslate'
import { REGEX_EMAIL, REGEX_POSTAL, REGEX_PHONE_NUMBER } from '@utils/default'

interface RegisterFormProps {
  registerHandler: (event: React.FormEvent<HTMLFormElement>) => void
  inputValuehandler: (
    event:
      | React.FormEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLSelectElement>,
    target: string
  ) => void
  inputError?: Record<string, any>
  inputValue?: IAddNewClient
  loading?: boolean
  onValueHandler: (values: Record<string, any>) => void
  titleAlign?: 'left' | 'center' | 'right'
}
export default function RegisterForm({
  registerHandler,
  inputError,
  onValueHandler,
  loading,
  titleAlign
}: RegisterFormProps) {
  const [currentStep, setCurrentStep] = React.useState('personalInfo')
  const [register, setRegister] = React.useState<Record<string, any>>()
  const [fieldsLength, setFieldsLength] = React.useState<number>(null)
  const { t } = useTranslate() //translation helper function

  const [errorInput, setErrorInput] = React.useState<Record<string, any>>() //{email: 'invalid email'} errorInput?.email

  const formFields = Object.keys(FORMS.register.steps)?.map((key) => ({
    key,
    values: { ...FORMS.register.steps[key] }
  }))

  React.useEffect(() => {
    setFieldsLength(
      formFields.filter((fields) => fields.key === currentStep)[0].values.fields
        .length
    )
    onValueHandler(register?.[currentStep])
  }, [register, currentStep])

  const onInputValueChange = (
    event:
      | React.FormEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLSelectElement>,
    currentStep: string,
    key: string
  ) => {
    const { value } = event?.currentTarget
    const errorCp = { ...errorInput }
    delete errorCp?.[key]
    setErrorInput(errorCp)
    setRegister((prev) => ({
      ...prev,
      [currentStep]: { ...prev?.[currentStep], [key]: value }
    }))
  }

  /**
   * 
   */
  const goNextStep = React.useCallback(
    (currentStep: string) => {
      const steps = formFields?.map((k) => k?.key)
      const index = steps.indexOf(currentStep)
      if (index === steps.length - 1) {
        return
      }
      switch (currentStep) {
        case 'auth': {
          const { email } = register?.[currentStep]
          if (email && !REGEX_EMAIL().test(email)) {
            return setErrorInput((prevState) => ({
              ...prevState,
              email: 'Invalid Email Address'
            }))
          }
        }

        case 'contacts': {
          const { phonenumber } = register?.[currentStep]
          if (phonenumber && !REGEX_PHONE_NUMBER().test(phonenumber)) {
            return setErrorInput((prevState) => ({
              ...prevState,
              phonenumber: 'Invalid Phone Number'
            }))
          }
        }

        case 'address': {
          const { postcode } = register?.[currentStep]
          if (postcode && !REGEX_POSTAL().test(postcode)) {
            return setErrorInput((prevState) => ({
              ...prevState,
              postcode: 'Invalid Postal Code'
            }))
          }
        }
      }

      //send values to parent
      onValueHandler(register?.[currentStep])
      setCurrentStep(steps[index + 1])
    },
    [currentStep, register]
  )
  const goPrevStep = React.useCallback(
    (currentStep: string) => {
      const steps = formFields?.map((k) => k?.key)
      const index = steps.indexOf(currentStep)
      if (index === 0) {
        return
      }
      setCurrentStep(steps[index - 1])
    },
    [currentStep]
  )

  if (loading) {
    return (
      <div className='d-flex align-items-center justify-content-center'>
        <MoonLoader loading size={80} color={Colors.primary} />
      </div>
    )
  }

  return (
    <div className={`${styles.register__container}`}>
      <form onSubmit={registerHandler}>
        {formFields?.map((step) => (
          <div className='register__step mb-4' key={step?.key}>
            {step?.key === currentStep && (
              <motion.div
                className='register__step_container'
                initial={{ opacity: 0, translateX: 100 }}
                animate={{ translateX: 0, opacity: 1 }}
              >
                <h4
                  className={`text-${
                    titleAlign ? titleAlign : 'center'
                  } heading_small heading_upper`}
                >
                  {t(step?.values?.title)}
                </h4>
                {inputError && Object.values(inputError).length > 0 && (
                  <span className='text-danger'>
                    {Object.values(inputError).length}{' '}
                    {t('common[input][invalid]')}
                  </span>
                )}
                {step?.values?.fields?.map((field: IFormField) =>
                  field?.name === 'country' ? (
                    <CountriesSelectInput
                      {...{
                        onSelect: (event) =>
                          onInputValueChange(event, currentStep, field.name)
                      }}
                      key={field?.name}
                    />
                  ) : (
                    <div className='form-group mt-4' key={field?.name}>
                      <label className='form-control-label'>
                        {t(field?.label)}
                      </label>
                      <input
                        className={`form-control`}
                        type={field.type}
                        // placeholder={field.placeholder}
                        onChange={(event) =>
                          onInputValueChange(event, currentStep, field.name)
                        }
                        required
                        value={register?.[currentStep]?.[field.name] || ''}
                      />
                      {errorInput && (
                        <span className='text-danger'>
                          {errorInput?.[field?.name]}
                        </span>
                      )}
                      <div
                        className='invalid-feedback'
                        style={{
                          display: inputError?.[field?.name] && 'block'
                        }}
                      >
                        <span>{inputError?.[field?.name]}</span>
                      </div>
                    </div>
                  )
                )}
              </motion.div>
            )}
          </div>
        ))}
        {currentStep === 'submit' && (
          <div className={`text-${titleAlign ? titleAlign : 'center'}`}>
            <button
              className='btn btn-secondary me-3'
              type='button'
              onClick={() => goPrevStep(currentStep)}
            >
              {t('common[no]')}
            </button>
            <button className='btn btn-pink' type='submit'>
              {t('common[yes]')}
            </button>
          </div>
        )}
      </form>
      {currentStep !== 'submit' && (
        <div className='form-group mb-4'>
          {currentStep !== 'personalInfo' && (
            <button
              className='btn btn-secondary me-3'
              onClick={() => goPrevStep(currentStep)}
              type='button'
            >
              {t('button[previous]')}
            </button>
          )}
          <button
            className='btn btn-pink'
            onClick={() => goNextStep(currentStep)}
            disabled={!isValidInputs(register, currentStep, fieldsLength)}
          >
            {t('button[next]')}
          </button>
        </div>
      )}
    </div>
  )
}

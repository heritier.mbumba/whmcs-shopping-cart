import React from 'react'

export interface IInputOption {
  value: string
  label?: string
}

export interface IFormField {
  label?: string
  type: string
  name: string
  placeholder?: string
  value?: string
  defaultValue?: string
  options?: IInputOption
}
export interface IStepFormField {
  label?: string
  values?: IFormField[]
  key?: string
}
interface IForm {
  title?: string
  description?: string
  steps?: {
    [key: string]: {
      title: string
      fields: IFormField[]
    }
  }
  fields?: IFormField[]
}

type FormsType = {
  login?: IForm
  register?: IForm
}

export const FORMS: FormsType = {
  login: {
    title: 'form[login][title]',
    description: 'form[login][description]',
    fields: [
      {
        label: 'form[login][email]',
        type: 'email',
        name: 'email',
        placeholder: 'john@email.com'
      },
      {
        label: 'form[login][password]',
        type: 'password',
        placeholder: '**************',
        name: 'password2'
      }
    ]
  },
  register: {
    title: 'form[register][title]',
    description: 'form[register][description]',
    steps: {
      personalInfo: {
        title: 'auth[register][tellusaboutyou]',
        fields: [
          {
            label: 'form[register][firstname]',
            type: 'text',
            name: 'firstname',
            placeholder: 'Charles'
          },
          {
            label: 'form[register][lastname]',
            type: 'text',
            name: 'lastname',
            placeholder: 'Dean'
          }
        ]
      },
      contacts: {
        title: 'auth[register][howcontactyou]',
        fields: [
          {
            label: 'form[register][phonenumber]',
            type: 'tel',
            name: 'phonenumber',
            placeholder: '+27'
          },
          {
            label: 'form[register][address1]',
            type: 'text',
            name: 'address1',
            placeholder: '265, Stepney Cresent rd'
          },
          {
            label: 'form[register][city]',
            type: 'text',
            name: 'city',
            placeholder: 'Johanesburg'
          }
        ]
      },
      address: {
        title: 'auth[register][whereyoulive]',
        fields: [
          {
            label: 'form[register][state]',
            type: 'select',
            name: 'state',
            placeholder: 'Guateng'
          },
          {
            label: 'form[register][postcode]',
            type: 'text',
            name: 'postcode',
            placeholder: '74545'
          },
          {
            label: 'form[register][country]',
            type: 'select',
            name: 'country',
            placeholder: 'South africa'
          }
        ]
      },
      auth: {
        title: 'auth[register][authcredentials]',
        fields: [
          {
            label: 'form[register][email]',
            type: 'email',
            name: 'email',
            placeholder: 'cdean@email.com'
          },
          {
            label: 'form[register][password2]',
            type: 'password',
            name: 'password2',
            placeholder: '**************'
          }
        ]
      },
      submit: {
        title: 'auth[register][readysubmit]',
        fields: []
      }
    }
  }
}

export const isValidInputs = (
  state: Record<string, IFormField>,
  currentStep: string,
  fieldsLength: number
) => {
  const validated: boolean[] = []

  if (!state) {
    return false
  } else if (!state?.[currentStep]) {
    return false
  } else {
    const currentStepFieldLength = Object?.keys(state?.[currentStep]).length

    if (currentStepFieldLength != fieldsLength) {
      return false
    } else {
      for (const key in state?.[currentStep]) {
        if (!state?.[currentStep]?.[key]) {
          validated.push(false)
        } else if (state?.[currentStep]?.[key] === '') {
          validated.push(false)
        } else {
          validated.push(true)
        }
      }
    }
  }
  if (validated.every((element) => element)) return true

  return false
}

import styles from './box.module.scss'
interface BoxItemProps {
  children: JSX.Element | JSX.Element[]
  cssClasses?: string
  onClick?: () => void
  boxShowOff?: boolean
}

/**
 * Component for wrapping elements
 * @param onClick event handlers
 * @param cssClasses css classes inherited from child component
 * @returns component that wrap the children
 */
export default function BoxItem({
  children,
  cssClasses,
  onClick,
  boxShowOff
}: BoxItemProps) {
  if (boxShowOff) {
    return (
      <div className={`${cssClasses} ${styles.boxitemoff}`} onClick={onClick}>
        {children}
      </div>
    )
  }
  return (
    <div className={`${cssClasses} ${styles.boxitem}`} onClick={onClick}>
      {children}
    </div>
  )
}

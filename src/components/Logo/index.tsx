import { useRouter } from "next/dist/client/router";
import * as React from "react";
import styles from "./logo.module.scss";

interface LogoProps {
  link?: string
}

/**
 * component that displays the company logo
 * @param link the url where a user should be redirected to if they click on the icon
 * @returns a JSX element that contains the company logo
 */

export default function Logo({ link }: LogoProps) {
  const router = useRouter()

  if (link) {
    return (
      <img
        src="/images/1-grid-logo.png"
        alt='1-grid logo'
        width={'100%'}
        height={'100%'}
        onClick={() => router.push(link)}
        className={styles.logo}
      />
    )
  }
  return (
    <img
      src="/images/1-grid-logo.png"
      alt='1-grid logo'
      width='100%'
      height='100%'
      className={styles.logo}
    />
  )
}

import LoginModal from '@components/Modal/AuthModal'
import useAnalytics from '@hooks/useAnalytics'
import useAuth from '@hooks/useAuth'
import useTranslate from '@hooks/useTranslate'
import { PageProps } from '@lib/types/app'
import { useRouter } from 'next/dist/client/router'
import React from 'react'

/**
 * wrapper component for implementing authentication on a component
 * @param Component
 * @returns JSX wrapper element for authentication
 */

export default function withAuth<T extends PageProps>(Component: React.FC<T>) {
  return (props: T) => {

    /**
     * 
     */
    const [showLogin, setShowLogin] = React.useState(false)

    /**
     * 
     */
    const { checkAuth, isAuthenticated } = useAuth()

    const { t } = useTranslate()

    const router = useRouter()

    const LogEvent = useAnalytics()

    //here we check if a user's session has expired
    //if a user's session has expired, we show them the login modal
    React.useEffect(() => {
      ;(async () => {
        if ((await checkAuth()).alreadyExpired) {
          setShowLogin(true)
        }
        
      })()
    }, [])


    React.useEffect(()=>{
      if(isAuthenticated){
        if(router.pathname === '/checkout'){
          LogEvent().checkout(5, 'Authenticated')
        }
      }
    }, [isAuthenticated])


    const loginBtnHandler = React.useCallback(()=>{
      if(!showLogin && !isAuthenticated) {
        return <div className="container mt-5">
        <div className="row">
          <div className="col-md-12 text-center">
          <button className="btn btn-primary" onClick={() => setShowLogin(true)}>{t('page[checkout][proceed]')}</button>
          </div>
        </div>
      </div>
      }
    }, [showLogin, isAuthenticated])

    return (
      <React.Fragment>
        <Component {...props} />
        {showLogin && <LoginModal onCloseHandler={() => setShowLogin(false)} />}
        {loginBtnHandler()}
      </React.Fragment>
    )
  }
}

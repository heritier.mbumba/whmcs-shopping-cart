import BoxItem from '@components/Box/BoxItem';
import React from 'react'

/**
 * 
 * @param Component React functional component
 * @returns React component
 */
function withErrorBundary<T>(Component: React.FC<T>) {
  return class extends React.Component<T> {

    /**
     * 
     */
    state = {
      errorOccured: false,
      error: null
    };
  
    /**
     * 
     * @param error 
     * @returns 
     */
    static getDerivedStateFromError(error: any) {
      return {
        errorOccured: true,
      };
    }
  
    /**
     * 
     * @param error 
     * @param errorInfo 
     */
    componentDidCatch(error: any, errorInfo: any) {
      /**
       * Log error to sentry
       */
      this.setState(prev=>({...prev, error: {
        title: 'Something went',
        message: error?.message || 'Ops! Please reload the page if the issue persite'
      }}))
    }

    render(){
      if(this.state.errorOccured){
        return (
          <BoxItem cssClasses="mt-4">
            <h4 className="title-1 title-f-300">{this.state.error?.title}</h4>
            <span className="text-muted">{this.state.error?.message}</span>
          </BoxItem>
        );
      }
      return <Component {...this.props}/>
    }
  }
}

export default withErrorBundary
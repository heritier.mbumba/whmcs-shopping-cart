import { useApplication } from '@app/context/App'
import { useShop } from '@app/context/Shop'
import { initializedSteps } from '@app/redux/dispatches/appDispatcher'
import useTranslate from '@hooks/useTranslate'
import { PageProps } from '@lib/types/app'
import Head from 'next/head'
import * as React from 'react'
import { useDispatch } from 'react-redux'
import Header from '../Header/header'
import styles from './page.module.scss'

interface IPage extends PageProps {
  renderHeader?: () => JSX.Element
  renderFooter?: () => JSX.Element
  layout?: 'default' | 'left-steps' | 'right-steps'
}

/**
 * Higher Order Component for wrapping children elements
 * @param title the title to use in the page title's meta data
 * @param description  the description to use in the page description's meta data
 * @param pathname the name to the current path
 * @param renderHeader boolean use to conditionally render the header component
 * @param renderFooter boolean use to conditionally render the footer component
 * @param withSteps boolean used to conditionally render the steps component
 * @returns a JSX wrapper that manages the layout of the page. Conditionally handles rendering extra components
 */

export default function Page({
  children,
  title,
  description,
  pathname,
  renderHeader,
  renderFooter,
  withSteps
}: IPage) {
  const { t } = useTranslate() //translation helper function

  //extracts the items in the redux cart state
  const { items } = useShop()

  /**
   * Get application config
   */
  const { config } = useApplication()

  const dispatch = useDispatch()

  //if there are no items in the cart, reinitialize the flows steps
  React.useEffect(() => {
    if (items.length === 0) {
      dispatch(initializedSteps(config.flows))
    }
  }, [items])

  return (
    <>
      <Head>
        <title>{title || t('common[pageTitle]')} | 1-grid</title>
        <meta name='description' content={description} />
      </Head>
      <main className={styles.main}>
        <Header
          activeLink={pathname}
          type='light'
          showTopHeader
          contact={config?.contacts?.general}
        />
        {renderHeader && renderHeader()}

        {children}

        {renderFooter && renderFooter()}
      </main>
    </>
  )
}

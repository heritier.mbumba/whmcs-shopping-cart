import useTranslate from '@hooks/useTranslate'
import { PageProps } from '@lib/types/app'
import { NextPage } from 'next'
import Head from 'next/head'
import * as React from 'react'
import Header from '../Header/header'

interface withPageMetaProps {
  title: string
  description: string
  renderHeader?: () => React.FC | JSX.Element
}

/**
 * component that displays the component passed to it with additional meta data
 * @param Component component to be used as a child component
 * @returns a JSX element that nest the child component and wraps it with meta data
 */

const withPageMeta = (Component: NextPage<PageProps>) => {
  const { t } = useTranslate() //translation helper function

  return (props: any) => {
    return (
      <React.Fragment>
        <Head>
          <meta name='description' content={props.description} />
          <title>
            {props.title ? props.title : t('common[pageTitle]')} |{' '}
            {t('company[name]')}
          </title>
        </Head>
        <main>
          <Header
            activeLink={props?.pathname}
            type='light'
            showTopHeader
            contact={props?.contacts.general}
          />
          <Component {...props} />
        </main>
      </React.Fragment>
    )
  }
}

export default withPageMeta

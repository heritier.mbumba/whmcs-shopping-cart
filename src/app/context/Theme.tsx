import * as React from "react";
import { Theme } from "@lib/types/app";

type ThemeContextType = {
  theme: Theme;
  setTheme: React.Dispatch<React.SetStateAction<Theme>>;
};

export const defaultTheme = {
  palette: {
    primary: {
      main: "#20bfdd",
    },
    common: {
      light: "#fff",
      black: "#000",
      grey: "#ccc",
    },
    secondary: {
      main: "#56565b",
    },
  },
  fontSize: 16,
  logo: "/images/placeholder-logo.png",
  name: 'default'
}

export const ThemeContext = React.createContext<Partial<ThemeContextType>>({
  setTheme: () => {},
});

export const useTheme = () => React.useContext(ThemeContext);

export const ThemeProvider: React.FC = ({ children }) => {
  const [theme, setTheme] = React.useState<Theme>(defaultTheme)

  return (
    <ThemeContext.Provider value={{ setTheme, theme }}>
      {children}
    </ThemeContext.Provider>
  );
};

import { IApplicationConfig } from '@lib/types/api'
import React from 'react'

export interface IApplicationProvider extends IApplicationConfig {}

export interface IAppContext {
  config?: IApplicationProvider
  setConfig?: (config: IApplicationProvider) => void
}

/**
 * Initializes a null Context value for the application
 */
export const AppContext = React.createContext<IAppContext>(null)

/**
 * @returns A helper function that uses the context
 */
export const useApplication = () => React.useContext<IAppContext>(AppContext)

/**
 * Function that provides a context provider for the app
 */
export const ApplicationProvider: React.FC = ({ children }) => {
  /**
   * Provides the app with hooks to manage the config of the app
   */
  const [config, setConfig] = React.useState<IApplicationProvider>()
  return (
    <AppContext.Provider value={{ config, setConfig }}>
      {children}
    </AppContext.Provider>
  )
}

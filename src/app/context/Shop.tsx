import { IRootState } from "@app/redux";
import { selectedCheckoutOptionDispatcher } from "@app/redux/dispatches/appDispatcher";
import {
  addDomainDispatcher,
  addDomainEppCodeDispatcher,

  addProductToDomainDispatcher,




  applyPromotionCode, getProRataAmount, removeDomainEppCodeDispatcher,



  removeItemInCart, removeProductToDomain,




  removePromoCodeDispatcher, updateBillingCycleDispatcher
} from "@app/redux/dispatches/shopDispatcher";
import { IAppState } from "@app/redux/reducers/app.reducer";
import { ICartState, IDiscount, IOrderSummary } from "@app/redux/reducers/cart.reducer";
import { ICheckoutFlow } from "@lib/types/api";
import { IDomainTld, IDomainWithPrice } from "@lib/types/domain";
import { IProduct, IProductAddons } from "@lib/types/product";
import { ICartItem } from "@lib/types/shop";
import { Moment } from "moment";
import React from "react";
import { useDispatch, useSelector } from "react-redux";

interface IShopContext {
  items?: ICartItem[];
  currentCheckoutFlow?: string;
  flows?: ICheckoutFlow,
  orderSummary?: IOrderSummary;
  discount?: IDiscount;
  checkoutOptions?: string[];
  promocode?: string;
  googleClick?: {gclid: string; expires_at: Moment}
  onAddDomainToCart?: (domains: IDomainWithPrice[]) => void;
  onAddProductToCart?: (domain: IDomainWithPrice, product: IProduct) => void;
  onAddProductAddonToCart?: (addon: IProductAddons) => void;
  onAddDomainEppCode?: (domain: IDomainWithPrice, code: string) => void;
  onRemoveDomainEppCode?: (domain: IDomainWithPrice) => void;
  onRemoveItemToCart?: (domain: IDomainWithPrice) => void;
  onRemoveItemProductToCart?: (
    domain: IDomainWithPrice,
    product: IProduct
  ) => void;
  onRemoveAddonToProduct?: (
    domain: IDomainWithPrice,
    pid: number,
    addonid: number
  ) => void;
  onUpdateProductBillingCycle?: (
    domain: string,
    pid: number,
    newBillingCycle: string
  ) => void;
  onGetProRataAmount?: (amount: number, billingCycle?: string) => number;
  onApplyPromotionCode?: (payload: {
    type: string;
    concerned: string;
    value: number;
    code: string;
  }) => void;
  onRemovePromoCode?: () => void;
  onSelectedCheckoutOption?: (flow: string) => void;
  onFlowInit?: () => void
}

export const ShopContext = React.createContext<IShopContext>(null);

export const useShop = () => React.useContext(ShopContext);

export default function ShopProvider({ children }) {
  const dispatch = useDispatch();
  const {items, orderSummary, discount, promocode} = useSelector<IRootState, ICartState>(state=>state?.cart)
  const {currentCheckoutFlow, flows, checkoutOptions, googleClick} = useSelector<IRootState, IAppState>(state=>state?.app)
  

  const providerValue = React.useMemo(
    (): IShopContext => ({
      
      /**
       * Shop items
       */
      items,

      /**
       * Current select checkout flow
       */
      currentCheckoutFlow,

      /**
       * Available flows
       */
      flows,


      /**
       * Discount object, container the promo detail
       */
      discount,

      orderSummary,


      /**
       * current flow checkout steps
       */
      checkoutOptions,

      /**
       * Promo code
       */
      promocode,

      /**
       * Google click identifier
       */
      googleClick,

      /**
       * 
       * @param domains 
       * @returns 
       */
       onAddDomainToCart: (domains) => dispatch(addDomainDispatcher(domains)),
      
      /**
       * 
       * @param domain 
       * @param product 
       * @returns 
       */
      onAddProductToCart: (domain, product) => dispatch(addProductToDomainDispatcher(domain, product)),

      /**
       * 
       * @param addon 
       */
      onAddProductAddonToCart: (addon) => {},

      /**
       * 
       * @param domain 
       * @param code 
       * @returns 
       */
      onAddDomainEppCode: (domain, code) => dispatch(addDomainEppCodeDispatcher(domain, code)),

      /**
       * 
       * @param domain 
       * @returns 
       */
      onRemoveDomainEppCode: (domain) =>
        dispatch(removeDomainEppCodeDispatcher(domain)),

      /**
       * 
       * @param domain 
       * @returns 
       */
      onRemoveItemToCart: (domain) => dispatch(removeItemInCart(domain)),

      /**
       * 
       * @param domain 
       * @param product 
       * @returns void
       */
      onRemoveItemProductToCart: (domain, product) =>
        dispatch(removeProductToDomain(domain, product)),

      /**
       * 
       * @param domain 
       * @param pid 
       * @param addonid 
       */
      onRemoveAddonToProduct: (
        domain,
        pid,
        addonid
      ) => {},

      /**
       * 
       * @param domain 
       * @param pid 
       * @param newBillingCycle 
       */
      onUpdateProductBillingCycle: (
        domain,
        pid,
        newBillingCycle
      ) => dispatch(updateBillingCycleDispatcher(domain, pid, newBillingCycle)),

      /**
       * 
       * @param amount 
       * @param billingCycle 
       */
      onGetProRataAmount: (amount, billingCycle) => getProRataAmount(amount, billingCycle),

      /**
       * Apply promo code where the promo is being set
       * @param payload 
       */
      onApplyPromotionCode: (payload) => dispatch(applyPromotionCode(payload)),

      /**
       * Change the flow
       * @param flow 
       * @returns 
       */
      onSelectedCheckoutOption: (flow) => dispatch(selectedCheckoutOptionDispatcher(flow)),

      onRemovePromoCode: () => dispatch(removePromoCodeDispatcher()),

      onFlowInit: () => dispatch({type: 'INITIAL_FLOW'})
    }),
    [currentCheckoutFlow, items, discount]
  );

  return (
    <ShopContext.Provider value={providerValue}>
      {children}
    </ShopContext.Provider>
  );
}

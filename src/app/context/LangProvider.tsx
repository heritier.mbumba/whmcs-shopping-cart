import { ILanguage } from "@lib/types/api";
import * as React from "react";

interface ILanguageContex {
  language?: ILanguage
  setLanguage?: (language: ILanguage) => void
  setLocale?: (locale: string) => void
}

/**
 * Initializes a null Context value for the application
 */
export const LanguageContext = React.createContext<ILanguageContex>(null)

/**
 * @returns A helper function that uses the context
 */
export const useLanguage = () =>
  React.useContext<ILanguageContex>(LanguageContext)

/**
 * Function that provides a context provider for the app that manages the language in the app
 */
export const LanguageProvider: React.FC = ({ children }) => {
  /**
   * Provides the app with hooks to manage the langauge of the app
   */
  const [language, setLanguage] = React.useState<ILanguage>()

  const setLocale = (locale: string) => {}

  return (
    <LanguageContext.Provider value={{ language, setLanguage, setLocale }}>
      {children}
    </LanguageContext.Provider>
  )
}

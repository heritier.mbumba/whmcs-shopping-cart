export default {
  upperFirstLetter: (str: string) => {
    const [first, ...others] = str.split('')
    return [first.toLocaleUpperCase(), ...others].join('')
  }
}
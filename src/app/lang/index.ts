export const LANG = {
  noLang: 'Invalid Language',
  en: {
    'company[name]': '1-grid',
    'page[home][title]': 'Welcome',
    'page[cart][title]': 'How would you like to get your business online?',
    'page[cart][configdomains]': 'Configure your domains',
    'page[cart][overview]': 'Selected domains',

    'page[noDomain][title]': 'No domain found',
    'page[noDomain][description]': 'There is no domain in your cart',
    'page[noDomain][action]': 'register domain',

    'page[billing][description]': 'Invalid code',
    'page[billing][cycle]': 'Please choose your billing cycle',

    'page[summary][promoCode]': 'Promo code is required',
    'page[summary][invalidCode]': 'Invalid Code',
    'page[summary][total]': 'Total',
    'page[summary][subtotal]': 'Subtotal',
    'page[summary][vatText]': 'VAT',
    'page[summary][vatAmount]': '15%',
    'page[summary][noProducts][title]': 'No products',
    'page[summary][noProducts][description]': 'There are no items in the cart',
    'page[summary][noProducts][action]': 'Register a domain',
    'page[summary][items][title]': 'Items',
    'page[summary][items][total]': 'Total summary',
    'page[summary][apply]': 'Apply',
    'page[summary][loadError]':
      'We could not load the payment gateways. Please try to pay via your client zone',

    'page[checkout][selectMethod]': 'Select your payment method',
    'page[checkout][sorry]': 'Sorry!',
    'page[checkout][processing]': 'Processing...',
    'page[checkout][proceed]': 'Proceed',

    'page[success][title]': 'Payment has been completed!',
    'page[success][description]':
      'Thank for purchasing with 1-Grid. Please wait while your service is being activated',
    'page[cancellation][title]': 'Payment Cancelled!',
    'page[cancellation][description]':
      'Please note that your order will not be activated until payment has been made. Log in to your customer zone to make a payment now',
    'promo[code][added]': 'Applied!',
    'domain[noresults]': 'No result was returned, Please try again',
    'domain[result][available]': 'Congratulations!',
    'domain[result][unavailable]': 'Oops!',
    'domain[result][msgAvailable]': 'Is available for registration',
    'domain[result][msgUnvailable]': 'Is unavailable for registration',
    'domain[noAddon]': 'No addons for this products',
    'domain[configured]': 'Configured domains',
    'domain[result][transfer]': 'Transfer cost',
    'domain[alternative][heading]': 'Alternatives domains',
    'domain[alternative][other]': 'Other alternatives',
    'domain[alternative][otherFound]': 'alternatives were found',
    'domain[errorName][message]': 'Please enter a name for your domain',
    'domain[errorName][title]': 'Invalid Domain Name',
    'domain[invalidName][description]': 'Domain name must not contain white space, symbols and should be at least 4 characters long',
    'domain[selectedAmount]': 'Domain(s) selected',
    'domain[invalidextension]': 'Invalid domain extension',
    'domain[invalidextension][message]': ' is not a supported domain extension',

    'form[choosewebaddress]': 'Choose a website address',
    'form[choosewebaddressdesc]': 'Search for a website address',
    'form[choosewebaddressfield]': 'Type your domain',
    'form[choosewebaddresssearch]': 'Search',
    'form[choosewebaddresscustomize]': 'Select extension',
    'form[domainregisterandtransfer]': 'Register or Transfer domain',

    'form[login][title]': 'Login',
    'form[login][description]': 'Enter your email and password to login',
    'form[login][email]': 'Email address',
    'form[login][password]': 'Password',

    'form[transfer][title]': 'Transfer',
    'form[register][title]': 'Register',
    'form[register][description]':
      'Fill the form field to register your account',
    'form[register][firstname]': 'First name',
    'form[register][lastname]': 'Last name',
    'form[register][phonenumber]': 'Phone number',
    'form[register][address1]': 'Home/Business - Address',
    'form[register][city]': 'City',
    'form[register][state]': 'Province',
    'form[register][postcode]': 'Postal code',
    'form[register][country]': 'Country',
    'form[register][country][select]': 'Select your country',
    'form[register][email]': 'Email address',
    'form[register][password2]': 'Password',

    'header[logotext]': 'WHMCS Shopping cart',

    'auth[register][title]': 'Register account',
    'auth[register][description]': 'Enter your email and password',
    'auth[register][loginLink]': 'Already have an account?',
    'auth[register][tellusaboutyou]': 'Hi, Tell us about you',
    'auth[register][howcontactyou]': 'How can we contact you',
    'auth[register][whereyoulive]': 'Where do you live',
    'auth[register][authcredentials]': 'Authorisation Credentials',
    'auth[register][readysubmit]': 'Ready to submit?',

    'auth[login][title]': 'Sign in',
    'auth[login][description]': 'Enter your email and password',
    'auth[login][credentialsMissing]': 'Email and password is required',
    'auth[login][success]': 'Success!',
    'auth[login][something]': 'Something went wrong',
    'auth[login][haveAccount]': 'Already have an account?',
    'auth[login][noAccount]': `Don't have an account?`,

    'button[continue]': 'Continue',
    'button[yesplease]': 'Yes, Please',
    'button[no]': 'No',
    'button[expresscheckout]': 'Express checkout',
    'button[register]': 'Register',
    'button[transfer]': 'Transfer',
    'button[checkout]': 'Checkout',
    'button[next]': 'Next',
    'button[previous]': 'Previous',
    'button[ordersummary]': 'Order summary',
    'button[summary]': 'Summary',
    'button[payNow]': 'Pay now',
    'component[timepill][year]': 'per Year',
    'component[timepill][month]': 'per Month',

    'timeInterval[month]': 'Monthly',

    'modal[domainTLD][title]': 'Choose your domain tld',
    'modal[domainTLD][description]':
      'What would you like your domain to look like?',
    'modal[flowSelect][title]': 'Select your option',
    'modal[flowSelect][description]':
      'How would you like to create your website',
    'modal[expresscheckout][title]': 'Express checkout',
    'modal[expresscheckout][description]':
      'This will be only domain order, without any products or addons',
    'modal[expresscheckout][confirmationTitle]': 'Are you sure?',
    'modal[expresscheckout][confirmationMsg]': `Please note; This action will take you straight to the payment
    gateways`,
    'modal[loading][checking]': 'Checking availability',
    'modal[payment][title]': 'Invoice payment',
    'modal[payment][description]':
      'You can also pay your invoices via client zone',
    'modal[invoices][unpaid]': 'Your unpaid invoices',
    'modal[invoices][select]': 'Select the invoice you would like to pay',

    'checkoutOptions[wb][label]': 'Build your own website',
    'checkoutOptions[wb][name]': 'Website Builder',
    'checkoutOptions[hosting][label]': 'Host your website',
    'checkoutOptions[hosting][name]': 'Web Hosting',
    'checkoutOptions[email][label]': 'Get a business email',
    'checkoutOptions[email][name]': 'Have a professional email',
    'checkoutOptions[domain][label]': 'Purchase this domain only',
    'checkoutOptions[domain][name]': 'Domain',

    'productConfigDesc[domains][title]': 'Link domain',
    'productConfigDesc[domains][description]':
      'Which domain would you like to link with your product?',
    'productConfigDesc[billing][title]': 'Billing options',
    'productConfigDesc[billing][description]':
      'Select your preferred billing cycle',
    'productConfigDesc[addons][title]': 'Product addons',
    'productConfigDesc[addons][description]':
      'Customize your product with these addons?',

    'flowTitle[sitebuilder][package]': 'Website builder',
    'flowTitle[sitebuilder][description]': 'Select your suitable package size',
    'flowTitle[sitebuilder][email]': 'Select your suitable email package',
    'flowTitle[email][package]': 'Business email ',
    'flowTitle[hosting][package]': 'Please select your package',
    'flowTitle[hosting][email]': 'Add a business email to your domain',
    'flowTitle[hosting][security]': 'How would like to secure your domain?',
    'flowTitle[hosting][essentials]': 'Essential Products',
    'flowTitle[hsoting][essentail]': 'Failed to load addons',

    'common[pleasewait]': 'Please wait...',
    'common[add]': 'Add',
    'common[addProductToCart]': 'Add to cart',
    'common[added]': 'Added',
    'common[buy]': 'Buy',
    'common[buynow]': 'Buy now',
    'common[select]': 'Select',
    'common[selected]': 'Selected',
    'common[totalduetoday]': 'Total due today',
    'common[due]': 'Due',
    'common[prorata]': 'Pro rata amount',
    'common[submit]': 'Submit',
    'common[input][invalid]': 'Invalid input(s)',
    'common[no]': 'No',
    'common[yes]': 'Yes',
    'common[tryAgain]': 'Try Again',
    'common[close]': 'Close',
    'common[needTransfer]': 'Need to transfer?',
    'common[eppCode]':
      'You will need to provide us with the epp code from your registration',
    'common[eppCode][add]': 'Add domain epp code',
    'common[eppCode][addAlt]': 'Add epp code for transfer',
    'common[eppCode][description]': 'Please add your eppcode for',
    'common[choosepackage]': 'Choose package',
    'common[pageTitle]':
      'Domain Name Registration, Web Hosting, Website Design, Company Registration',
    'common[addProduct]': 'Add product',
    'common[free]': 'Free',
    'common[registration]': 'Registration',
    'common[customizeDomains]': 'Customize your domain with these products',
    'common[products]': 'Product(s)',
    'common[code]': 'Code',
    'common[essential][title]': 'Essential Products',
    'common[essential][description]':
      '1-grid recommends these add-on products ',
    'common[off]': 'off',
    'common[getDeal]': 'Get the deal',
    'common[smlDomain]': 'domain',
    'common[combo][or]': 'or',
    'common[combo][pt1]': 'By one website builder and get a free',
    'common[combo][pt2]': 'domain registration',
    'common[accessDenied]': 'Access denied',
    'common[loading]': 'Loading...',

    'component[box][from]': 'From',
    'component[box][popular]': 'Most Popular',

    'user[greeting]': 'Hello',
    'user[logout]': 'Logout',
    'user[invoice][title]': 'Invoices',
    'user[zone]': 'Client Zone',
    'user[invoice][have]': 'You have',
    'user[invoice][unpaid]': 'unpaid Invoices',
    'user[invoice][pay]': 'Pay Invoices',
    'user[notLoggedIn]': 'You are not logged in'
  },
  fr: {
    'home[title]': 'Bienvenue',
    'form[choosewebaddress]': 'Choisissez une adresse de site Web',
    'form[choosewebaddressdesc]': 'Rechercher une adresse de site Web',
    'form[choosewebaddressfield]': 'Tapez votre domaine',
    'form[choosewebaddresssearch]': 'Rechercher',
    'form[domainregisterandtransfer]': 'Enregistrez ou Transferez domaine',
    'header[logotext]': 'WHMCS basket'
  },
  pt: {
    'home[title]': 'Receber',
    'form[choosewebaddress]': 'Escolha um endereço de site',
    'form[choosewebaddressdesc]': 'Pesquise o endereço de um site',
    'form[choosewebaddressfield]': 'Digite seu domínio',
    'form[choosewebaddresssearch]': 'Procurar',
    'form[domainregisterandtransfer]': 'Register or Transfer domain',
    'header[logotext]': 'WHMCS basket'
  }
}

import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { persistReducer, persistStore } from "redux-persist";
import webStorage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import { appReducer, cartReducer } from "./reducers";

const rootReducer = combineReducers({
  cart: cartReducer,
  app: appReducer,
});

const config = {
  key: "WHMCS_CART",
  storage: webStorage,
};

const persistedReducer = persistReducer(config, rootReducer);

export const store = createStore(
  persistedReducer,
  process.env.NODE_ENV === 'production' ? applyMiddleware(thunk) : composeWithDevTools(applyMiddleware(thunk))
);
export const persistor = persistStore(store);
export type IRootState = ReturnType<typeof rootReducer>;

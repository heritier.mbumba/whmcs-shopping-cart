import { ICheckoutFlow } from '@lib/types/api'
import { IPageCheck } from '@lib/types/app'
import { IProductStep, IStep } from '@lib/types/product'
import products from 'pages/api/v1/shop/products'
import { IRootState } from '..'
import { UPDATE_CHECKOUT_OPTION, NEXT_STEP, STEP_INIT, UPDATE_CHECKOUT_FLOW, UPDATE_FLOW_PRODUCT_CONFIG } from '../actions'
import { isStepFullfilled, isStepPassed } from '../helps'



export const selectedCheckoutOptionDispatcher = (key: string) => {
  return (dispatch: Function, state: () => IRootState) => {

    
    const flows = {...state()?.app?.flows}
    const flow = state()?.app?.currentCheckoutFlow

    
    let options: string[] = []
    if(flows.hasOwnProperty(key)){
      options =  flows?.[key]
    }else if(key === 'express'){
      options = []
    }else if(flow === key){
      options = []
    }
    

    dispatch({type: UPDATE_CHECKOUT_FLOW, payload: {flow: flow === key ? null : key, options}})
  }
}

/**
 * Update steps
 * =================================
 * 1. Get current query string from the pathname
 * 2. 
 * @param key 
 * @returns 
 */

export const updatePageStep = (key: string) => {
  return (dispatch: Function, state: () => IRootState) => {
    const currentCheckoutFlow = state()?.app?.currentCheckoutFlow
    const steps = state()?.app?.flows?.[currentCheckoutFlow] as IStep[]
    const items = state()?.cart?.items.slice()
   
    const keyExistInTheSteps = steps?.findIndex(step=>step.key === key) > -1


    if (keyExistInTheSteps) {
      const keyIndex = steps.findIndex(step=>step.key === key)

      steps[keyIndex].checked = true
      steps[keyIndex].passed = false
      for(let i = keyIndex; i !== 0; i--){
        steps[i].checked = true
        steps[i - 1 ].fullfilled = isStepFullfilled(items, steps[i - 1 ].key)
        steps[i-1].passed = isStepPassed(items, steps[i - 1].key)
        
      }
      
    
      dispatch({
        type: NEXT_STEP,
        payload: steps
      })
    }
  }
}

export const initializedSteps = (flows: ICheckoutFlow) => (dispatch: Function) => {

  dispatch({ type: STEP_INIT, payload: flows })
}



import { GoogleAnalytics } from '@lib/analytics'
import { IDomainWithPrice } from '@lib/types/domain'
import { IProduct } from '@lib/types/product'
import { ICartItem } from '@lib/types/shop'
import moment from 'moment'
import { IRootState } from '..'
import {
  ADD_DOMAIN,
  ADD_DOMAIN_EPP_CODE,
  ADD_PRODUCT_TO_DOMAIN,
  PROMO_CODE,
  REMOVE_ITEM,
  REMOVE_PRODUCT_TO_DOMAIN,
  REMOVE_PROMO_CODE,
  UPDATE_ORDER_SUMMARY,
  UPDATE_PRODUCT_BILLING_CYCLE
} from '../actions'
import {
  getDomainPricing,
  getDomainRegPeriod,
  getDomainType,
  onlyUniqueDomains
} from '../helps'

const ga = new GoogleAnalytics()

/**
 * Add selected domains to the store
 * @param newDomains
 * @returns
 */
export const addDomainDispatcher = (newDomains: IDomainWithPrice[]) => {
  return (
    dispatch: ({ type: string, payload: any }) => void,
    state: () => IRootState
  ) => {
    const items = state()?.cart.items.slice()
    const currency = state()?.cart?.currency

    const filteredDomains = newDomains?.filter((domain) =>
      onlyUniqueDomains(
        domain,
        items?.map((item) => item.domain)
      )
    )

    if (filteredDomains.length === 0) {
      return
    }

    filteredDomains.forEach((domain) => {
      items.push({
        domain,
        products: [],
        regperiod: getDomainRegPeriod(domain, items),
        domaintype: getDomainType(domain, items)
      })
    })

    dispatch({
      type: ADD_DOMAIN,
      payload: {
        items
      }
    })

    dispatch({
      type: UPDATE_ORDER_SUMMARY,
      payload: cartSummaryTotal(items, currency)
    })

    return ga.addDomainToCart(filteredDomains)
  }
}

/**
 * Add Domain Epp code for domain transfer
 * @param domain
 * @param code
 * @returns
 */
export const addDomainEppCodeDispatcher = (
  domain: IDomainWithPrice,
  code: string
) => {
  return (dispatch: Function, state: () => IRootState) => {
    const items = state()?.cart?.items.slice()
    const itemIndex = items?.findIndex(
      (item) => item?.domain?.name === domain?.name
    )
    items[itemIndex] = {
      ...items[itemIndex],
      domain: {
        ...items[itemIndex].domain,
        eppcode: code
      }
    }

    dispatch({
      type: ADD_DOMAIN_EPP_CODE,
      payload: items
    })
  }
}

/**
 * remove Domain epp code from the domain
 * @param domain
 * @returns
 */
export const removeDomainEppCodeDispatcher = (domain: IDomainWithPrice) => {
  return (dispatch: Function, state: () => IRootState) => {
    const items = state()?.cart?.items.slice()
    const itemIndex = items?.findIndex(
      (item) => item?.domain?.name === domain?.name
    )

    delete items[itemIndex].domain.eppcode

    dispatch({
      type: ADD_DOMAIN_EPP_CODE,
      payload: items
    })
  }
}

/**
 * Add product to a domain
 * @param domain
 * @param newProduct
 * @returns
 */
export const addProductToDomainDispatcher = (
  domain: IDomainWithPrice,
  newProduct: IProduct
) => {
  return (
    dispatch: ({ type: string, payload: any }) => void,
    state: () => IRootState
  ) => {
    const items = state().cart.items.slice()
    const currency = state()?.cart?.currency

    const itemIndex = items?.findIndex(
      (item) => item.domain.name === domain.name
    )

  

    if (itemIndex > -1) {
      const prodIndex = items[itemIndex].products.findIndex(
        (p) => p.type === getUpdatedProduct(newProduct).type
      )

      if (prodIndex > -1) {
        items[itemIndex].products.splice(prodIndex, 1)
        ga.removeProductToCart(items[itemIndex].products[prodIndex]) //Google Analytics checkout options
        items[itemIndex].regperiod = getDomainRegPeriod(domain, items)
        items[itemIndex].domaintype = getDomainType(domain, items)
        if (newProduct?.freeDomains?.length > 0) {
          items[itemIndex].domain = {
            ...items[itemIndex].domain,
            freeDomain: false
          }
        }
      } else {
        items[itemIndex].products.push({ ...getUpdatedProduct(newProduct) })
        ga.addProductToCart(newProduct)
        items[itemIndex].regperiod = getDomainRegPeriod(domain, items)
        items[itemIndex].domaintype = getDomainType(domain, items)

        /**
         * Update the domain pricing if the product has free domain
         */
        const freeDomains = newProduct?.freeDomains || []

        if (freeDomains?.length > 0 && freeDomains.includes(domain.tld)) {
          items[itemIndex].domain = {
            ...items[itemIndex].domain,
            freeDomain: true
          }
        }
      }
    }

    dispatch({
      type: ADD_PRODUCT_TO_DOMAIN,
      payload: items
    })

    return dispatch({
      type: UPDATE_ORDER_SUMMARY,
      payload: cartSummaryTotal(items, currency)
    })
  }
}

export const removeProductToDomain = (
  domain: IDomainWithPrice,
  prod: IProduct
) => {
  return (
    dispatch: ({ type: string, payload: any }) => void,
    state: () => IRootState
  ) => {
    const items = state().cart.items.slice()
    const currency = state()?.cart?.currency
    const itemIndex = items?.findIndex(
      (item) => item?.domain.name === domain.name
    )

    if (itemIndex > -1) {
      //found the domain

      const getProductIndex = items[itemIndex].products.findIndex(
        (p) => p.pid === prod.pid
      )

      if (getProductIndex > -1) {
        // has that product, please remove it

        items[itemIndex].products.splice(getProductIndex, 1)
        ga.removeProductToCart(items[itemIndex].products[getProductIndex]) //Google Analytics checkout options
        items[itemIndex].regperiod = getDomainRegPeriod(domain, items)
        items[itemIndex].domaintype = getDomainType(domain, items)

        if (prod?.freeDomains?.length > 0) {
          items[itemIndex].domain = {
            ...items[itemIndex].domain,
            freeDomain: false
          }
        }
      }
    }

    dispatch({
      type: REMOVE_PRODUCT_TO_DOMAIN,
      payload: items
    })

    return dispatch({
      type: UPDATE_ORDER_SUMMARY,
      payload: cartSummaryTotal(items, currency)
    })
  }
}

export const updateBillingCycleDispatcher = (
  domain: string,
  pid: number,
  newBillingCycle: string
) => {
  return (dispatch: Function, state: () => IRootState) => {
    const items = state()?.cart?.items.slice()
    const itemIndex = items?.findIndex((item) => item.domain.name === domain)
    const productIndex = items[itemIndex].products.findIndex(
      (p) => p.pid === pid
    )
    const currency = state()?.cart?.currency

    items[itemIndex].products[productIndex].defaultBillingCycle =
      newBillingCycle

    dispatch({
      type: UPDATE_PRODUCT_BILLING_CYCLE,
      payload: items
    })

    return dispatch({
      type: UPDATE_ORDER_SUMMARY,
      payload: cartSummaryTotal(items, currency)
    })
  }
}

/**
 * Remove item from the cart
 * @param domain
 * @returns
 */
export const removeItemInCart = (domain: IDomainWithPrice) => {
  return (
    dispatch: ({ type: string, payload: any }) => void,
    state: () => IRootState
  ) => {
    const items = state().cart.items.slice()
    const currency = state().cart?.currency
    const itemIndex = items.findIndex(
      (item) => item.domain.name === domain.name
    )
    ga.removeItemToCart(items[itemIndex])
    items.splice(itemIndex, 1)

    dispatch({
      type: UPDATE_ORDER_SUMMARY,
      payload: cartSummaryTotal(items, currency)
    })

    dispatch({
      type: REMOVE_ITEM,
      payload: items
    })
  }
}

/**
 * Calculate pro rata am
 * @param amount
 * @param billingCycle
 * @returns
 */
export const getProRataAmount = (
  amount: number,
  billingCycle: string = 'monthly'
) => {
  if (['monthly', 'semiannually', 'quarterly'].includes(billingCycle)) {
    const endOfMonth = moment().endOf('month')
    const today = moment()
    const diff = endOfMonth.diff(today, 'days') + 1
    const pastDays = moment().daysInMonth() - diff
    let perDayAmount = Number(amount) / moment().daysInMonth()

    if (billingCycle === 'monthly') {
      return perDayAmount * diff
    } else if (billingCycle === 'semiannually') {
      const semiannually = moment().add(5, 'months')
      const remainedDays = semiannually.diff(today, 'days') + diff

      perDayAmount = Number(amount) / (remainedDays + pastDays)

      return perDayAmount * remainedDays
    } else if (billingCycle === 'quarterly') {
      const quarterly = moment().add(2, 'months')
      const remainedDays = quarterly.diff(today, 'days') + diff

      perDayAmount = Number(amount) / (remainedDays + pastDays)

      return perDayAmount * remainedDays
    }
  }

  return 0.0
}

export const cartSummaryTotal = (
  items: ICartItem[],
  currency: 'ZAR' | 'USD'
) => {
  const total = items?.reduce((prev, current) => {
    const domainTotal = current?.domain?.freeDomain
      ? 0.0
      : getDomainPricing(current?.domain)
    const productsTotal = current?.products.reduce((p, c) => {
      let bcycle = c?.defaultBillingCycle

      const getTotal = () => {
        const normalTotal = Number(c?.pricing?.[currency || 'ZAR']?.[bcycle])
        const proRataTotal = getProRataAmount(c?.pricing?.[currency || 'ZAR']?.[bcycle], bcycle)

        if (bcycle === 'annually') {
          return (p += normalTotal)
        }
        return (p += proRataTotal)
      }

      return getTotal()
    }, 0)

    return (prev += domainTotal + productsTotal)
    // return prev += domainTotal
  }, 0)

  return {
    subtotal: total - total * 0.15,
    vat: total * 0.15,
    total
  }
}

export const applyPromotionCode = (payload: {
  type: string
  concerned: string
  value: number
  code: string
}) => {
  return (dispatch: Function, state: () => IRootState) => {
    const items = state()?.cart?.items?.slice()
    const domains: string[] = []

    let itemIds = []
    const productIds = items
      ?.map((item) => item?.products.map((p) => p.pid))
      .flatMap((p) => p)
    const domainIds = items?.map((item) => 'D.' + item?.domain.tld)

    itemIds = productIds.concat()
    
    const appliedTo = []

    const { type, concerned, value, code } = payload
    let CalcUnit

    const concernedArr = concerned.split(',')
    itemIds.forEach((id) => {
      if (concerned.includes(id)) {
        appliedTo.push(id)
      }
    })

    if (appliedTo.length === 0) {
      return dispatch({
        type: PROMO_CODE,
        payload: {
          code,
          discount: {
            code,
            type,
            value,
            message:
              'Promo code is not applicable to the product(s) in the cart',
            label: 'Error'
          }
        }
      })
    }

    if (type === 'Percentage') {
      CalcUnit = 'Percentage'
    }

    if (concerned?.includes(',')) {
      //Many products for the same promo code
    } else {
      if (concerned?.includes('D.')) {
        const [name, ...tld] = concerned?.split('.')
        domains.push(tld.join('.'))
      }
    }

    dispatch({
      type: PROMO_CODE,
      payload: {
        code,
        discount: {
          code,
          type,
          value,
          message:
            type === 'Percentage'
              ? `Promo discount of ${value} ${
                  type === 'Percentage' && '%'
                } will be applied`
              : 'Promo discount will be applied on your invoice',
          label: 'Applied'
        }
      }
    })
  }
}

export const removePromoCodeDispatcher = () => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_PROMO_CODE
    })
  }
}


export const getUpdatedProduct = (product: IProduct): IProduct => {
  if(product?.type === 'sitebuilder'){
    return {
      ...product,
      type: 'hosting'
    }
  }
  return product
}
import { IDomainWithPrice } from '@lib/types/domain'
import { ICartItem } from '@lib/types/shop'

/**
 * Get domain pricing based on the status or type
 * @param domain
 * @returns
 */
export const getDomainPricing = (domain: IDomainWithPrice) => {
  if (domain?.status) {
    return +domain?.register
  } else if (domain?.type === 'existing') {
    return 0.0
  } else {
    return +domain?.transfer
  }
}

/**
 * Check if the domain has a hosting package
 * @param domain
 * @param items
 * @returns
 */
export const hasHostingPackage = (
  domain: IDomainWithPrice,
  items: ICartItem[]
) => {
  const domainIndex = items.findIndex(
    (item) => item.domain.name === domain.name
  )
  const productIndex = items[domainIndex]?.products?.findIndex((product) =>
    ['hosting', 'sitebuilder'].includes(product?.type)
  )
  if (productIndex !== -1) {
    return true
  }
  return false
}

/**
 * Filter unique domains
 * @param domain
 * @param domains
 * @returns
 */
export const onlyUniqueDomains = (
  domain: IDomainWithPrice,
  domains: IDomainWithPrice[]
) => {
  const index = domains.findIndex((d) => d.name === domain.name)

  if (index > -1) {
    return false
  }
  return true
}

const DOMAIN_STATUS = {
  1: 'register',
  0: 'transfer',
  2: 'existing'
}
const DOMAIN_REG = {
  1: '1',
  0: '',
  2: ''
}

export const getDomainType = (domain: IDomainWithPrice, items: ICartItem[]) => {
  const domaintype: string[] = []

  const concerned = items?.find((item) => item.domain.name === domain.name)

  if (concerned?.products?.length > 0) {
    concerned?.products?.forEach((product, index) => {
      if (index === 0) {
        domaintype[0] = DOMAIN_STATUS?.[domain?.status]
      } else {
        domaintype.push('')
      }
    })
  } else {
    domaintype.push(DOMAIN_STATUS?.[domain?.status])
  }

  return domaintype
}

export const getDomainRegPeriod = (
  domain: IDomainWithPrice,
  items: ICartItem[]
) => {
  const regperiod: string[] = []

  const concerned = items?.find((item) => item.domain.name === domain.name)

  if (concerned?.products?.length > 0) {
    concerned?.products?.forEach((product, index) => {
      if (index === 0) {
        regperiod[0] = DOMAIN_REG?.[domain?.status]
      } else {
        regperiod.push('')
      }
    })
  } else {
    regperiod.push(DOMAIN_REG?.[domain?.status])
  }

  return regperiod
}

export const isStepFullfilled = (items: ICartItem[], key: string) => {
  const products = items.map((item) => item.products).flatMap((d) => d)
  const domains = items.map((item) => item.domain)
  if (key === 'domain' && domains.length > 0) {
    return true
  }

  if (key === 'hosting') {
    const exist =
      products.filter((product) =>
        ['sitebuilder', 'hosting'].includes(product.type)
      )?.length > 0
    if (exist) {
      return true
    } else {
      return false
    }
  }

  const wasFullfilled =
    products.filter((product) => product.type === key)?.length > 0

  if (wasFullfilled) {
    return true
  }

  return false
}

export const isStepPassed = (items: ICartItem[], key: string) => {
  const products = items.map((item) => item.products).flatMap((d) => d)
  const domains = items.map((item) => item.domain)

  if (key === 'hosting') {
    const passed =
      products.filter((product) =>
        ['sitebuilder', 'hosting'].includes(product.type)
      )?.length === 0
    if (passed) {
      return true
    } else {
      return false
    }
  }

  if (key === 'domain') {
    if (domains.length === 0) {
      return true
    } else {
      return false
    }
  }

  return products.filter((product) => product?.type === key)?.length === 0
}

import { IDomain, IDomainWithPrice } from "@lib/types/domain";
import { ICartItem } from "@lib/types/shop";
import {
  ADD_DOMAIN,
  ADD_DOMAIN_EPP_CODE,
  ADD_PRODUCT_TO_DOMAIN,
  APP_INIT,
  UPDATE_CHECKOUT_OPTION,
  PROMO_CODE,
  REMOVE_ITEM,
  REMOVE_PRODUCT_TO_DOMAIN,
  RESET_CART,
  UPDATE_ORDER_SUMMARY,
  UPDATE_PRODUCT_BILLING_CYCLE,
  USED_DOMAINS,
  REMOVE_PROMO_CODE,
} from "../actions";

export interface IOrderSummary {
  subtotal: number;
  vat: number;
  total: number;
};

export interface IDiscount {
  code: string
  type: string
  value: number;
  message?: string;
  label?: string
}


export interface ICartState {
  items: ICartItem[];
  orderSummary: IOrderSummary;
  discount?: IDiscount;
  currency?: "ZAR" | "USD"
  promocode?: string
}
type CartAction = {
  type: string;
  payload?: any;
};

const CartState: ICartState = {
  items: [],
  orderSummary: {
    subtotal: 0.0,
    vat: 0.0,
    total: 0.0,
  },
  
  
};

export default function cartReducer(
  state = CartState,
  { type, payload }: CartAction
) {
  switch (type) {
    case ADD_DOMAIN:
      return {
        ...state,
        items: payload.items as ICartItem[],
      };

    case ADD_PRODUCT_TO_DOMAIN:
      return {
        ...state,
        items: payload as ICartItem[],
      };

    case REMOVE_PRODUCT_TO_DOMAIN:
      return {
        ...state,
        items: payload as ICartItem[],
      };

    case REMOVE_ITEM:
      return {
        ...state,
        items: payload as ICartItem[]
      }

    case UPDATE_ORDER_SUMMARY:
      return {
        ...state,
        orderSummary: payload as IOrderSummary,
      };

    case UPDATE_PRODUCT_BILLING_CYCLE:
      return {
        ...state,
        items: payload as ICartItem[],
      };

    case ADD_DOMAIN_EPP_CODE:
      return {
        ...state,
        items: payload as ICartItem[]
      }

    case APP_INIT:
     
      return {
        ...state,
        currency: payload?.currency as "ZAR" | "USD",
        
        
      }

    case PROMO_CODE:
      return {
        ...state,
        promocode: payload.code as string,
        discount: payload.discount as IDiscount
      }

    case REMOVE_PROMO_CODE:
      return {
        ...state,
        promocode: null,
        discount: null
      }

    

    case RESET_CART:
      return CartState

    default:
      return state;
  }
}

import { IProduct, IProductGroup } from "@lib/types/product";

export interface IShopState {
  products: IProduct[];
  groups: IProductGroup[];
}
type ShopAction = {
  type: string;
  payload?: any;
};

const ShopState: IShopState = {
  products: [],
  groups: [],
};

export default function shopReducer(
  state = ShopState,
  { type, payload }: ShopAction
) {
  switch (type) {
    default:
      return state;
  }
}

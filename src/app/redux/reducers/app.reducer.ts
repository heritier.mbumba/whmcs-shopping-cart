import { ICheckoutFlow } from '@lib/types/api'
import { IPageCheck } from '@lib/types/app'
import moment, { Moment } from 'moment'
import {
  APP_INIT,
  NEXT_STEP,
  STEP_INIT,
  UPDATE_CHECKOUT_FLOW,
  UPDATE_CHECKOUT_OPTION,
  UPDATE_FLOW_PRODUCT_CONFIG,
} from '../actions'


export interface IAppState {

  currentCheckoutFlow: string
  flows?:  ICheckoutFlow
  lastSync?: number
  checkoutOptions?: string[];
  googleClick?: {
    gclid: string;
    expires_at: Moment
  }
}

type AppAction = {
  type: string
  payload: any
}

const AppState: IAppState = {
  currentCheckoutFlow: 'domain',
  lastSync: Date.now()
}

export default function appReducer(
  state = AppState,
  { type, payload }: AppAction
) {
  switch (type) {

    case APP_INIT:
      const flow = payload?.flows?.[state?.currentCheckoutFlow] as string[]
      const options = flow?.map(flow=>flow)
     
      return {
        ...state,
        flows: payload?.flows as ICheckoutFlow,
        currentCheckoutFlow: 'sitebuilder',
        checkoutOptions: options,
        googleClick: {
          gclid: payload?.gclid,
          expires_at: moment().add('3', 'months')
        }

      }

    case NEXT_STEP:
      return {
        ...state,
        flows: {
          ...state.flows,
          [state.currentCheckoutFlow]: payload
        }
      }

    case STEP_INIT:
      
      return {
        ...state,
        flows: payload as ICheckoutFlow
      }

    case UPDATE_CHECKOUT_FLOW:
      return  {
        ...state,
        currentCheckoutFlow: payload.flow as string,
        checkoutOptions: payload.options as string[]
      }

    case UPDATE_FLOW_PRODUCT_CONFIG:
      return {
        ...state,
        currentProductConfig: payload as string
      }

    case 'INITIAL_FLOW':
      return {
        ...state,
        currentCheckoutFlow: 'sitebuilder'
      }

    default:
      return state
  }
}

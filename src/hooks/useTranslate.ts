import { LANG } from '../app/lang/index'

/**
 * Translation helper function
 * @returns a function for doing translation calls
 */
export default function useTranslate() {
  const t = (key: string) => {
    if (!LANG['en']?.[key]) {
      console.warn(`No string '${key}' for locale english' and target ${key}`)
    }
    return LANG['en']?.[key] || LANG['noLang']
  }

  return {
    t
  }
}

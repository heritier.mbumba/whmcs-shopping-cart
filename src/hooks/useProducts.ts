import { useApplication } from '@app/context/App'
import { useShop } from '@app/context/Shop'
import { IProduct, IProductGroupType, IProductType } from '@lib/types/product'
import { productDescriptions } from '@utils/default'
import React from 'react'

interface useProductsProps {
  type: IProductType
  group?: IProductGroupType
}

interface IProductDescription {
  description: string
  features: string[]
}

/**
 * Helper function that provides methods to retrieve data regarding products
 * @returns
 */
export default function useProducts() {
  /**
   * Hook used to manage the state of products
   * NOT SURE IF WE ACTUALLY NEED THIS
   */
  const [products, setProducts] = React.useState<IProduct[]>()

  /**
   * Hook used to manage the state of errors that may occur
   */
  const [error, setError] = React.useState<{ title: string; message: string }>()

  /**
   * Extracts the supported product groups from the application config
   */
  const {
    config: { supportedProductGroups }
  } = useApplication()

  /**
   * Extracts the items state from the Shop Provider from redux
   */
  const { items } = useShop()

  /**
   * Retrieves the description of a product
   * @param type the type of product to search for
   * @param pid the id of the product to search for
   * @returns String that contains the description of a specified product
   */
  const getProductDescription = React.useCallback(
    (type: string, pid: number): IProductDescription => {
      return productDescriptions?.[type]?.[pid]
    },
    []
  )

  /**
   * Checks to see which domains are free on certain products
   * @param gid the id of a group to search through
   * @param pid the id of the product to search for
   */
  const getProductFreeDomains = React.useCallback(
    (gid: number, pid: number) => {
      const group = supportedProductGroups?.find((group) => group?.id === gid)
      return group?.freeDomains?.[pid]
    },
    []
  )

  /**
   * Checks to see if a product has already been selected
   * @param domain the domain that has to be checked
   * @pid the id of the product that has been selected
   * @returns boolean whose value is dependant on if a product is already seleceted for a domain
   */
  const isProductSelected = React.useCallback((domain: string, pid) => {
    const item = items?.find((item) => item.domain.name === domain)
    if (item) {
      const { products } = item
      const product = products?.find((prod) => prod.pid === pid)
      if (product) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }, [])

  /**
   * Checks for domains that have products attached to them
   * @returns an array of domains with products attached to them
   */
  const getUsedDomains = () => {
    const products = items?.filter((item) => item?.products?.length > 0)

    return products
      ?.filter((item) => {
        const prods = item?.products?.filter((product) =>
          ['sitebuilder', 'hosting'].includes(product?.type)
        )
        return prods.length > 0
      })
      ?.map((p) => p.domain.name)
  }

  const memoReturn = React.useMemo(
    () => ({
      products,
      error,
      getProductDescription,
      getProductFreeDomains,
      isProductSelected,
      getUsedDomains
    }),
    []
  )

  return memoReturn
}

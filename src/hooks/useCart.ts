import { useShop } from '@app/context/Shop'
import { IDomainWithPrice } from '@lib/types/domain'
import { IProduct } from '@lib/types/product'
import React from 'react'

/**
 * Function used to generate data based on the Cart data
 */
export default function useCart() {
  /**
   * Hook used to retrieve the items from the shop state
   */
  const { items } = useShop()

  /**
   * Helper function used to check if a domain already has this product attached to it
   * @param domain the domain that needs to be checked
   * @param product the product that is going to be compared
   * @returns
   */
  const domainHasTheSameProduct = (
    domain: IDomainWithPrice,
    product: IProduct
  ) => {
    const itemIndex = items?.findIndex(
      (item) => item.domain.name === domain?.name
    )
    if (itemIndex > -1) {
      const productIndex = items[itemIndex]?.products?.findIndex(
        (p) => p.pid === product.pid || p.type === product.type
      )
      if (productIndex > -1) {
        return true
      }
      return false
    }
  }

  /**
   * Helper function used to check if a product has already been selected
   * @param domain the domain the product is attached to
   * @param pid the id of the product that is going to be checked
   * @returns a boolean that is determined by if the product is already in the cart
   */
  const isProductSelected = (domain: IDomainWithPrice, pid: number) => {
    const itemIndex = items?.findIndex(
      (item) => item.domain.name === domain.name
    )
    const prodIndex = items[itemIndex]?.products?.findIndex(
      (p) => p.pid === pid
    )
    if (prodIndex > -1) {
      return true
    }
    return false
  }

  /**
   * Helper function that checks if a domain already has a product attached of the same type
   * @param domain the domain that is going to be checked
   * @param type the type of the product
   * @returns returns a boolean based off of if the domain already has a similar type of product attached
   */
  const domainHasTheSameProductType = (
    domain: IDomainWithPrice,
    type: string
  ) => {
    const itemIndex = items?.findIndex(
      (item) => item.domain.name === domain?.name
    )
    if (itemIndex > -1) {
      const productIndex = items[itemIndex]?.products?.findIndex(
        (p) => p.type === type
      )
      if (productIndex > -1) {
        return true
      }
      return false
    }
  }

  /**
   *
   */
  const canDomainBeUsed = React.useCallback(
    (domain: IDomainWithPrice, type: string) => {
      const itemIndex = items?.findIndex(
        (item) => item.domain.name === domain?.name
      )
      const products = items[itemIndex]?.products.filter((p) =>
        ['hosting', 'sitebuilder'].includes(type)
      )

      if (products?.length > 0) {
        return false
      }
      return true
    },
    []
  )

  /**
   * Helper function to check if an item is in the cart
   * @param key the type of the product to be checked
   * @param domain the domain to be checked for product
   * @returns a boolean whose value depends on if the product is in the cart
   */
  const isCartItemHas = React.useCallback(
    (key: string, domain: string) => {
      const item = items?.find((item) => item.domain.name === domain)
      if (item) {
        if (item?.products?.length > 0) {
          if (key === 'package') {
            return true
          } else if (key === 'security') {
            const index = item?.products?.findIndex(
              (product) => product.type === 'ssl'
            )
            if (index > -1) {
              return true
            } else {
              return false
            }
          } else {
            const index = item?.products?.findIndex(
              (product) => product.type === key
            )
            if (index > -1) {
              return true
            } else {
              return false
            }
          }
        } else {
          if (key === 'domain') {
            return true
          }
        }
      } else {
        return false
      }
    },
    [items]
  )

  /**
   * This function is used to build up the payload regarding a user's order
   * @returns a payload for the order a user has placed
   */
  const getOrderPayload = () => {
    //checks if a domain has products attached to it
    const hasProducts =
      items?.filter((item) => item?.products?.length > 0)?.length > 0

    //returns an array containing the names of the domains in the items shop state
    const domains = items?.map((item) => item.domain.name)

    //object with details regariding the order
    const order = {
      pid: items
        ?.map((item) => item?.products?.map((p) => p.pid))
        .flatMap((p) => p),
      domain: hasProducts
        ? items
            ?.map((item) => item?.products?.map((p) => item.domain.name))
            .flatMap((d) => d)
        : domains,
      eppcode: items
        ?.map((item) => item?.products?.map((p) => item.domain?.eppcode || ''))
        .flatMap((d) => d),
      billingcycle: items
        ?.map((item) => item?.products?.map((p) => p.defaultBillingCycle))
        .flatMap((d) => d),
      regperiod: items
        ?.map((item) => item.regperiod?.map((rg) => +rg))
        .flatMap((d) => d),
      domaintype: items?.map((item) => item?.domaintype).flatMap((d) => d),
      addons: items
        ?.map((item) =>
          item?.products?.map(
            (p) => p.addons?.map((a) => a.id || '').flatMap((a) => a) || ''
          )
        )
        .flatMap((p) => p),
      noemail: true,
      noinvoiceemail: true
    }

    return order
  }

  /**
   * Creates a React Memo to prohibit unneeded rerenders
   */
  const memoReturn = React.useMemo(
    () => ({
      domainHasTheSameProduct,
      domainHasTheSameProductType,
      canDomainBeUsed,
      isProductSelected,
      isCartItemHas,
      getOrderPayload
    }),
    []
  )

  return memoReturn
}

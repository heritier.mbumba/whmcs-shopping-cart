import React from 'react'

/**
 * Helper function that is used to make API calls fro fetching data
 * @param url the url to send the request to
 * @param token a token used for authentication
 * @returns the built up request based on the provided parameters
 */
export default function useFetch() {
  const request = React.useCallback((url: string, token?: string): {
    post: (data: Record<string, any>) => Promise<{
      data?: any
      status: number
      message?: string
    }>
    get: () => Promise<
      | {
          data?: Record<string, any>
          status: number
          message?: string
        }
      | any
    >
  } => {
    //sets the headers in the request call
    const headers = new Headers({
      'Content-Type': 'application/json'
    })

    //if there is a token, it adds it to the request headers
    if (token) {
      headers.append('token', token)
    }

    //timeout used to cancel a request
    let timer = setTimeout(() => {
      return {
        status: 408,
        message: 'Message found'
      }
    }, 30000)

    return {
      /**
       * Post request to send data
       * @param payload the data that should be included in the body of the request
       * @returns a response status and the data from the response, or an error message
       */
      post: async (payload: Record<string, any> | null = null) => {
        try {
          const req = await fetch(url, {
            method: 'POST',
            headers,
            body: JSON.stringify(payload)
          })

          const { status, data, message } = await req.json()

          if (status === 200) {
            clearTimeout(timer)
            return {
              status: 200,
              data
            }
          }
          clearTimeout(timer)
          return {
            status: 400,
            message
          }
        } catch (error) {
          clearTimeout(timer)
          return {
            status: 500,
            message: 'Server response error'
          }
        }
      },

      /**
       * Get request to retrieve data
       * @returns data revceived from the request, or an error message if it failed
       */
      get: async () => {
        try {
          const req = await fetch(url, {
            method: 'GET',
            headers
          })
          const data = await req.json()

          if (data?.status === 200) {
            clearTimeout(timer)
            return { ...data }
          }
          clearTimeout(timer)
          return {
            ...data
          }
        } catch (error) {
          clearTimeout(timer)

          return {
            status: 500,
            message: 'Server response error'
          }
        }
      }
    }
  }, [])

  return {
    request
  }
}

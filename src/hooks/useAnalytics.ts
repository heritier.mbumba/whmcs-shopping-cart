import { GoogleAnalytics } from '@lib/analytics'
import React from 'react'

const useAnalytics = () => React.useCallback(() => new GoogleAnalytics(), [])

export default useAnalytics

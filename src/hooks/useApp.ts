import { IApplicationConfig } from "@lib/types/api";
import config from '../../config.json';

export default function useApp() {


  const configuration = config as IApplicationConfig

  const loadingConfig = () => configuration

  return {
    loadingConfig,
  };
}

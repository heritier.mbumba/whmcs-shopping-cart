export default function useAssets(){

    const getImage = (src: string) => {
        return "/images/" + src
    }

    return {
        getImage
    }
}
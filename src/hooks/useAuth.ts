import moment from 'moment'
import { DefaultSession } from 'next-auth'
import { getSession } from 'next-auth/client'
import React from 'react'
import useFetch from './useFetch'

type UseAuthState = {
  user: DefaultSession
  isAuthenticated: boolean
}

/**
 * Helper function that provides authentication details regarding a user
 * @returns the authenticated user and their authenticated status
 */
export default function useAuth() {
  /**
   * Hook used to manage the state of a user
   */
  const [state, setState] = React.useState<UseAuthState>()

  /**
   * Helper function used to make api calls
   */
  const { request } = useFetch()

  /**
   * When the component mounts, this functionality is executed
   * Retrieves the session of a user and sets it in a state
   */
  React.useEffect(() => {
    let isActive = true

    if (isActive) {
      ;(async () => {
        const auth = await getSession({
          triggerEvent: true
        })

        if (auth?.user) {
          setState({ user: auth.user, isAuthenticated: true })
        }
      })()
    }

    return () => {
      isActive = false
    }
  }, [])

  /**
   * Callback used to retrieve the session of a user
   * Returns a boolean based on if a user's session has expired
   */
  const checkAuth = React.useCallback(async () => {
    const data = (await request('/api/auth/session').get()) as DefaultSession

    return {
      alreadyExpired:
        Object.values(data)?.length > 0 ? moment().isAfter(data?.expires) : true
    }
  }, [])

  return {
    ...state,
    checkAuth
  }
}

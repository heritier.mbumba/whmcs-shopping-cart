import { useShop } from '@app/context/Shop'
import DomainLookupResult from '@components/Domain/DomainLookupResult'
import DomainSearchForm from '@components/Domain/SearchForms/DomainSearchForm'
import Page from '@components/HOC/Page'
import { PageProps } from '@lib/types/app'
import { IDomainWithPrice } from '@lib/types/domain'
import { NextPage } from 'next'
import { useRouter } from 'next/dist/client/router'
import React from 'react'

interface HomePageProps extends PageProps {}

/**
 * Component used to display the UI to the user containing the domain search form and domain search results
 * @param client data regarding details of the client/user
 * @param otherProps other props passed from the parent. Includes details related to the metadata of this page
 * @returns JSX element for displaying the UI related to the domain search and results
 */
const HomePage: NextPage<HomePageProps> = ({ client, ...otherProps }) => {

  const router = useRouter()

  const {currentCheckoutFlow, onFlowInit} = useShop()

  /**
   * Hook used to manage the state of the search results of a domain
   */
  const [result, setResult] =
    React.useState<{
      tld: string
      domains: IDomainWithPrice[]
      error?: Record<string, any>;
      alternativeDomains: string[]
    }>()


  /**
   * Initialize the current flow to sitebuilder if it is null
   */
  React.useEffect(()=>{
    if(!currentCheckoutFlow){
      onFlowInit()
    }
  }, [result])

  /**
   * Helper function used to reset the state of the search results
   */
  const resetResultState = () => {
    router.query  = undefined
    setResult(null)
  }

  return (
    <Page {...otherProps}>
      {!result?.domains && (
        <DomainSearchForm
          {...{
            style: 'default',
            query: router?.query as {domain: string},
            onResultHandler: (tld, domains, alternativeDomains) => setResult({ tld, domains, alternativeDomains })
          }}
        />
      )}
      {result?.domains && (
        <DomainLookupResult {...{ result, setResult, resetResultState }} />
      )}
    </Page>
  )
}

export default HomePage

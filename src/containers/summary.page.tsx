import { useLazyQuery } from '@apollo/client'
import { useShop } from '@app/context/Shop'
import BoxItem from '@components/Box/BoxItem'
import ModalDefault, { IDefaultModalRef } from '@components/Modal/ModalDefault'
import OrderItemList from '@components/Order/OrderItemList'
import { Colors } from '@constants/Colors'
import useAnalytics from '@hooks/useAnalytics'
import useTranslate from '@hooks/useTranslate'
import { PageProps } from '@lib/types/app'
import { GET_PROMOTIONS } from '@utils/graphql-type'
import { motion } from 'framer-motion'
import { NextPage } from 'next'
import { useRouter } from 'next/dist/client/router'
import Link from 'next/link'
import React from 'react'
import { FaRegTimesCircle } from 'react-icons/fa'
import Page from 'src/components/HOC/Page'
import pageStyles from './page.module.scss'
import styles from './summary.module.scss'

interface SummaryrPageProps extends PageProps {}

/**
 * Parent component used to display the summary page
 * Contains the summary of the order pricing and the items (domains with their attached products) a user has selected
 * @param client data related to the client/user logged in
 * @param otherProps metadata related to the current page
 * @returns JSX element that handles the UI for the summary page
 */
const SummaryPage: NextPage<SummaryrPageProps> = ({
  client,
  ...otherProps
}) => {
  const { t } = useTranslate() //translation helper function

  /**
   * Shop hook used to extract the data from the redux store
   */
  const {
    orderSummary,
    items,
    discount,
    onApplyPromotionCode,
    onRemovePromoCode
  } = useShop()

  /**
   * Hook used to handle the state of the promocode
   */
  const [promoCode, setPromoCode] = React.useState<string>()

  /**
   * GraphQL query used to retrieve data related to any promotions
   */
  const [getPromotions, { data, loading, error }] = useLazyQuery(GET_PROMOTIONS)

  /**
   * Modal Ref
   */
  const modalRef = React.useRef<IDefaultModalRef>()

  /**
   * Nextjs router hook
   */
  const router = useRouter()

  const LogEvent = useAnalytics()


  React.useEffect(() => {
    if(items.length > 0){
      LogEvent().checkout(4, 'Order Summary')
    }
  }, [items])

  /**
   * Apply promo GraphQL request,
   * This verify the promo code authenticity
   */
  const onApplyPromoHandler = React.useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault()
      if (!promoCode) {
        return alert(t('page[summary][promoCode]'))
      }

      getPromotions({
        variables: {
          code: promoCode
        }
      })
    },
    [loading, promoCode, data]
  )

  /**
   * Hook used to handle the behavior when a user enters a promocode
   */
  React.useEffect(() => {
    if (!loading) {
      modalRef.current.closeSpinner()
    }
    if (data?.getPromotions) {
      // modalRef.current.close()
      //proccess the discount here
      const { message, code, ...promotion } = data?.getPromotions[0]
      if (message) {
        modalRef.current.renderTitle(
          `${t('page[summary][invalidCode]')}: ${code}`
        )
        modalRef.current.renderDescription(message)
      } else {
        const { type, value, appliesto } = promotion
        onApplyPromotionCode({ type, concerned: appliesto, value, code })
      }
    }
  }, [loading, data])

  return (
    <Page {...otherProps}>
      <section className={`${pageStyles.page__summary} ${pageStyles.page}`}>
        <div className='container pb-5'>
          {items?.length > 0 && (
            <div className='row d-xs-block d-md-none mt-5'>
              <div className='col-xs-12'>
                <div className={styles.summary__ordermobile}>
                  <h1 className='heading_big heading_upper'>Total</h1>
                  <h4 className='heading__primary heading_medium border-bottom pb-3'>
                    R{orderSummary.total?.toFixed(2) || '0.00'}
                  </h4>
                  <div className='d-flex'>
                    <div className='sub-total me-5'>
                      <span className='d-block'>
                        {t('page[summary][subtotal]')}
                      </span>
                      <span className={styles.summary_vatpill}>
                        R{orderSummary?.subtotal.toFixed(2)}
                      </span>
                    </div>
                    <div className='vat-total'>
                      <span className='d-block'>
                        {t('page[summary][vatText]')}(
                        {t('page[summary][vatAmount]')})
                      </span>
                      <span className={styles.summary_vatpill}>
                        R{orderSummary?.vat.toFixed(2)}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

          <div className='row'>
            {/* if there are no items in a users cart, display a message and a button that reroutes them to the start of a flow */}
            {items?.length === 0 && (
              <div className='col-md-12 col-xs-12 text-center mt-5'>
                <h1 className='heading__primary'>
                  {t('page[summary][noProducts][title]')}
                </h1>
                <span className='d-block mb-5'>
                  {t('page[summary][noProducts][description]')}
                </span>
                <Link href='/'>
                  <a className=' btn btn-pink'>
                    {t('page[summary][noProducts][action]')}
                  </a>
                </Link>
              </div>
            )}
            {items.length > 0 && (
              <div className='col-xs-12 col-md-8'>
                <h4 className='heading_small heading_upper mt-5 mb-4'>
                  {t('page[summary][items][title]')}
                </h4>
                <motion.div className={styles.order__item_container}>
                  {items?.map(({ domain, products }) => (
                    <OrderItemList
                      {...{ products, domain }}
                      key={domain.name}
                    />
                  ))}
                  {items?.length > 0 && (
                    <button
                      className='btn btn-pink d-md-none'
                      onClick={() => router.push('checkout')}
                    >
                      {t('button[checkout]')}
                    </button>
                  )}
                </motion.div>
              </div>
            )}
            {items.length > 0 && (
              <div className='d-none d-md-block col-xs-12 col-md-4 mt-5 pb-3'>
                <div className={styles.summary__order}>
                  <h1 className='heading_big heading_upper'>
                    {t('page[summary][items][total]')}
                  </h1>
                  <hr />
                  <div className='summary__order_detail pt-3 mb-4'>
                    <div className='d-flex align-items-center justify-content-between subtotal'>
                      <span className='d-block paragraph heading_upper'>
                        {t('page[summary][subtotal]')}
                      </span>
                      <span className='d-block heading_small'>
                        R{orderSummary.subtotal.toFixed(2)}
                      </span>
                    </div>
                    <div className='d-flex align-items-center justify-content-between vat'>
                      <span className='d-block paragraph'>
                        {t('page[summary][vatText]')} (
                        {t('page[summary][vatAmount]')})
                      </span>
                      <span className='d-block heading_small'>
                        R{orderSummary.vat.toFixed(2)}
                      </span>
                    </div>
                    <div className='d-flex align-items-center justify-content-between total'>
                      <span className='d-block paragraph heading_upper'>
                        {t('page[summary][total]')}
                      </span>
                      <strong className='d-block heading__primary_medium color-pink'>
                        R{orderSummary.total.toFixed(2)}
                      </strong>
                    </div>
                  </div>
                  {discount ? (
                    <BoxItem
                      boxShowOff
                      cssClasses={`d-flex align-items-center justify-content-between`}
                    >
                      <div>
                        <h4 className='title-f-600 text-success'>
                          {discount?.label}
                        </h4>
                        <span className='small'>{discount?.message}</span>
                      </div>
                      <FaRegTimesCircle
                        size={20}
                        color={Colors.danger}
                        onClick={onRemovePromoCode}
                      />
                    </BoxItem>
                  ) : loading ? (
                    <div>
                      <span>{t('common[pleasewait]')}</span>
                    </div>
                  ) : (
                    <form onSubmit={onApplyPromoHandler} className='row mb-2'>
                      <div className='col-xs-12 col-md-9 pe-0'>
                        <input
                          type='text'
                          className={`${styles.summary_promo_input} form-control`}
                          placeholder='apply promo code'
                          onChange={(e) => setPromoCode(e.currentTarget.value)}
                        />
                      </div>
                      <div className='col-md-3 col-xs-12 ps-0'>
                        <button
                          className={`${styles.order_apply_button} btn btn-secondary`}
                          disabled={loading}
                        >
                          {t('page[summary][apply]')}
                        </button>
                      </div>
                    </form>
                  )}
                  {data?.getPromotions[0]?.message && (
                    <p className='text-danger'>
                      {data?.getPromotions[0]?.message}
                    </p>
                  )}
                  <button
                    className='btn btn-pink mt-3'
                    onClick={() => router.push('checkout')}
                  >
                    {t('button[checkout]')}
                  </button>
                </div>
              </div>
            )}
          </div>
        </div>
        <ModalDefault ref={modalRef} />
      </section>
    </Page>
  )
}

export default SummaryPage

import { useLazyQuery } from '@apollo/client'
import { useApplication } from '@app/context/App'
import { useShop } from '@app/context/Shop'
import { RESET_CART } from '@app/redux/actions'
import withAuth from '@components/HOC/withAuth'
import LoadingSpinner from '@components/Modal/LoadingSpinner'
import ModalDefault, { IDefaultModalRef } from '@components/Modal/ModalDefault'
import { Colors } from '@constants/index'
import useAnalytics from '@hooks/useAnalytics'
import useAuth from '@hooks/useAuth'
import useCart from '@hooks/useCart'
import useTranslate from '@hooks/useTranslate'
import { IGateway } from '@lib/types/api'
import { PageProps } from '@lib/types/app'
import { ADD_GCLID, ADD_ORDER, GET_UNPAID_INVOICE } from '@utils/graphql-type'
import moment from 'moment'
import { NextPage } from 'next'
import { useRouter } from 'next/dist/client/router'
import React from 'react'
import { FaCheckCircle, FaRegCircle } from 'react-icons/fa'
import { useDispatch } from 'react-redux'
import Page from 'src/components/HOC/Page'
import styles from './checkout.module.scss'

interface CheckoutPageProps extends PageProps {
  csrfToken: string
  orderInvoice?: {
    client: {
      firstname: string
      lastname: string
      email: string
    }
    invoiceid: number
  }
}

/**
 * Page that manages the UI for displaying different payment methods to a user
 * @param client the details of the current client logged in
 * @param orderInvoice invoice for the order that has been generated
 * @param otherProps the other props, just the route path
 * @returns
 */

const CheckoutPage: NextPage<CheckoutPageProps> = ({
  client,
  orderInvoice,
  ...otherProps
}) => {
  const { t } = useTranslate() //translation helper function

  /**
   * Creates a ref to only close the specific modal related to this component, eg the results modal
   */
  const resultModalRef = React.useRef<IDefaultModalRef>()

  /**
   * Creates a ref to only close the specific modal related to this component, eg the payment modal
   */
  const paymentModalRef = React.useRef<IDefaultModalRef>()

  /**
   * Google Analytics hook used to log events
   */
  const LogEvent = useAnalytics()

  /**
   * Hook used to manage the state of the selected payment method
   */
  const [selectedPaymentMethod, setSelectedPaymentMethod] =
    React.useState<IGateway>()

  /**
   * Authentication hook used to chcek if the user is logged in and the details of the logged in user
   */
  const { isAuthenticated, user: loggedInUser } = useAuth()

  /**
   * Hook used to extract functionality from the Shop Context provider which receives data from the app redux state
   * items is the current items in the redux state
   * promocode is the promocode realted to the app
   * googleClick is the function used to track a user's clicks
   */
  const { items, promocode, googleClick, orderSummary } = useShop()

  /**
   * Extracts the config from the application state in redux
   */
  const {
    config: { gateways }
  } = useApplication()

  /**
   * Hook that helps to extract the data related to an order
   */
  const { getOrderPayload } = useCart()

  /**
   * Hook used to receive the session of the user
   */
  const { user } = useAuth()

  /**
   * GraphQL query used to add the order
   */
  const [addOrderHandler, { loading, data, error }] = useLazyQuery(ADD_ORDER)

  /**
   * Google Click Identifier used to track user's clicking activity
   */
  const [addGclidHandler] = useLazyQuery(ADD_GCLID)

  /**
   * GraphQL query used to retrieve the invoice of an order
   */
  const [getOrderInvoiceIdHandler, { data: iData, loading: iLoading }] =
    useLazyQuery(GET_UNPAID_INVOICE)

  /**
   * Next.js router hook used to redirect a user
   */
  const router = useRouter()

  /**
   * Redux dispatch hook
   */
  const dispatch = useDispatch()

  /**
   * Reroutes a user to the home page if there are no items in their cart when they reach this page
   */
  React.useEffect(() => {
    if (items?.length === 0) {
      router.push('/')
    }
  }, [])

  /**
   * When a user is logged in, this function executes
   * This sets the payment method to the payment method a user previously selected
   */
  React.useEffect(() => {
    if (loggedInUser?.paymentmethod) {
      const defaultGate = gateways?.find(
        (g) => g.key === loggedInUser?.paymentmethod
      )
      if (defaultGate) {
        setSelectedPaymentMethod(defaultGate)
      }
    }
  }, [loggedInUser])

  /**
   * Callback used to handle the functionality when a user proceeds with payment
   */
  const onProceedHandler = React.useCallback(() => {
    const order = {
      ...getOrderPayload(),
      clientid: loggedInUser?.userid,
      promocode: promocode || '',
      paymentmethod: selectedPaymentMethod?.key || 'payfasteft'
    }

    if (loggedInUser?.userid) {
      addOrderHandler({
        variables: {
          order
        }
      })
      LogEvent().checkout(6, 'checkout', items, orderSummary?.total)
    }
  }, [loading, data, error, loggedInUser, selectedPaymentMethod])

  /**
   * This function executes when data is loading from one of the GraphQl queries
   * If there is an error, we display an error modal to a user
   * If there are no errors, we retrieve the details related to the order's invoice
   * If there is a Google Click Id, we sned a GraphQL query to log the event
   */
  React.useEffect(() => {
    if (data?.addOrder?.error) {
      //
      resultModalRef?.current?.open()
      resultModalRef?.current?.renderTitle(t('page[checkout][sorry]'))
      resultModalRef?.current?.renderDescription(data?.addOrder?.error?.message)
    } else if (data?.addOrder?.result) {
      getOrderInvoiceIdHandler({
        variables: {
          id: data?.addOrder?.result?.invoiceid,
          client: {
            firstname: user?.name,
            lastname: user?.lastname,
            email: user?.email
          }
        }
      })

      if (googleClick?.gclid) {
        if (moment().isBefore(googleClick.expires_at)) {
          addGclidHandler({
            variables: {
              orderid: data?.addOrder?.result?.orderid,
              gclid: googleClick?.gclid
            }
          })
        }
      }
    }
  }, [loading])

  /**
   * Renders the snapscan logo if it is the user's selected payment method
   */
  React.useEffect(() => {
    if (iData?.getInvoice?.result) {
      if (iData?.getInvoice?.result?.snapscan) {
        paymentModalRef?.current?.renderHeaderLogo(
          <img src='/images/paymentmethods/snapscan.png' className='img-fit' />
        )
      }
      paymentModalRef?.current?.open()
    }
  }, [iLoading])

  /**
   * Helper function used to conditionally render the selected payment method's portal
   */
  const renderModalContent = () => {
    if (iData?.getInvoice?.result?.snapscan) {
      const { invoiceid, total, apiKey, accessApiKey } =
        iData?.getInvoice?.result?.snapscan
      LogEvent().success('snapscan-show', items, data?.addOrder?.result?.orderid, orderSummary, promocode)
      dispatch({ type: RESET_CART })
      return (
        <div
          className={`d-flex flex-column align-items-center ${styles.gateway__snapscan}`}
        >
          <h4 className='heading__primary color-pink'>R{total}</h4>

          <a
            href={`https://pos.snapscan.io/qr/${apiKey}?id=${invoiceid}&amount=${Math.ceil(
              total * 100
            )}&verifyHash=${accessApiKey}&strict=true`}
            className={styles.gateway__snapscan_item}
          >
            <img
              src={`https://pos.snapscan.io/qr/${apiKey}.svg?id=${invoiceid}&amount=${Math.ceil(
                total * 100
              )}&verifyHash=${accessApiKey}&strict=true&snap_code_size=225`}
            />
          </a>
        </div>
      )
    } else if (iData?.getInvoice?.result?.payfast) {
      if (iData?.getInvoice?.result?.paymentidentifier) {
        paymentModalRef?.current?.close()
        window?.payfast_do_onsite_payment(
          { uuid: iData?.getInvoice?.result?.paymentidentifier },
          (result) => {
            if (result === true) {
              //set analytic
              LogEvent().success('payfast-success', items, data?.addOrder?.result?.orderid, orderSummary, promocode)
              dispatch({ type: RESET_CART })
            } else {
              LogEvent().success('payfast-failed', items, data?.addOrder?.result?.orderid, orderSummary, promocode)
              dispatch({ type: RESET_CART })
              return onClosePaymentModal()
            }
          }
        )
      }
      return (
        <div className='mt-5'>
          <h4 className='heading__primary color-pink'>
            {t('page[checkout][sorry]')}
          </h4>
          <p>{t('page[summary][loadError]')}</p>
        </div>
      )
    }
  }

  /**
   * Helper function used to reset the cart state and reroute a person to the cancelled payment page
   */
  const onClosePaymentModal = () => {
    dispatch({ type: '@@INIT' })
    return router.push('/payment-cancelled')
  }

  return (
    <Page {...otherProps}>
      <section className='checkout__page mt-5'>
        <div className='container'>
          <div className='row'>
            <div className='col-xs-12 mt-5 text-center col-md-8 offset-md-2'>
              <h1 className='heading__primary heading_upper'>
                {t('button[checkout]')}
              </h1>
              <h4 className='heading_small color-pink'>
                {t('page[checkout][selectMethod]')}
              </h4>
              <div
                className={`d-flex align-items-center justify-content-between ${styles.checkout__paymentmethod}`}
              >
                {gateways?.map((gateway) => (
                  <div
                    className={styles.checkout__paymentmethod_item}
                    onClick={() => setSelectedPaymentMethod(gateway)}
                    key={gateway?.name}
                  >
                    {selectedPaymentMethod?.name === gateway.name ? (
                      <FaCheckCircle
                        className={styles.checkout__paymentmethod_selected}
                        size={40}
                        color={Colors.pink}
                      />
                    ) : (
                      <FaRegCircle
                        className={styles.checkout__paymentmethod_selected}
                        size={40}
                        color={Colors.grey}
                      />
                    )}
                    <div className='d-flex align-items-center justify-content-center'>
                      <div className={styles.checkout__paymentmethod_img}>
                        <img
                          src={gateway.icon}
                          alt=''
                          className={styles.checkout__paymentmethod_imgel}
                        />
                      </div>
                    </div>
                    <span className={styles.checkout__paymentmethod_name}>
                      {gateway.name}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          </div>
          {isAuthenticated && (
            <div className='row mt-5'>
              <div className='col-xs-12 col-md-6 text-center offset-md-3'>
                <button
                  className='btn btn-pink'
                  onClick={onProceedHandler}
                  disabled={loading || !selectedPaymentMethod}
                >
                  {loading
                    ? t('page[checkout][processing]')
                    : t('page[checkout][proceed]')}
                </button>
              </div>
            </div>
          )}
        </div>
      </section>

      {(loading || iLoading) && <LoadingSpinner />}
      <ModalDefault ref={resultModalRef} />
      <ModalDefault
        ref={paymentModalRef}
        hideCloseBtn
        renderContent={renderModalContent}
        onCloseHandler={() => router.push('/payment-cancelled')}
      />
    </Page>
  )
}

export default withAuth(CheckoutPage)

import { useShop } from '@app/context/Shop'
import BoxItem from '@components/Box/BoxItem'
import CartCheckoutFlow from '@components/Cart/CartCheckoutFlow'
import CartTracker from '@components/Cart/CartTracker'
import CartCheckoutOptions from '@components/Cart/CheckoutOptions'
import Page from '@components/HOC/Page'
import useTranslate from '@hooks/useTranslate'
import { PageProps } from '@lib/types/app'
import { NextPage } from 'next'
import { useRouter } from 'next/dist/client/router'
import React from 'react'
import { default as pageStyles, default as styles } from './page.module.scss'

interface ICartPage extends PageProps {}

const CartPage: NextPage<ICartPage> = (props) => {

  /**
   * 
   */
  const { query, replace } = useRouter();

  /**
   * 
   */
  const { items, flows, currentCheckoutFlow, checkoutOptions, onSelectedCheckoutOption } = useShop()

  /**
   * 
   */
  const {t} = useTranslate()



  /**
   * 
   */
  React.useEffect(() => {
    if (!query?.config) {
      /**
       * Check redux current flow
       */
      if (items?.length > 0) {
        replace(`/cart?config=${currentCheckoutFlow}`);
        
      } else {
        replace('/')
      }
    } else if (
      ![...Object.keys(flows), "flow", "domains"].includes(
        query?.config as string
      )
    ) {
      replace("/");
      
    }else {
      if(items?.length === 0){
        replace("/");
      }
    }
    onSelectedCheckoutOption(query?.config as string)
  }, [query])

  
  if (query?.config === 'flow') {
    return (
      <Page {...props}>
        <section className={`${pageStyles.page} ${pageStyles.page__cart}`}>
          <div className='container'>
            <CartCheckoutOptions />
          </div>
        </section>
      </Page>
    )
  }

  
  return (
    <Page {...props}>
      <section className={pageStyles.page}>
        <div className='container'>
          <div className='row'>
            <div className='col-xs-12 col-md-9'>
              <CartCheckoutFlow
                {...{
                  steps: checkoutOptions,
                  flowName: query?.config as string
                }}
              />
            </div>
            <div className={`d-none d-md-block col-md-3 ${styles.overview} mt-5 pe-0`}>
              <BoxItem cssClasses={`me-1`}>
                <h4 className='title-2 title-f-400'>{t('page[cart][overview]')}</h4>
                <CartTracker
                  {...{
                    data: items,
                    steps: checkoutOptions,
                  }}
                />
              </BoxItem>
            </div>
          </div>
        </div>
      </section>
    </Page>
  )
}

export default CartPage

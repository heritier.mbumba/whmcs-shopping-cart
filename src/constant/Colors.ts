export const Colors = {
  primary: "#20bfdd",
  secondary: "#56565b",
  light: "#fff",
  pink: "#dd206f",
  grey: "#ccc",
  success: "#52b788",
  danger: "#ff6b6b",
  warning: "#ffd7ba",
};